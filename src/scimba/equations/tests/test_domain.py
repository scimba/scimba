import torch
from scimba.equations.domain import (
    DiskBasedDomain,
    Id_domain,
    ParametricCurveBasedDomain,
    SpaceTensor,
    SquareDomain,
)


def test_domain():
    x = SpaceTensor(
        torch.tensor([[-1.0, -2.0], [1.0, 2.0], [3.0, 4.0]]), torch.tensor([1, 1, 2])
    )

    x1, x2 = x.get_coordinates()
    print(x.x.shape, x1.shape, x2.shape)
    print(f"x1 = {x1}")
    print(f"x2 = {x2}")

    x1, x2 = x.get_coordinates(label=1)
    print(x.x.shape, x1.shape, x2.shape)
    print(f"x1 = {x1}")
    print(f"x2 = {x2}")

    x1, x2 = x.get_coordinates(label=2)
    print(x.x.shape, x1.shape, x2.shape)
    print(f"x1 = {x1}")
    print(f"x2 = {x2}")


def exact_disk_normal(t):
    return torch.cat((torch.cos(t), torch.sin(t)), dim=1)


def test_disk_normals():
    domain = DiskBasedDomain(2, [0.0, 0.0], 1.0)

    t = torch.linspace(0, 2 * torch.pi, 5, requires_grad=True)[:, None]

    normals = domain.compute_normals(t)

    assert torch.allclose(normals, exact_disk_normal(t))


def test_parameteric_disk_normals():
    def surface(t):
        return torch.cat((torch.cos(t), torch.sin(t)), dim=1)

    domain = ParametricCurveBasedDomain(2, [[0.0, 2 * torch.pi]], surface)

    t = torch.linspace(0, 2 * torch.pi, 5, requires_grad=True)[:, None]

    normals = domain.compute_normals(t, Id_domain)

    assert torch.allclose(normals, exact_disk_normal(t))


def exact_sphere_normal(t):
    theta = t[:, 0, None]
    phi = t[:, 1, None]
    return torch.cat(
        (
            -torch.sin(phi) * torch.cos(theta),
            -torch.sin(phi) * torch.sin(theta),
            -torch.cos(phi),
        ),
        dim=1,
    )


def test_parameteric_sphere_normals():
    def surface(t):
        theta = t[:, 0, None]
        phi = t[:, 1, None]
        return torch.cat(
            (
                torch.sin(phi) * torch.cos(theta),
                torch.sin(phi) * torch.sin(theta),
                torch.cos(phi),
            ),
            dim=1,
        )

    domain = ParametricCurveBasedDomain(
        3, [[0.0, 2 * torch.pi], [0.0, torch.pi]], surface
    )

    t = torch.cat(
        (
            torch.linspace(0, 2 * torch.pi, 5, requires_grad=True)[:, None],
            torch.linspace(1e-5, torch.pi - 1e-5, 5, requires_grad=True)[:, None],
        ),
        dim=1,
    )

    normals = domain.compute_normals(t, Id_domain)

    assert torch.allclose(normals, exact_sphere_normal(t))


def test_sphere_normals():
    domain = DiskBasedDomain(3, [0.0, 0.0, 0.0], 1.0)

    t = torch.cat(
        (
            torch.linspace(0, 2 * torch.pi, 5, requires_grad=True)[:, None],
            torch.linspace(1e-5, torch.pi - 1e-5, 5, requires_grad=True)[:, None],
        ),
        dim=1,
    )

    normals = domain.compute_normals(t)

    assert torch.allclose(normals, exact_sphere_normal(t))


def test_cube_normals():
    domain = SquareDomain(3, [[0.0, 1.0], [0.0, 1.0], [0.0, 1.0]])
    true_normals = torch.tensor(
        [
            [-1.0, 0.0, 0.0],
            [+1.0, 0.0, 0.0],
            [0.0, -1.0, 0.0],
            [0.0, +1.0, 0.0],
            [0.0, 0.0, -1.0],
            [0.0, 0.0, +1.0],
        ]
    )
    for id in range(2 * domain.dim):
        normals = domain.compute_normals(torch.tensor([1.0]), id)
        assert torch.allclose(normals, true_normals[id])


if __name__ == "__main__":
    test_domain()
    test_disk_normals()
    test_parameteric_disk_normals()
    test_sphere_normals()
    test_parameteric_sphere_normals()
    test_cube_normals()
