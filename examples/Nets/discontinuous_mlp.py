"""
Implementation of discontinuous MLP from the paper
https://www.sciencedirect.com/science/article/pii/S0377042722003430
"""

from pathlib import Path

import matplotlib.pyplot as plt
import scimba.nets.mlp as dmlp
import scimba.nets.training as training
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


def g(x):
    x1 = x[:, 0]
    x2 = x[:, 1]
    a = torch.sin(0.4 * PI * (x1 + x2)) * (x2 >= torch.exp(x1))
    b = (torch.sin(0.7 * PI * (x1 + x2)) - 4.0) * (x2 < torch.exp(x1) - 1.0)
    c = (torch.sin(PI * (x1 + x2)) + 4.0) * (
        (x2 < torch.exp(x1)) & (x2 >= torch.exp(x1) - 1.0)
    )
    return a + b + c


def main():
    sampler = uniform_sampling.UniformSampling(2, [[-2.0, 2.0], [-2.0, 2.0]])
    x = sampler.sampling(1000)
    y = g(x)
    y = y[:, None]

    file_name = "testd.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training.Trainer.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 40, 20]
    network = dmlp.DiscontinuousMLP(
        2, 1, layer_sizes=tlayers, layer_type=[False, True, False, True]
    )

    print(x.size(), " ", y.size())
    trainer = training.Trainer(
        in_size=2,
        input_data=x,
        out_size=1,
        output_data=y,
        network=network,
        file_name=file_name,
        learning_rate=1.2e-2,
        decay=0.99,
        batch_size=200,
    )

    trainer.train(epochs=1000)

    x = sampler.sampling(50000)
    y = g(x)
    y_pred = network.forward(x)

    fig, ax = plt.subplots(1, 2, figsize=(15, 5))
    im = ax[0].scatter(
        x[:, 0].detach().cpu().numpy(),
        x[:, 1].detach().cpu().numpy(),
        s=3,
        c=y.detach().cpu().numpy(),
        cmap="gist_ncar",
        label="u(x, y)",
    )
    fig.colorbar(im, ax=ax[0])

    im = ax[1].scatter(
        x[:, 0].detach().cpu().numpy(),
        x[:, 1].detach().cpu().numpy(),
        s=3,
        c=y_pred[:, 0].detach().cpu().numpy(),
        cmap="gist_ncar",
        label="u_theta(x, y)",
    )
    fig.colorbar(im, ax=ax[1])
    plt.show()


if __name__ == "__main__":
    main()
