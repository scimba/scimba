# How to run a Vlasov-Poisson simulation ?:
#  Rk: If nothing is specified the results will be saved in the folder data/
- for a Landau case:
`python ex_VlasovPoisson.py -i config_bydefault_Landau.yaml`
- for a Bump-on-Tail case:
`python ex_VlasovPoisson.py -i config_bydefault_BOT.yaml`

# How to run a PINN from these Vlasov-Poisson simulations ?
Therefore you can run the script `ex_PINNvlasov_1x1v.py` developed for CEMRACS2023 to run the default configuration defined in `config_default_PINNvlasov_1X1V.yaml`
or with the options: 
- `-h` for help
- `-i config_test_PINNvlasov_1X1V.yaml` for a small test case
All the results are then saved in the folder defined by `folder_for_saved_networks` in the YAML config file. 
For instance:
```
cd $SCIMBAPATH/applications/scimba_plasma/vlasovpoisson
python ex_PINNvlasov_1x1v.py   
```
will save the results in the two folders `data` and `D_1X1V_VLASOV` as:

data
├── LD_eps0.001_t10.0_Nx1024_Nv128.h5
├── LD_fdistribu_eps0.001_t10.0_Nx1024_Nv128.png
└── LD_PotEnergy_eps0.001_t10.0_Nx1024_Nv128.png

D_1X1V_VLASOV
├── LD_EPS0.001_T10.0_NX1024_NV128
│   └── RESU_NEPOCHS7000_NDATA200_NCOLLOC3000
│       ├── fig_deltaf_epochs14000.png
│       ├── fig_deltaf_epochs7000.png
│       ├── fig_fdistribu_epochs14000.png
│       ├── fig_fdistribu_epochs7000.png
│       ├── fig_loss.png
│       └── LD_eps0.001_t10.0_Nx1024_Nv128.pth
└── runs
    └── LD_EPS0.001_T10.0_NX1024_NV128
            └── exp_RESU_NEPOCHS7000_NDATA200_NCOLLOC3000
                └── events.out.tfevents.1705590376.persee.partenaires.cea.fr.264481.0

