from pathlib import Path

import h5py as h5
import numpy as np

from applications.scimba_plasma.vlasovpoisson import vlasov as vl

test_path = Path(__file__).parent


def test_solve_vlasov():
    # Set grid
    nx, nv = 4, 4
    xmin, xmax = 0.0, 4 * np.pi
    vmin, vmax = -6.0, 6.0

    # Create Vlasov-Poisson simulation
    sim = vl.VlasovPoisson(xmin, xmax, nx, vmin, vmax, nv)

    # Initialize distribution function
    X, V = np.meshgrid(sim.x, sim.v, indexing="ij")
    eps, kx = 0.001, 0.5
    Teq = 1.0
    feq = 1.0 / np.sqrt(2.0 * np.pi * Teq) * np.exp(-0.5 * V * V / Teq)
    f = feq * (1.0 + eps * np.cos(kx * X))

    # Set time domain
    nstep = 4
    t, dt = np.linspace(0.0, 50.0, nstep, retstep=True)

    # Set the output distribution function and electric field
    F = np.zeros((nstep, nx, nv))
    F[0, :] = f
    efield = np.zeros((nstep, nx))

    # Run simulation
    nrj, F, efield = sim.run(f, nstep, dt, F, efield)

    # Save data in HDF5 file
    HDF5filename = test_path / f"VP_Nx{nx}_Nv{nv}.h5"
    with h5.File(HDF5filename, "w") as fh5:
        fh5.create_dataset("xgrid", data=sim.x)
        fh5.create_dataset("vgrid", data=sim.v)
        fh5.create_dataset("timegrid", data=t)
        fh5.create_dataset("Teq", data=Teq)
        fh5.create_dataset("feq", data=feq)
        fh5.create_dataset("epsilon", data=eps)
        fh5.create_dataset("kx", data=kx)
        fh5.create_dataset("fdistribu", data=F)
        fh5.create_dataset("efield", data=efield)
