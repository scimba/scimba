from pathlib import Path

import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.neuralgalerkin.neural_galerkin_xv as ng
import scimba.pinns.pinn_xv as pinn_xv
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.sampling_pde_txv as sampling_pde_txv
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

PI = 3.14159265358979323846

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class Advection_xv_2D(pdes.AbstractPDExv):
    def __init__(self, xdomain, vdomain, p_domain=[[1.0, 1.00001]]):
        super().__init__(
            nb_unknowns=1,
            space_domain=xdomain,
            velocity_domain=vdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_x = True
        self.first_derivative_v = True
        self.second_derivative_x = False
        self.second_derivative_v = False

    # Abstract methods in pdes.AbstractPDExv must be inside subclasses.
    def bc_residual(
        self,
        w: dict,
        x: domain.SpaceTensor,
        v: torch.Tensor,
        mu: torch.Tensor,
        **kwargs,
    ):
        u = self.get_variables(w)
        return u

    def residual(
        self,
        w: dict,
        x: domain.SpaceTensor,
        v: torch.Tensor,
        mu: torch.Tensor,
        **kwargs,
    ):
        x1, x2 = x.get_coordinates()
        u_x1 = self.get_variables(w, "w_x")
        u_x2 = self.get_variables(w, "w_y")
        v_1, v_2 = self.get_velocities(v)
        return -(v_1 * u_x1 + v_2 * u_x2)  # <v,grad_x(u)>
    
    #def post_processing(self, x,v, mu, w):
    #    x1, x2 = x.get_coordinates()
    #    return x1 * (1 - x1) * x2 * (1 - x2) * w


def space_gauss(x, y, mean):
    sig0 = 0.1
    f = torch.exp(-((x - mean[0]) ** 2.0 + (y - mean[1]) ** 2.0) / sig0**2)
    return f


def velocity_gauss(x, y, mean):
    sig0 = 0.03 * (2.0 * torch.pi)
    f = torch.exp(-((x - mean[0]) ** 2.0 + (y - mean[1]) ** 2.0) / sig0**2)
    return f


# Initial condition : sum of 2 product of 2 gaussian distributions (dim 2 space, dim 2 velocity)
def init(x: domain.SpaceTensor, v, mu):
    x1, x2 = x.get_coordinates()
    v1, v2 = v[:, 0], v[:, 1]
    v1 = v1[:, None]
    v2 = v2[:, None]

    mu_x1 = torch.tensor([0.5, 0.25])
    mu_x2 = torch.tensor([0.75, 0.5])

    mu_v1 = torch.tensor([0.0, 1.0])
    mu_v2 = torch.tensor([-1.0, 0.0])

    f_x1 = space_gauss(x1, x2, mu_x1)
    f_x2 = space_gauss(x1, x2, mu_x2)

    f_v1 = velocity_gauss(v1, v2, mu_v1)
    f_v2 = velocity_gauss(v1, v2, mu_v2)

    return 2.0*(f_x1 * f_v1 + f_x2 * f_v2)


# compute the exact solution of linear boltzmann equation for u_0 = init


def sol_ref(t: float, x: domain.SpaceTensor, v: torch.Tensor, mu: torch.Tensor):
    x1, x2 = x.get_coordinates()
    v1, v2 = v[:, 0], v[:, 1]
    v1 = v1[:, None]
    v2 = v2[:, None]

    mu_x1 = torch.tensor([0.5, 0.25])
    mu_x2 = torch.tensor([0.75, 0.5])

    mu_v1 = torch.tensor([0.0, 1.0])
    mu_v2 = torch.tensor([-1.0, 0.0])

    f_xt1 = space_gauss(x1 - t * v1, x2 - t * v2, mu_x1)
    f_xt2 = space_gauss(x1 - t * v1, x2 - t * v2, mu_x2)

    f_vt1 = velocity_gauss(v1, v2, mu_v1)
    f_vt2 = velocity_gauss(v1, v2, mu_v2)

    f = f_xt1 * f_vt1 + f_xt2 * f_vt2
    return 2.0*f


def f(t):
    return torch.cat([torch.cos(2.0 * PI * t), torch.sin(2.0 * PI * t)], axis=1)


def main():
    file_name_NG = "advection_xv2d_nn_NG.pth"
    delete_previous_networks = True
    # delete_previous_networks = False

    if delete_previous_networks:
        (
            Path.cwd()
            / Path(ng.NeuralGalerkin_xv.FOLDER_FOR_SAVED_NETWORKS)
            / file_name_NG
        ).unlink(missing_ok=True)

    xdomain = domain.SpaceDomain(
        2, domain.SquareDomain(2, torch.tensor([[0.0, 1.0], [0.0, 1.0]]))
    )
    vdomain = domain.ParametricCurveBasedDomain(2, [[0.0, 1.0]], f)
    pde = Advection_xv_2D(xdomain, vdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    v_sampler = sampling_pde_txv.VSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling,
        model=pde,
    )

    tlayers = [20, 20,20,20, 20]


    #network = pinn_xv.MLP_xv(
    #    pde=pde,
    #    layer_sizes=tlayers,
    #    activation_type="sine",
    #)
    network = pinn_xv.Fourier_xv(
            pde=pde,
            list_type_features={"x": False,"v": False, "xv": True, "mu": False, "xvmu": False},
            mean_features={"x": 0.0,"v":0.0,"xv":0.0, "mu": 0.0, "xmu": 0.0},
            std_features={"x": 0.0,"v":0.0 ,"xv":2.0 , "mu": 0.0, "xvmu": 0.0},
            nb_features=10,
            layer_sizes=tlayers,
            activation_type="sine",
        )

    # network = pinn_txv.MLPxmu_2momentv(
    #    pde,
    #    layer_sizes=tlayers,
    # )

    pinn = pinn_xv.PINNxv(network, pde, init_net_bool=False)

    model = ng.NeuralGalerkin_xv(
        pde,
        x_sampler,
        v_sampler,
        mu_sampler,
        pinn,
        scheme="rk2",
        type_init=1,
        subsample_bool=False,
        subsample=3000
    )

    model.compute_initial_data(
        projector=None,
        w0=init,
        n_collocation=10000,
        epoch_init=14000,
        lr_init=2.8e-2,
        file_name=file_name_NG,
    )

    model.time_loop(dt=0.002, T=0.5, n_collocation=4000)

    return model


if __name__ == "__main__":
    model = main()

    v_given = torch.tensor(
        [
            torch.cos(torch.tensor(torch.pi / 2.0)),
            torch.sin(torch.tensor(torch.pi / 2.0)),
        ]
    )

    model.plot(n_visu=30000, sol_exact=sol_ref, v_given=v_given)
