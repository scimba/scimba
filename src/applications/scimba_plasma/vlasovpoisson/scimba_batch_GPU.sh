#!/bin/bash
#SBATCH --account=cin4469
#SBATCH --job-name=scimba
#SBATCH --constraint=MI250
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --output=%A.out
#SBATCH --time=1:00:00

module purge

module load cray-python
module load aws-ofi-rccl

module list

# Use a Python venv unless you install everything in your home.
# source ./python_environment/bin/activate
source ~/bin/AI_python_env/bin/activate
export SCIMBAPATH=/lus/home/CT5/cin4469/gdgirard/work_cin4469/AI_work/scimba_git
export PYTHONPATH=/lus/home/CT5/cin4469/gdgirard/work_cin4469/AI_work/PINN_CEMRACS2023_git/:$PYTHONPATH
export PYTHONPATH=$SCIMBAPATH:$PYTHONPATH

export TRANSFORMERS_OFFLINE=1

export MIOPEN_USER_DB_PATH="/tmp/${USER}-miopen-cache-${SLURM_JOB_ID}"
export MIOPEN_CUSTOM_CACHE_DIR="${MIOPEN_USER_DB_PATH}"

################################################################################
# Run
################################################################################

srun \
    --ntasks-per-node=1 \
    --gpus-per-task=1 \
    --cpu-bind=none \
    --mem-bind=none \
    --label \
    -- torchrun --nnodes="${SLURM_NNODES}" --nproc_per_node="1" \
    --rdzv-id="${SLURM_JOBID}" \
    --rdzv-backend=c10d \
    --rdzv-endpoint="$(scontrol show hostname ${SLURM_NODELIST} | head -n 1):29400" \
    --max-restarts="0" \
    -- ex_PINNvlasov_1x1v.py -i config_testinit_LD0.01.yaml
