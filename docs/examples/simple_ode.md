# Simple ODE

We solve the ODE:

$$\frac{du(t)}{dt} + \alpha u(t) =0$$

on $t\in[0,10], \alpha\in [0.5,1.0]$.

We learn $u(t,x,\alpha)$.

**Parametric model:**

$$u_{net}(t,\alpha) = u_0 + t \cdot u_{\theta}(t,\alpha)$$

```{literalinclude} ../../examples/PINNs/ode/simple_ode.py
```
