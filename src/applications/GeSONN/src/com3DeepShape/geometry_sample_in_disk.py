"""
Author:
    A BELIERES FRENDO (IRMA)
Date:
    05/05/2023

Solve poisson EDP in a shape genereted by a symplectomorphism
-Delta u = f
Inspired from a code given by V MICHEL DANSAC (INRIA)
"""

# %%


# ----------------------------------------------------------------------
#   IMPORTS - MACHINE CONFIGURATION
# ----------------------------------------------------------------------

# imports
import time
from pathlib import Path

import torch

from scimba.equations import domain, pdes
from scimba.nets import sympnet, training, training_tools
from scimba.pinns import pinn_x, training_x
from scimba.pinns.pinn_losses import PinnLossesData
from scimba.sampling import sampling_pde
from scimba.sampling.sampling_parameters import MuSampler
from scimba.sampling.uniform_sampling import UniformSampling
from src.com1PINNs import boundary_conditions as bc
from src.com1PINNs import sourceTerms
from src.out1Plot import makePlots

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}; script is poisson.py")


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain, deepGeoDict):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=0,
        )

        self.first_derivative = True
        self.second_derivative = False

        self.source_term = deepGeoDict.get("source_term", "one")
        self.boundary_condition = deepGeoDict.get(
            "boundary_condition", "homogeneous_dirichlet"
        )

    # Jacobian of the symplectic map

    def compute_sympnet_jacobian(self, x, y):
        T = self.disk_to_shape(x, y)

        J_a = torch.autograd.grad(T[0].sum(), x, create_graph=True)[0]
        J_b = torch.autograd.grad(T[0].sum(), y, create_graph=True)[0]
        J_c = torch.autograd.grad(T[1].sum(), x, create_graph=True)[0]
        J_d = torch.autograd.grad(T[1].sum(), y, create_graph=True)[0]

        return J_a, J_b, J_c, J_d

    # Functions to compute the boundary residual

    def get_normal(self, x, y):
        nx, ny = x, y
        return nx, ny

    def get_tangential_jacobian(self, x, y):
        J_a, J_b, J_c, J_d = self.compute_sympnet_jacobian(x, y)
        nx, ny = self.get_normal(x, y)

        Jac_tan_x = J_d * nx - J_c * ny
        Jac_tan_y = -J_b * nx + J_a * ny

        return torch.sqrt(Jac_tan_x**2 + Jac_tan_y**2)

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        if self.boundary_condition != "robin":
            return u
        else:
            tangential_jacobian = self.get_tangential_jacobian(*x.get_coordinates())
            return 0.5 * tangential_jacobian * u**2

    # Functions to compute the residual

    def get_metric_tensor(self, x, y):
        J_a, J_b, J_c, J_d = self.compute_sympnet_jacobian(x, y)

        fac = (J_a * J_d - J_b * J_c) ** 2
        A_a = (J_d**2 + J_b**2) / fac
        A_b = -(J_c * J_d + J_a * J_b) / fac
        A_c = A_b
        A_d = (J_c**2 + J_a**2) / fac

        return A_a, A_b, A_c, A_d

    def left_hand_term(self, x, y, w):
        a, b, c, d = self.get_metric_tensor(x, y)

        dx_u = self.get_variables(w, "w_x")
        dy_u = self.get_variables(w, "w_y")

        A_grad_u_grad_u = (a * dx_u + b * dy_u) * dx_u + (c * dx_u + d * dy_u) * dy_u

        if self.boundary_condition == "homogeneous_neumann":
            u = self.get_variables(w, "w")
            return A_grad_u_grad_u + u**2

        return A_grad_u_grad_u

    def right_hand_term(self, x, y, w):
        u = self.get_variables(w, "w")
        f = sourceTerms.get_f(*self.disk_to_shape(x, y), name=self.source_term)
        return f * u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        A_grad_u_grad_u = self.left_hand_term(x1, x2, w)
        f_u = self.right_hand_term(x1, x2, w)
        return 0.5 * A_grad_u_grad_u - f_u


class PINN_and_SympNet(pinn_x.PINNx):
    def __init__(self, network_pinn, network_sympnet, pde, boundary_condition):
        networks = torch.nn.ModuleList([network_pinn, network_sympnet])
        networks.forward = network_pinn.forward
        super().__init__(networks, pde)

        self.sympnet_forward = network_sympnet.forward
        self.sympnet_inverse = network_sympnet.inverse

        self.boundary_condition = boundary_condition

        self.pde = pde
        self.pde.disk_to_shape = self.disk_to_shape
        self.pde.inverse_disk_to_shape = self.inverse_disk_to_shape

    def disk_to_shape(self, x1, x2):
        xy = self.sympnet_forward(torch.cat((x1, x2), axis=1))
        return xy[:, 0, None], xy[:, 1, None]

    def inverse_disk_to_shape(self, x1, x2):
        xy = self.sympnet_inverse(torch.cat((x1, x2), axis=1))
        return xy[:, 0, None], xy[:, 1, None]

    def get_w(self, data: domain.SpaceTensor, mu: torch.Tensor) -> torch.Tensor:
        x1, x2 = data.get_coordinates()
        image_x1, image_x2 = self.disk_to_shape(x1, x2)
        image = torch.cat((image_x1, image_x2), axis=1)
        return bc.apply_BC(self(image, mu), x1, x2, 0, 1, name=self.boundary_condition)


# ----------------------------------------------------------------------
#   CLASSE NETWORK - RESEAU DE NEURONES
#   approximation d'un symplectomorphisme
# ----------------------------------------------------------------------


class PINNs:
    DEFAULT_PINNS_DICT = {
        "learning_rate": 5e-3,
        "layer_sizes": [10, 20, 40, 20, 10],
        "nb_of_networks": 4,
        "networks_size": 4,
        "file_name": "default",
        "symplecto_name": None,
        "to_be_trained": True,
        "source_term": "one",
        "boundary_condition": "homogeneous_dirichlet",
        "pinn_activation": "tanh",
        "sympnet_activation": "tanh",
    }

    # constructeur
    def __init__(self, **kwargs):
        deepGeoDict = kwargs.get("deepGeoDict", self.DEFAULT_PINNS_DICT)

        # copy (key, value) from DEFAULT_PINNS_DICT to deepGeoDict if not present
        for key, value in self.DEFAULT_PINNS_DICT.items():
            if deepGeoDict.get(key) is None:
                deepGeoDict[key] = value

        # Storage file
        self.fig_storage = Path.cwd() / "outputs" / "PINNs" / "img"
        self.fig_storage.mkdir(parents=True, exist_ok=True)
        self.fig_storage = self.fig_storage / f"{deepGeoDict['file_name']}"

        # PINN parameters
        self.learning_rate = deepGeoDict["learning_rate"]
        self.layer_sizes = deepGeoDict["layer_sizes"]
        self.pinn_activation = deepGeoDict["pinn_activation"]

        # SympNet parameters
        self.n_sympnet_layers = deepGeoDict["n_sympnet_layers"]
        self.sympnet_layer_size = deepGeoDict["sympnet_layer_size"]
        self.sympnet_activation = deepGeoDict["sympnet_activation"]

        # Source term of the Poisson problem
        self.source_term = deepGeoDict["source_term"]

        # Boundary condition of the Poisson problem
        self.boundary_condition = deepGeoDict["boundary_condition"]

        # create domain

        xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.0, 0.0], 1.0))

        # create PDE

        self.pde = Poisson_2D(xdomain, deepGeoDict=deepGeoDict)

        # create samplers

        x_sampler = sampling_pde.XSampler(pde=self.pde)
        mu_sampler = MuSampler(sampler=UniformSampling, model=self.pde)
        sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

        # create network

        network_pinn = pinn_x.MLP_x(
            pde=self.pde,
            layer_sizes=self.layer_sizes,
            activation_type=self.pinn_activation,
        )
        network_sympnet = sympnet.SympNet(
            dim=2,
            p_dim=0,
            widths=[self.sympnet_layer_size] * self.n_sympnet_layers,
            activation=self.sympnet_activation,
        )

        self.model = PINN_and_SympNet(
            network_pinn,
            network_sympnet,
            self.pde,
            self.boundary_condition,
        )

        # create losses and optimizers

        loss_function = training.MassLoss()
        if self.boundary_condition == "robin":
            losses = PinnLossesData(
                residual_f_loss=loss_function, bc_loss_bool=True, w_bc=1, w_res=1
            )
        else:
            losses = PinnLossesData(residual_f_loss=loss_function)

        optimizers = training_tools.OptimizerData(
            learning_rate=self.learning_rate, decay=0.99
        )

        # create trainer

        self.trainer = training_x.TrainerPINNSpace(
            pde=self.pde,
            network=self.model,
            sampler=sampler,
            losses=losses,
            optimizers=optimizers,
            file_name=f"{deepGeoDict['file_name']}.pth",
        )

        # flag for saving results
        self.save_results = kwargs.get("save_results", False)

    def get_u(self, x, y):
        data = domain.SpaceTensor(torch.cat((x, y), axis=1))
        mu = torch.ones((x.shape[0], 0))
        return self.model.get_w(data, mu)

    def train(self, epochs, n_collocation, **kwargs):
        start = time.perf_counter()
        self.trainer.train(
            epochs=epochs,
            n_collocation=n_collocation,
            n_bc_collocation=n_collocation,
            **kwargs,
        )
        return time.perf_counter() - start

    def plot_result(self, save_plots, error=False):
        makePlots.loss(self.trainer.losses.loss_history, save_plots, self.fig_storage)
        n_visu = 128 if device == "cuda" else 768

        makePlots.edp_contour(
            0,
            1,
            self.get_u,
            self.model.disk_to_shape,
            self.model.inverse_disk_to_shape,
            save_plots,
            self.fig_storage / "u",
            n_visu=n_visu,
        )

        # makePlots.edp_contour(
        #     0,
        #     1,
        #     self.get_fv,
        #     lambda x, y: metricTensors.apply_symplecto(x, y, name=self.symplecto_name),
        #     lambda x, y: metricTensors.apply_symplecto(
        #         x, y, name=f"inverse_{self.symplecto_name}"
        #     ),
        #     save_plots,
        #     self.fig_storage / "fv",
        #     n_visu=n_visu,
        # )
