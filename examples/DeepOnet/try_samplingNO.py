import scimba.sampling.sampling_functions as sf
import torch


class base_ft(sf.ParametricFunction_t):
    def __init__(self):
        super().__init__(dim_f=2,dim_p=1,p_domain=[[0.1,1.0]])

    def __call__(self,t,params):
        alpha = self.get_parameters(params)
        return torch.cat([t,alpha*t],axis=1)

t_sensor = torch.linspace(0.0,1.0,20)[:,None]
t_loss = torch.linspace(0.0,1.0,12)[:,None]

print(t_sensor.shape)
sampler = sf.ParametricFunctionsSampler_t(base_ft(), t_sensor=t_sensor, t_loss=t_loss)

f_f,f_l = sampler.sampling(5)
print(f_f.shape)
print(f_l.shape)



class base_fx(sf.ParametricFunction_x):
    def __init__(self):
        super().__init__(dim_f=2,dim_p=1,dim_x=2)

    def __call__(self,x,params):
        alpha = self.get_parameters(params)
        x1,x2 = x.get_coordinates()
        return torch.cat([x1,alpha*x2],axis=1)


x_sensor = torch.tensor([[0.1,0.2],[0.1,0.2],[-0.1,0.2],[0.4,0.2]])
x_loss = torch.tensor([[0.1,0.2],[0.1,0.2],[0.1,0.2],[0.1,0.2],[0.1,0.2],[0.1,0.2]])

print('/+///////////////')
print(x_sensor.shape)
sampler = sf.ParametricFunctionsSampler_x(base_fx(),x_sensor=x_sensor, x_loss=x_loss)

f_f,f_l = sampler.sampling(7)
print(f_f.shape)
print(f_l.shape)



class base_fx(sf.ParametricFunction_x):
    def __init__(self):
        super().__init__(dim_f=2,dim_p=1,dim_x=2)

    def __call__(self,x,params):
        alpha = self.get_parameters(params)
        x1,x2 = x.get_coordinates()
        return torch.cat([x1,alpha*x2],axis=1)

t_sensor = torch.linspace(0.0,1.0,3)[:,None]
t_loss = torch.linspace(0.0,1.0,4)[:,None]
x_sensor = torch.tensor([[0.1,0.2],[0.1,0.2],[0.4,0.2]])
x_loss = torch.tensor([[0.1,0.2],[0.1,0.2],[0.1,0.2],[0.1,0.2]])

print('/+///////////////')
print(x_sensor.shape)
sampler = sf.ParametricFunctionsSampler_x(base_fx(),t_sensor=t_sensor,t_loss=t_loss,x_sensor=x_sensor, x_loss=x_loss)

f_f,f_l = sampler.sampling(9)
print(f_f.shape)
print(f_l.shape)





class base_ftx(sf.ParametricFunction_tx):
    def __init__(self):
        super().__init__(dim_p=1,dim_x=2)

    def __call__(self,t,x,params):
        alpha = self.get_parameters(params)
        x1,x2 = x.get_coordinates()
        return torch.cat([x1*t,alpha*x2],axis=1)
