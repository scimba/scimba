import copy as cp

import pylab as plt
import scimba.nets.mlp as mlp
import scimba.odelearning.abstract_ode as abstractode
import scimba.odelearning.generation_ode_data as generation_ode_data
import scimba.odelearning.generic_flux as genericflux
import scimba.odelearning.loss_odelearning as loss
import scimba.odelearning.training_diffphy_ode as training
import torch


class SIRflux_beta_sint(genericflux.Flux):
    def __init__(self, beta):
        super().__init__()

        self.beta = beta
        self.nb_unknowns = 3
        self.nb_parameters = 1

    def forward(self, t, state, mu):
        t = self.get_t(t)
        gamma = self.get_parameters(mu)
        S, I, R = self.get_variables(state)

        betat = 1.0 + 0.333 * torch.sin(0.3 * t)

        dS = -self.beta * betat * S * I
        dI = self.beta * betat * S * I - gamma * I
        dR = gamma * I

        return self.set_derivatives(state, dS, dI, dR)


class SIRflux_beta_t(genericflux.Flux):
    def __init__(self, net):
        super().__init__()

        self.net = cp.deepcopy(net)
        self.nb_unknowns = 3
        self.nb_parameters = 1

    def forward(self, t, state, mu):
        t = self.get_t(t)
        gamma = self.get_parameters(mu)
        S, I, R = self.get_variables(state)

        dS = -self.net(t) * S * I
        dI = self.net(t) * S * I - gamma * I
        dR = gamma * I

        return self.set_derivatives(state, dS, dI, dR)


class SIR_ode(abstractode.ClassicODE):
    def random_init_data(self):
        S = 0.9 + 0.1 * torch.rand(1)
        I = 1.0 - S
        R = 0.0
        return S, I, R

    def random_params(self):
        gamma = 0.4
        return gamma

    def plot_data(self, time, data):
        time = time.detach().cpu()
        data = data.detach().cpu()
        fig, axs = plt.subplots(2, 2, figsize=(14, 10))
        axs[0, 0].plot(time, data[..., 0])
        axs[0, 0].set_title("S dynamic")
        axs[0, 1].plot(time, data[..., 1])
        axs[0, 1].set_title("I dynamic")
        axs[1, 0].plot(time, data[..., 2])
        axs[1, 0].set_title("R dynamic")
        plt.show()


class MSELoss_RefOde(loss.LossODELearning_withdata):
    def __init__(self, ode):
        super().__init__(ode)
        self.loss_used = torch.nn.MSELoss()

    def apply(self, x, xref, t, mu):
        """
        x: shape(nb_time, nb_samples, nb_unknowns)
        """
        return self.loss_used(input=x, target=xref)


network = mlp.GenericMLP(1, 1, layer_sizes=[10, 20, 20, 10])
flux_ref = SIRflux_beta_sint(0.6)
flux = SIRflux_beta_t(network)

oderef = SIR_ode(flux_ref)
ode = SIR_ode(flux)

ts = torch.linspace(
    start=0.0,
    end=50.0,
    steps=200,  # number of time steps per time batch
)

# one batch of initial conditions
initial_conditions = oderef.generate_initial_conditions(20)
mu = oderef.generate_parameters(20)
oderef.time_scheme(initial_conditions, ts, mu)

oderef.plot_data(ts, oderef.solution)

# generation of data
data_ode = generation_ode_data.DataGenerate(
    oderef, T=50, num_time_batch=25, num_timestep_per_batch=10
)
data_ode.create_dataset(50)
loss_for_training = MSELoss_RefOde(ode)

# compute the model before training
initial_conditions = torch.tensor([0.95, 0.01, 0.0])
mu = torch.tensor([0.4])
oderef.time_scheme(initial_conditions, ts, mu)
sol_ref = cp.copy(oderef.solution.detach().cpu())
ode.time_scheme(initial_conditions, ts, mu)
sol_pre_train = cp.copy(ode.solution.detach().cpu())
beta_1 = ode.flux.net(ts[:, None]).detach().cpu()

# training
trainer = training.Trainer_DF_ODE_with_data(
    model=ode, data=data_ode, loss_f=loss_for_training, nb_batch=5
)
trainer.train(epochs=20)

# compute the model after training
ode.time_scheme(initial_conditions, ts, mu)
sol_post_train = cp.copy(ode.solution.detach().cpu())
beta_2 = ode.flux.net(ts[:, None]).detach().cpu()

# plot
fig, axs = plt.subplots(2, 2, figsize=(14, 10))
ts = ts.detach().cpu()
axs[0, 0].plot(ts, sol_ref[..., 0].detach().cpu(), "r")
axs[0, 0].plot(ts, sol_pre_train[..., 0].detach().cpu(), "b+")
axs[0, 0].plot(ts, sol_post_train[..., 0].detach().cpu(), "g+")
axs[0, 0].set_title("S dynamic")
axs[0, 1].plot(ts, sol_ref[..., 1].detach().cpu(), "r")
axs[0, 1].plot(ts, sol_pre_train[..., 1].detach().cpu(), "b+")
axs[0, 1].plot(ts, sol_post_train[..., 1].detach().cpu(), "g+")
axs[0, 1].set_title("I dynamic")
axs[1, 0].plot(ts, sol_ref[..., 2].detach().cpu(), "r")
axs[1, 0].plot(ts, sol_pre_train[..., 2].detach().cpu(), "b+")
axs[1, 0].plot(ts, sol_post_train[..., 2].detach().cpu(), "g+")
axs[1, 0].set_title("R dynamic")
axs[1, 1].plot(ts, 0.6 * (1.0 + 0.333 * torch.sin(0.3 * ts)), "r")
axs[1, 1].plot(ts, beta_1, "b+")
axs[1, 1].plot(ts, beta_2, "g+")
axs[1, 1].set_title(" beta(t)")
plt.show()
