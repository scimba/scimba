"""
Author:
    A BELIERES FRENDO (IRMA)
Date:
    05/05/2023

Solve poisson EDP in a shape genereted by a symplectomorphism
-Delta u = f
Inspired from a code given by V MICHEL DANSAC (INRIA)
"""

# %%


# ----------------------------------------------------------------------
#   IMPORTS - MACHINE CONFIGURATION
# ----------------------------------------------------------------------

# imports
import time
from pathlib import Path

import torch

from scimba.equations import domain, pdes
from scimba.nets import training, training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from src.com1PINNs import boundary_conditions as bc
from src.com1PINNs import metricTensors, sourceTerms
from src.out1Plot import makePlots

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}; script is poisson.py")


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain, PINNsDict):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=0,
        )

        self.first_derivative = True
        self.second_derivative = False

        self.source_term = PINNsDict.get("source_term", "one")
        self.boundary_condition = PINNsDict.get(
            "boundary_condition", "homogeneous_dirichlet"
        )
        self.symplecto_name = PINNsDict.get("symplecto_name", None)

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def get_metric_tensor(self, x, y):
        # il faut calculer :
        # A = J_T^{-t}*J_T^{-1}

        if self.symplecto_name is not None:
            T = metricTensors.apply_symplecto(x, y, name=self.symplecto_name)

            J_a = torch.autograd.grad(T[0].sum(), x, create_graph=True)[0]
            J_b = torch.autograd.grad(T[0].sum(), y, create_graph=True)[0]
            J_c = torch.autograd.grad(T[1].sum(), x, create_graph=True)[0]
            J_d = torch.autograd.grad(T[1].sum(), y, create_graph=True)[0]

            fac = (J_a * J_d - J_b * J_c) ** 2
            A_a = (J_d**2 + J_b**2) / fac
            A_b = -(J_c * J_d + J_a * J_b) / fac
            A_c = A_b
            A_d = (J_c**2 + J_a**2) / fac

        else:
            A_a = 1
            A_b = 0
            A_c = 0
            A_d = 1

        return A_a, A_b, A_c, A_d

    def left_hand_term(self, x, y, w):
        a, b, c, d = self.get_metric_tensor(x, y)

        dx_u = self.get_variables(w, "w_x")
        dy_u = self.get_variables(w, "w_y")

        A_grad_u_grad_u = (a * dx_u + b * dy_u) * dx_u + (c * dx_u + d * dy_u) * dy_u

        if self.boundary_condition == "homogeneous_neumann":
            u = self.get_variables(w, "w")
            return A_grad_u_grad_u + u**2

        return A_grad_u_grad_u

    def right_hand_term(self, x, y, w):
        u = self.get_variables(w, "w")

        # terme source
        f = sourceTerms.get_f(
            *metricTensors.apply_symplecto(x, y, name=self.symplecto_name),
            name=self.source_term,
        )

        return f * u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()

        A_grad_u_grad_u = self.left_hand_term(x1, x2, w)
        f_u = self.right_hand_term(x1, x2, w)
        residual = 0.5 * A_grad_u_grad_u - f_u

        if self.boundary_condition == "homogeneous_neumann":
            u = self.get_variables(w, "w")
            residual += u**2

        return residual

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        if self.source_term == "one" and self.symplecto_name == "ellipse":
            x1_0, x2_0 = 0, 0
            a, b = 1 / 0.9, 0.9
            rho = 0.5 / (1 / a**2 + 1 / b**2)
            return rho * (1 - ((x1 - x1_0) / a) ** 2 - ((x2 - x2_0) / b) ** 2)
        else:
            return 0 * x1


# ----------------------------------------------------------------------
#   CLASSE NETWORK - RESEAU DE NEURONES
#   approximation d'un symplectomorphisme
# ----------------------------------------------------------------------


class PINNs:
    DEFAULT_PINNS_DICT = {
        "learning_rate": 5e-3,
        "layer_sizes": [10, 20, 40, 20, 10],
        "rho_min": 0,
        "rho_max": 1,
        "file_name": "default",
        "symplecto_name": None,
        "to_be_trained": True,
        "source_term": "one",
        "boundary_condition": "homogeneous_dirichlet",
        "activation_type": "tanh",
    }

    # constructeur
    def __init__(self, **kwargs):
        PINNsDict = kwargs.get("PINNsDict", self.DEFAULT_PINNS_DICT)

        # copy (key, value) from DEFAULT_PINNS_DICT to PINNsDict if not present
        for key, value in self.DEFAULT_PINNS_DICT.items():
            if PINNsDict.get(key) is None:
                PINNsDict[key] = value

        # Storage file
        self.fig_storage = Path.cwd() / "outputs" / "PINNs" / "img"
        self.fig_storage.mkdir(parents=True, exist_ok=True)
        self.fig_storage = self.fig_storage / f"{PINNsDict['file_name']}"

        # Learning rate
        self.learning_rate = PINNsDict["learning_rate"]
        # Layer sizes
        self.layer_sizes = PINNsDict["layer_sizes"]

        # Geometry of the shape
        self.rho_min, self.rho_max = PINNsDict["rho_min"], PINNsDict["rho_max"]
        self.theta_min, self.theta_max = 0, 2 * torch.pi
        self.Vol = torch.pi * (self.rho_max - self.rho_min) ** 2
        self.symplecto_name = PINNsDict["symplecto_name"]

        # Source term of the Poisson problem
        self.source_term = PINNsDict["source_term"]

        # Boundary condition of the Poisson problem
        self.boundary_condition = PINNsDict["boundary_condition"]

        # create domain

        xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.0, 0.0], 1.0))

        # create PDE

        self.pde = Poisson_2D(xdomain, PINNsDict=PINNsDict)

        # create samplers

        x_sampler = sampling_pde.XSampler(pde=self.pde)
        mu_sampler = sampling_parameters.MuSampler(
            sampler=uniform_sampling.UniformSampling, model=self.pde
        )
        sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

        # create network

        activation_type = PINNsDict["activation_type"]
        network = pinn_x.MLP_x(
            pde=self.pde, layer_sizes=self.layer_sizes, activation_type=activation_type
        )
        self.pinn = pinn_x.PINNx(network, self.pde)

        # modify the get_w method of the network

        def get_w(pinn, data: domain.SpaceTensor, mu: torch.Tensor) -> torch.Tensor:
            x1, x2 = data.get_coordinates()
            image = self.disk_to_shape(data.x)
            return bc.apply_BC(
                pinn(image, mu),
                x1,
                x2,
                self.rho_min,
                self.rho_max,
                name=self.boundary_condition,
            )

        self.pinn.get_w = get_w.__get__(self.pinn, pinn_x.PINNx)

        # create losses and optimizers

        losses = pinn_losses.PinnLossesData(residual_f_loss=training.MassLoss())
        optimizers = training_tools.OptimizerData(
            learning_rate=self.learning_rate, decay=0.99
        )

        # create trainer
        self.trainer = training_x.TrainerPINNSpace(
            pde=self.pde,
            network=self.pinn,
            sampler=sampler,
            losses=losses,
            optimizers=optimizers,
            file_name=f"{PINNsDict['file_name']}.pth",
        )

        # flag for saving results
        self.save_results = kwargs.get("save_results", False)

    def disk_to_shape(self, x):
        x1, x2 = (x[:, i, None] for i in range(2))
        x1, x2 = metricTensors.apply_symplecto(x1, x2, name=self.symplecto_name)
        return torch.cat((x1, x2), axis=1)

    def inverse_disk_to_shape(self, x):
        x1, x2 = (x[:, i, None] for i in range(2))
        inverse_name = f"inverse_{self.symplecto_name}"
        x1, x2 = metricTensors.apply_symplecto(x1, x2, name=inverse_name)
        return torch.cat((x1, x2), axis=1)

    def get_u(self, x, y):
        data = domain.SpaceTensor(torch.cat((x, y), axis=1))
        mu = torch.ones((x.shape[0], 0))
        return self.pinn.get_w(data, mu)

    def get_error(self, x, y):
        data = domain.SpaceTensor(torch.cat((x, y), axis=1))
        mu = torch.ones((x.shape[0], 0))
        image = domain.SpaceTensor(self.disk_to_shape(data.x))
        return self.pinn.get_w(data, mu) - self.pde.reference_solution(image, mu)

    def train(self, epochs, n_collocation, **kwargs):
        start = time.perf_counter()
        self.trainer.train(epochs=epochs, n_collocation=n_collocation, **kwargs)
        return time.perf_counter() - start

    def plot_result(self, save_plots, error=False):
        makePlots.loss(self.trainer.losses.loss_history, save_plots, self.fig_storage)
        n_visu = 128 if device == "cuda" else 768

        def symplecto(x, y):
            return metricTensors.apply_symplecto(x, y, name=self.symplecto_name)

        def inverse(x, y):
            return metricTensors.apply_symplecto(
                x, y, name=f"inverse_{self.symplecto_name}"
            )

        makePlots.edp_contour(
            self.rho_min,
            self.rho_max,
            self.get_u,
            symplecto,
            inverse,
            save_plots,
            self.fig_storage / "u",
            n_visu=n_visu,
        )

        if error:
            makePlots.edp_contour(
                self.rho_min,
                self.rho_max,
                self.get_error,
                symplecto,
                inverse,
                save_plots,
                self.fig_storage / "error",
                n_visu=n_visu,
                colormap="gist_heat",
            )

        # makePlots.edp_contour(
        #     self.rho_min,
        #     self.rho_max,
        #     self.get_fv,
        #     lambda x, y: metricTensors.apply_symplecto(x, y, name=self.symplecto_name),
        #     lambda x, y: metricTensors.apply_symplecto(
        #         x, y, name=f"inverse_{self.symplecto_name}"
        #     ),
        #     save_plots,
        #     self.fig_storage / "fv",
        #     n_visu=n_visu,
        # )
