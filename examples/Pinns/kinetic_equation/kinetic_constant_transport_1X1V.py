from pathlib import Path

from scimba.equations import pde_1x1v_transport
from scimba.pinns import pinn_txv
from scimba.pinns.training_txv import TrainerPinnKinetic
import scimba.pinns.pinn_losses as pinn_losses
import scimba.nets.training_tools as training_tools

pde = pde_1x1v_transport.ConstantInX()

network = pinn_txv.MLP_txv(pde=pde, layer_sizes=[80, 80, 80, 80])
pinn = pinn_txv.PINNtxv(network, pde)

file_name = "testkinetic.pth"
(Path.cwd() / Path(TrainerPinnKinetic.FOLDER_FOR_SAVED_NETWORKS) / file_name).unlink(
    missing_ok=True
)

losses = pinn_losses.PinnLossesData(w_res=1.0)
optimizers = training_tools.OptimizerData(learning_rate=1e-2,decay=0.99)
trainer = TrainerPinnKinetic(
    pde=pde,
    network=pinn,
    losses =losses,
    optimizers=optimizers,
    file_name=file_name,
    batch_size=2500,
)

trainer.train(
    epochs=500,
    n_collocation=5000,
)

trainer.plot(reference_solution=True)
