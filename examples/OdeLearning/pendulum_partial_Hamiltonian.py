import copy as cp

import pylab as plt
import scimba.nets.mlp as mlp
import scimba.odelearning.abstract_ode as abstractode
import scimba.odelearning.generation_ode_data as generation_ode_data
import scimba.odelearning.generic_flux as genericflux
import scimba.odelearning.loss_odelearning as loss
import scimba.odelearning.training_diffphy_ode as training
import torch


class Pendulum_flux(genericflux.Flux):
    def __init__(self):
        super().__init__()

        self.nb_unknowns = 2
        self.nb_parameters = 1

    def forward(self, t, state, mu):
        omega = self.get_parameters(mu)
        q, p = self.get_variables(state)

        dq = p
        dp = -(omega**2) * q

        return self.set_derivatives(state, dq, dp)


class Pendulum_partialflux(genericflux.Flux):
    def __init__(self, net):
        super().__init__()

        self.net = cp.deepcopy(net)
        self.nb_unknowns = 2
        self.nb_parameters = 1

    def forward(self, t, state, mu):
        omega = self.get_parameters(mu)
        q, p = self.get_variables(state)

        inputs = self.prepare_network_inputs(q, omega)

        dq = p
        dp = -self.net(inputs)

        return self.set_derivatives(state, dq, dp)


class Pendulum(abstractode.ClassicODE):
    def random_init_data(self):
        q = 0.4 + 0.3 * torch.rand(1)
        p = 0
        return p, q

    def random_params(self):
        omega = 2.1 + torch.rand(1)
        return omega

    def plot_data(self, time, data):
        time = time.detach().cpu()
        data = data.detach().cpu()
        fig, axs = plt.subplots(1, 2, figsize=(14, 5))
        axs[0].plot(time, data[..., 0])
        axs[0].set_title("q dynamic")
        axs[1].plot(time, data[..., 1])
        axs[1].set_title("p dynamic")
        plt.show()


class MSELoss_RefOde(loss.LossODELearning_withdata):
    def __init__(self, ode):
        super().__init__(ode)
        self.loss_used = torch.nn.MSELoss()

    def apply(self, x, xref, t, mu):
        """
        x: shape(nb_time, nb_samples, nb_unknowns)
        """
        return self.loss_used(input=x, target=xref)


network = mlp.GenericMLP(2, 1, layer_sizes=[20, 20, 20, 20])
flux_ref = Pendulum_flux()
flux = Pendulum_partialflux(network)

oderef = Pendulum(flux_ref)
ode = Pendulum(flux)

ts = torch.linspace(
    start=0.0,
    end=2.0,
    steps=200,  # number of time steps per time batch
)

# one batch of initial conditions
initial_conditions = oderef.generate_initial_conditions(20)
mu = oderef.generate_parameters(20)
oderef.time_scheme(initial_conditions, ts, mu)

oderef.plot_data(ts, oderef.solution)


# generattion of data
data_ode = generation_ode_data.DataGenerate(
    oderef, T=2, num_time_batch=100, num_timestep_per_batch=5
)
data_ode.create_dataset(100)
loss_for_training = MSELoss_RefOde(ode)

# compute the model before training
initial_conditions = torch.tensor([0.5, 0.0])
mu = torch.tensor([2.5])
oderef.time_scheme(initial_conditions, ts, mu)
sol_ref = cp.deepcopy(oderef.solution.detach().cpu())

ode.time_scheme(initial_conditions, ts, mu)
sol_pre_train = cp.deepcopy(ode.solution.detach().cpu())

# training
trainer = training.Trainer_DF_ODE_with_data(
    model=ode, data=data_ode, loss_f=loss_for_training, nb_batch=5
)
trainer.train(epochs=50)

# compute the model after training
ode.time_scheme(initial_conditions, ts, mu)
sol_post_train = cp.deepcopy(ode.solution.detach().cpu())


def H(x):
    return x[..., 1] ** 2 + 0.5**2 * x[..., 0] ** 2


# plot
fig, axs = plt.subplots(2, 2, figsize=(14, 10))
ts = ts.detach().cpu()
axs[0, 0].plot(ts, sol_ref[..., 0].detach().cpu(), "r")
axs[0, 0].plot(ts, sol_pre_train[..., 0].detach().cpu(), "b+")
axs[0, 0].plot(ts, sol_post_train[..., 0].detach().cpu(), "g+")
axs[0, 0].set_title("q dynamic")
axs[0, 1].plot(ts, sol_ref[..., 1].detach().cpu(), "r")
axs[0, 1].plot(ts, sol_pre_train[..., 1].detach().cpu(), "b+")
axs[0, 1].plot(ts, sol_post_train[..., 1].detach().cpu(), "g+")
axs[0, 1].set_title("p dynamic")
plt.show()
