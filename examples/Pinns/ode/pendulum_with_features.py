from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations.pdes import AbstractODE


class AmortizedPendulum_simple(AbstractODE):
    def __init__(self):
        super().__init__(
            nb_unknowns=1,
            time_domain=[0, 20.0],
            nb_parameters=2,
            parameter_domain=[[0.4, 0.400001], [2.0, 2.000001]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.t_min, self.t_max = self.time_domain[0], self.time_domain[1]
        self.data_construction = "sampled"

    def initial_condition(self, mu, **kwargs):
        c = 0.02
        lam, omega = self.get_parameters(mu)
        du0 = -c * lam * omega * 0.5
        return [
            0.5 * torch.ones_like(lam),
            du0 * torch.ones_like(lam),
        ]

    def residual(self, w, t, mu, **kwargs):
        c = 0.02
        lam, omega = self.get_parameters(mu)
        u = self.get_variables(w)
        u_t = self.get_variables(w, "w_t")
        u_tt = self.get_variables(w, "w_tt")
        return u_tt + 2 * c * lam * omega * u_t + omega * omega * u


    def make_data(self, n_data):
        # construit le jeu de données autrement qu'en samplant la solution de reference
        pass

    def reference_solution(self, t, mu):
        c = 0.02
        lam, omega = self.get_parameters(mu)
        return (
            0.5
            * torch.exp(-c * lam * omega * t)
            * torch.cos((1 - c * c * lam * lam) ** (0.5) * omega * t)
        )


def main():
    ode = AmortizedPendulum_simple()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name1 = "testff.pth"
    file_name2 = "testff2.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name1
        ).unlink(missing_ok=True)

    tlayers = [20,40,40,40,20]
    network1 = pinn_t.MLP_t(
        ode=ode,
        layer_sizes=tlayers,
        activation_type="sine"
    )
    pinns1 = pinn_t.PINNt(network1,ode)
    network2 = pinn_t.Fourier_t(
        ode=ode,
        layer_sizes=tlayers,
        activation_type="sine",
        nb_features= 20,
        mean_features={"t":0.0,"mu":0.0,"tmu":0.0},
        std_features={"t":1.0,"mu":0.0,"tmu":0.0},
    )
    pinns2 = pinn_t.PINNt(network2,ode)

    losses = pinn_losses.PinnLossesData(init_loss_bool=True,w_data=0,
        w_res=0.25,
        w_init=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=8e-3,
        decay=0.992)
    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=pinns1,
        sampler=sampler,
        losses = losses,
        optimizers = optimizers,
        file_name=file_name1,
        batch_size=4000,
    )

    if new_training:
        trainer.train(
            epochs=500, n_collocation=4000, n_init_collocation=2000, n_data=0
        )

    trainer.plot()

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name2
        ).unlink(missing_ok=True)

    trainer2 = training_t.TrainerPINNTime(
        ode=ode,
        network=pinns2,
        sampler=sampler,
        losses = losses,
        optimizers = optimizers,
        file_name=file_name2,
        batch_size=4000,
    )

    if new_training:
        trainer2.train(
            epochs=500, n_collocation=4000, n_init_collocation=2000, n_data=0
        )

    trainer2.plot(reference_solution=True)


if __name__ == "__main__":
    main()
