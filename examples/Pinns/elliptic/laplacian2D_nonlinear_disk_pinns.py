from pathlib import Path

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_x, pinn_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class NonlinearPoissonDisk2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.5, 1]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        f = self.get_parameters(mu)

        u = self.get_variables(w, "w")
        u_x = self.get_variables(w, "w_x")
        u_y = self.get_variables(w, "w_y")
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")

        x1_0, x2_0 = self.space_domain.large_domain.center

        term = 1 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2
        rhs = (2 * f**2 - 0.5 * f) * term - f**2

        return u * u_xx + u_x**2 + u * u_yy + u_y**2 + u + rhs

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        x1_0, x2_0 = self.space_domain.large_domain.center
        f = self.get_parameters(mu)
        return 0.5 * f * (1 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2)


def main():
    xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.5, 0.5], 1.0))
    pde = NonlinearPoissonDisk2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"

    new_training = True
    # new_training = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 40, 20]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(bc_loss_bool=True, w_res=1.0, w_bc=10.0)
    optimizers = training_tools.OptimizerData(learning_rate=5e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=2000,
    )

    if new_training:
        trainer.train(epochs=1000, n_collocation=2000, n_bc_collocation=2000)

    trainer.plot(20000, reference_solution=True)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
