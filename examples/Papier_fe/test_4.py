from pathlib import Path

import matplotlib.pyplot as plt
import scimba.equations.domain as domain
import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import pdes

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

# +
PI = 3.14159265358979323846

torch.set_default_dtype(torch.double)
torch.set_default_device(device)


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 1.00001]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.compute_normals = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u_external = self.get_variables(w, label=0)
        u_internal_x = self.get_variables(w, "w_x", label=1)
        u_internal_y = self.get_variables(w, "w_y", label=1)
        n_x, n_y = x.get_normals(label=1)
        return u_external, u_internal_x * n_x + u_internal_y * n_y

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        mu_theta = self.get_parameters(mu)

        mu1 = torch.sin(mu_theta)
        mu2 = torch.sin(mu_theta)
        exp = torch.exp(-((x2 - mu2) ** 2 + (x1 - mu1) ** 2) / 5)
        f = exp
        return u_xx + u_yy + f

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        return 0.0 * x1


# domain creation
xdomain = domain.DiskBasedDomain(2, [0.0, 0.0], 1.0)
fulldomain = domain.SpaceDomain(2, xdomain)


# Hole and its signed distance function (sdf)
class Hole(domain.SignedDistance):
    def __init__(self):
        super().__init__(dim=2)

    def sdf(self, x):
        x1, x2 = x.get_coordinates()
        center = [0.0, 0.0]
        radius = 0.5
        return (x1 - center[0]) ** 2 + (x2 - center[1]) ** 2 - radius**2


# parametric curve that describes the hole
def paramcurve_bchole(t):
    radius = 0.5
    return torch.cat(
        [
            radius * torch.cos(2 * PI * t),
            radius * torch.sin(2 * PI * t),
        ],
        axis=1,
    )


def get_trainer():
    sdf = Hole()
    hole = domain.SignedDistanceBasedDomain(2, [[0.0, 1.0], [0.0, 1.0]], sdf)
    bc_hole = domain.ParametricCurveBasedDomain(2, [[0.0, 1.0]], paramcurve_bchole)
    hole.add_bc_subdomain(bc_hole)

    fulldomain.add_hole(hole)

    pde = Poisson_2D(fulldomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "testdomain_hole.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    x, mu = sampler.bc_sampling(1000)
    x1, x2 = x.get_coordinates(label=0)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="r")
    x1, x2 = x.get_coordinates(label=1)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="b")
    # x1, x2 = x.get_coordinates(label=3)
    # plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="y")
    plt.show()

    x, mu = sampler.sampling(1000)
    x1, x2 = x.get_coordinates(label=0)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="r")
    plt.show()

    tlayers = [40, 40, 40, 40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="tanh")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=True, w_res=1.0, w_bc=30.0
    )  # , adaptive_weights="annealing"
    optimizers = training_tools.OptimizerData(learning_rate=1.7e-2, decay=0.99)

    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=8000,
    )

    return sampler, trainer


if __name__ == "__main__":
    sampler, trainer = get_trainer()

    x, mu = sampler.bc_sampling(1000)
    x1, x2 = x.get_coordinates(label=0)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="r")
    x1, x2 = x.get_coordinates(label=1)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="b")
    # x1, x2 = x.get_coordinates(label=3)
    # plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="y")
    plt.show()

    x, mu = sampler.sampling(1000)
    x1, x2 = x.get_coordinates(label=0)
    plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="r")
    plt.show()

    trainer.train(epochs=2000, n_collocation=8000, n_bc_collocation=8000)
    ### tooo doo bug
    trainer.plot(20000)
    trainer.plot_2d_contourf()
