import numpy as np
import scipy.stats as stats


class Mesh:
    def __init__(self, N, a, b):
        self.N = N
        self.a = a
        self.b = b
        self.m = np.linspace(a, b, N)
        self.h = np.abs(self.m[1] - self.m[0])
        self.mid = (self.a + self.b) / 2
        self.L = self.b - self.a


class DataGenerator:
    def __init__(
        self,
        nx,  # number of points
        alpha0,  # nonlinearity coefficient
        a=0,  # start of the interval
        b=1,  # end of the interval
        k=lambda u: u**4,  # nonlinearity
        seed=None,  # random seed
    ):
        self.a = a
        self.b = b
        self.nx = nx
        self.k = k
        self.alpha0 = alpha0

        self.mesh = Mesh(nx, self.a, self.b)
        self.dist = stats.norm(loc=0.0, scale=1.0)

        self.normalisation_for_f = 1000 * self.alpha0
        # self.normalisation_for_f = self.alpha0

        self.seed = seed

    def make_XY(self, batch_size):
        alpha = self.generate_gaussian_mix(batch_size)
        U = self.generate_gaussian_mix(batch_size)
        f_nor = self.elliptic(U, alpha) / self.normalisation_for_f
        X = np.stack([f_nor, alpha], axis=2)
        Y = U[:, :, None]
        return X, Y

    def elliptic(self, U, alpha_ref):
        alpha = np.array(alpha_ref)
        Um = U[:, :-1]
        Up = U[:, 1:]
        Am = alpha[:, :-1]
        Ap = alpha[:, 1:]

        Am = np.concatenate([alpha[:, :1], Am], axis=1)
        Ap = np.concatenate([Ap, alpha[:, -1:]], axis=1)

        Um = np.concatenate([U[:, :1], Um], axis=1)
        Up = np.concatenate([Up, U[:, -1:]], axis=1)

        # diffusion terms
        Kp = 0.5 * (self.k(U) + self.k(Up))
        Km = 0.5 * (self.k(Um) + self.k(U))
        c_Ap = self.alpha0 * 0.5 * (alpha + Ap)
        c_Am = self.alpha0 * 0.5 * (Am + alpha)

        Dp = c_Ap * Kp * (Up - U) / self.mesh.h
        Dm = c_Am * Km * (U - Um) / self.mesh.h
        diffusion = (Dm - Dp) / self.mesh.h

        return diffusion + U

    def generate_gaussian_mix(self, nb_data):
        np.random.seed(self.seed)
        nb_gauss_max = 6

        std = np.random.uniform(
            low=0.025,
            high=0.07,
            size=[nb_gauss_max, nb_data, 1],
        )
        mean = np.random.uniform(
            low=self.mesh.mid - 0.25 * self.mesh.L,
            high=self.mesh.mid + 0.25 * self.mesh.L,
            size=[nb_gauss_max, nb_data, 1],
        )
        x = (self.mesh.m[None, None, :] - mean) / std

        gauss = self.dist.pdf(x)
        mask = np.random.uniform(
            low=0,
            high=1.75,
            size=[nb_gauss_max - 1, nb_data, 1],
        )
        mask = np.floor(mask)  # un peu plus de 0 que de 1 dans ce masque

        mask = np.concatenate(
            [np.ones([1, nb_data, 1]), mask], axis=0
        )  # pour avoir au moins une gaussienne à chaque fois
        gauss *= mask
        res = np.sum(gauss, axis=0)  # on somme les gaussiennes

        return 0.5 + res / (np.max(res, axis=1)[:, None] + 0.1)

    def G(self, U, f, alpha):
        ellip = self.elliptic(U, alpha)
        return ellip - f

    def get_G_for_scipy(self, f, alpha):
        return lambda U: self.elliptic(U, alpha) - self.normalisation_for_f * f

    def plot_data(self, X, Y):
        import matplotlib.pyplot as plt

        f_nor = X[:, :, 0]
        f = f_nor * self.normalisation_for_f
        alpha = X[:, :, 1]
        U = Y[:, :, 0]
        g = self.G(U, f, alpha)
        nb = min(10, X.shape[0])

        fig, ax = plt.subplots(nb, 4, figsize=(16, nb))

        for i in range(nb):
            ax[i, 0].plot(self.mesh.m, f_nor[i, :])
            ax[i, 1].plot(self.mesh.m, alpha[i, :])
            ax[i, 2].plot(self.mesh.m, U[i, :])
            ax[i, 3].plot(self.mesh.m, g[i, :])
        ax[0, 0].set_title("f_nor")
        ax[0, 1].set_title("alpha")
        ax[0, 2].set_title("U")
        ax[0, 3].set_title("G(U)")
        fig.tight_layout()


def test_data_generation(nx, alpha0, n_data):
    gen = DataGenerator(nx, alpha0)
    gen.plot_data(*gen.make_XY(n_data))


if __name__ == "__main__":
    test_data_generation(nx=100, alpha0=2, n_data=10)
