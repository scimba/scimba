import torch
from torch import nn

from scimba.nets.activation import IsotropicRadial, AnisotropicRadial

def test_activation_radial():
    mu= torch.tensor([[1.0,2.0]])
    x= torch.tensor([[1.0,0.2],[0.1,0.1],[3.0,4.6]])


    ac=IsotropicRadial(in_size=2,m=mu)
    ac.forward(x)

    ac=AnisotropicRadial(in_size=2,m=mu)
    ac.forward(x)
    assert (1==1)

if __name__ == "__main__":
    test_activation_radial()