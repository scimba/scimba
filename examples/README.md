# Solve Vlasov equation with physics-informal network

equation

$$
\frac{df}{dt} + v \frac{df}{dx} + E \frac{df}{dv} = 0
$$

The value of $E$ is given, computed from a numerical simulation. We will run first the Vlasov-Poisson simulation

```python
import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
import vlasovpoisson.vlasov as vl

# Set grid
nx, nv = 64, 64
xmin, xmax = 0.0, 4*np.pi
vmin, vmax = -6., 6.

# Create Vlasov-Poisson simulation
sim = vl.VlasovPoisson(xmin, xmax, nx, vmin, vmax, nv)

# Initialize distribution function
X, V = np.meshgrid(sim.x, sim.v)
eps, kx = 0.001, 0.5
f = (1.0+eps*np.cos(kx*X))/(2.0*np.pi)* np.exp(-0.5*V*V)
f = f.T

# Set time domain
nstep = 1000
t, dt = np.linspace(0.0, 50.0, nstep, retstep=True)

# Set the output distribution function and electric field
F = np.zeros((nstep,nx,nv))
F[0,:] = f
efield = np.zeros((nstep,nx))

# Run simulation
nrj, F, efield = sim.run(f, nstep, dt, F, efield)

# Plot energy
plt.figure()
plt.plot(t, nrj, label=r"$\int E(x) dx$")
plt.plot(t, -5.95-0.34*t)
plt.legend()
```

## Save data in HDF5 file

```python
HDF5filename = f'VP_Nx{nx}_Nv{nv}.h5'
with h5.File(HDF5filename,'w') as fh5:
    fh5.create_dataset("fdistribu",data=F)
    fh5.create_dataset("xgrid",data=sim.x)
    fh5.create_dataset("vgrid",data=sim.v)
    fh5.create_dataset("timegrid",data=t)
    fh5.create_dataset("efield",data=efield)
```

```python
%ls *.h5
```

```python
import os
import sys
import torch
import yaml
import pytest

current = os.getcwd()
pytorchdir = os.path.join(current, "..")
sys.path.append(pytorchdir)

import pytorch.Nets.MLP as MLP
import pytorch.PINNs.Domain as Domain
import pytorch.PINNs.PDE_1X1V_transport as PDE_1X1V_transport
from pytorch.PINNs.Training_txv import Trainer_PINN_txv
from pytorch.PINNs.PINN_txv import PINN_txv
import pytorch.Nets.Training as Training

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
```

## set parameters for the simulation

```python
input_filename = os.path.join(current, "config_bydefault_PDE_1X1V_vlasov.yaml")
input_conf = yaml.safe_load(open(input_filename))

folder_for_saved_networks = input_conf["folder_for_saved_networks"]
n_epochs = input_conf["NN"]["epochs"]
n_data = input_conf["NN"]["n_data"]
n_colloc = input_conf["NN"]["n_collocation"]
save_dir_name = os.path.join(f"{folder_for_saved_networks}",
    f"RESU_NEPOCHS{n_epochs}_NDATA{n_data}_NCOLLOC{n_colloc}")
save_dir = Path(save_dir_name)
save_dir.mkdir(parents=True, exist_ok=True)

print(f"==> Results saved in {save_dir.absolute()}")
input_conf["folder_for_saved_networks"] = str(save_dir.absolute())

filename_simu = os.path.join(current, input_conf["filename_simu"])
print(f"==> Simulation results load in {filename_simu}")
```

```python
pde = PDE_1X1V_transport.Vlasov(filename_simu)

config = input_conf

network = PINN_txv(
    net=MLP.Generic_MLP, pde=pde, layer_sizes=config["NN"]["layer_sizes"]
)

trainer = Trainer_PINN_txv(
    pde=pde,
    network=network,
    folder_for_saved_networks=config["folder_for_saved_networks"],
    file_name=config["filename_save"],
    learning_rate=config["Trainer"]["learning_rate"],
    decay=config["Trainer"]["decay"],
    batch_size=config["Trainer"]["batch_size"],
    w_data=config["Trainer"]["w_data"],
    w_res=config["Trainer"]["w_res"],
    w_bc=config["Trainer"]["w_init"],
    w_init=config["Trainer"]["w_init"],
)

trainer.train(
    epochs=config["NN"]["epochs"],
    n_collocation=config["NN"]["n_collocation"],
    n_data=config["NN"]["n_data"],
    n_init_collocation=config["NN"]["n_init_collocation"],
    n_bc_collocation=config["NN"]["n_bc_collocation"],
)
```

```python
trainer.plot_2()
```

## Integrating Tensorboard with PINNs

- 1: Install required libraries 

```bash
pip install torch torchvision tensorboard
```

- 2: `SummaryWriter` allows writing logs for TensorBoard.

```pythonO
from torch.utils.tensorboard import SummaryWriter
```

- 3: In the main script, create and configure a summary writer 

```python
writer = SummaryWriter('runs/experiment_1')
```

- 4: Pass the writer object to the training function/class in "Training_txv.py"

```python
trainer = test_PINN_txv(
    config=input_conf,
    train=True,
    pde=PDE_1X1V_transport.constant_in_x()
)
trainer.train(writer, epochs=500)
```

- 5: Log Training Metrics "we can log different metrics in future"

```python
writer.add_scalar('Loss/train', self.loss.item(), epoch)
```

- 6: Close the `SummaryWriter`

Close the writer at the end of the script using:

```python
writer.close()
```

- 7: Start TensorBoard 

Run the following command in the terminal:

```bash
tensorboard --logdir runs/experiment_1
```

Access TensorBoard using the URL in a web browser. example http://localhost:6006/

## Notes

In case you encounter a problem with the port being busy, etc.:
- Start TensorBoard on a different port using `--port`:

  ```bash
  tensorboard --logdir=runs --port=6007
  ```

- If you want to use port 6006, for example, and need to find out if it's busy, you can run the following command in Linux:

  ```bash
  lsof -i :6006 
  ```

  If it's busy, you can kill the job, for example:

  ```bash
  kill 12332
  ```
