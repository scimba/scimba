import os
from pathlib import Path

import yaml
from applications.scimba_plasma.vlasovpoisson import (
    PINNvlasov_sampling,
    PINNvlasov_training,
    PINNvlasov_equations,
)
from scimba.nets.training_tools import OptimizerData
from scimba.pinns import pinn_txv
from scimba.pinns.pinn_losses import PinnLossesData

test_path = Path(__file__).parent


def test_PINN_txv(tmp_path):
    os.chdir(tmp_path)
    input_filepath = test_path / "config_bydefault_PDE_1X1V_vlasov_for_tests.yaml"
    with open(input_filepath, "r") as f:
        config = yaml.safe_load(f)

    filename_simu = test_path / config['VlasovPoisson']['file']
    print(f"==> Simulation results load in {filename_simu}")

    new_load_simu = False
    time_domain = [config["VlasovPoisson"]["time_init"], config["VlasovPoisson"]["time_end"]]
    pde = PINNvlasov_equations.Vlasov_externalE(filename_simu, new_load_simu, time_domain)
    sampler = PINNvlasov_sampling.PdeGrid1D1VSampler(pde)
    pde.gives_sampler(sampler)

    layer_sizes = config["NN"]["layer_sizes"]

    network = pinn_txv.MLP_txv(pde=pde, layer_sizes=layer_sizes)
    pinn = pinn_txv.PINNtxv(network, pde)

    file_name = "test.pth"

    pinn = pinn_txv.PINNtxv(network, pde)

    losses = PinnLossesData(
        bc_loss_bool=True,
        init_loss_bool=True,
        data_loss_bool=True,
        w_data=config["Trainer"]["w_data"],
        w_res=config["Trainer"]["w_res"],
        w_bc=config["Trainer"]["w_bc"],
        w_init=config["Trainer"]["w_init"],
    )
    optimizers = OptimizerData(
        learning_rate=config["Trainer"]["learning_rate"],
        decay=config["Trainer"]["decay"],
    )
    trainer = PINNvlasov_training.TrainerPinnVlasov(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=config["Trainer"]["batch_size"],
    )

    epochs = config["NN"]["epochs"]
    n_collocation = config["NN"]["n_collocation"]
    n_data = config["NN"]["n_data"]
    n_init_collocation = config["NN"]["n_init_collocation"]
    n_bc_collocation = config["NN"]["n_bc_collocation"]

    trainer.train(
        epochs=epochs,
        n_collocation=n_collocation,
        n_data=n_data,
        n_init_collocation=0,
        n_bc_collocation=0,
    )

if __name__ == "__main__":
    test_PINN_txv(tmp_path=Path.cwd())
