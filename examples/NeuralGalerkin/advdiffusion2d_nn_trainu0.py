import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.neuralgalerkin.neural_galerkin_x as ng
import scimba.pinns.pinn_x as pinn_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
import time

PI = 3.14159265358979323846


class AdvectionDiffusion2D(pdes.AbstractPDEx):
    def __init__(self, xdomain, p_domain=[[1.0, 1.00001]]):
        super().__init__(
            nb_unknowns=1,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative = True
        self.second_derivative = True

    def bc_residual(self, w, x, mu):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        u_x = self.get_variables(w, "w_x")
        u_xx = self.get_variables(w, "w_xx")
        u_y = self.get_variables(w, "w_y")
        u_yy = self.get_variables(w, "w_yy")
        ax = -2.0 * PI * x2  # mu[:,0,None]
        ay = 2.0 * PI * x1  # mu[:,0,None]
        D = 0.02  # mu[:,1,None]
        return -(ax * u_x + ay * u_y - D * (u_xx + u_yy))



def sol_ref(t, x, mu):
    x1,x2 = x.get_coordinates()
    x1_t = 0.4 * torch.cos(2.0 * PI * t)
    x2_t = 0.4 * torch.sin(2.0 * PI * t)
    D = 0.02  # mu[:,1,None]
    sig0 = 0.1
    c_t = sig0 * sig0 / (4 * D * t + sig0 * sig0)
    sig2 = 4 * D * t + sig0 * sig0
    f = torch.exp(-((x1 - x1_t) ** 2.0 + (x2 - x2_t) ** 2.0) / (sig2))
    return (c_t * f + 1.0)


def init(x, mu):
    x1,x2 = x.get_coordinates()
    x1_t = 0.4
    x2_t = 0.0
    sig0 = 0.1
    c_t = 1.0
    sig2 = sig0 * sig0
    f = torch.exp(-((x1 - x1_t) ** 2.0 + (x2 - x2_t) ** 2.0) / (sig2))

    return (c_t * f + 1.0)  # x * (1 - x)


def main():
    xdomain = domain.SpaceDomain(2,domain.DiskBasedDomain(2, [0.0, 0.0], 0.7))
    pde = AdvectionDiffusion2D(xdomain=xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )

    # tlayers = [20, 40, 40, 20]
    tlayers = [40,40,40,40,40]
    network = pinn_x.MLP_x(
        pde=pde,
        layer_sizes=tlayers,
        activation_type="tanh",
    )
    pinns = pinn_x.PINNx(network, pde, init_net_bool=False)
    # create the model
    model = ng.NeuralGalerkin_x(
        pde,
        x_sampler,
        mu_sampler,
        pinns,
        scheme="rk2",
        type_init=1,
        matrix_regulatization=1.0e-9,
        ls_bool = False,
        subsample_bool = True,
        subsample =3000,
    )

    # solve the problem
    start=time.time()
    model.compute_initial_data(w0=init,
                               lr_init=1.6e-2, 
                               epoch_init=50,
                               n_collocation=3000)
    model.time_loop(dt=0.0015, 
                    T=0.15,
                    sol_exact=sol_ref,
                    n_collocation =3000) #0.4
    end=time.time()
    print("temps: ",end-start)

    model.plot(n_visu=20000, sol_exact=sol_ref)


if __name__ == "__main__":
    main()
