from math import pi as PI
from pathlib import Path

import matplotlib.pyplot as plt
import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_x, pinn_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.compute_normals = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u_top = self.get_variables(w, label=0)
        u_x_bottom = self.get_variables(w, "w_x", label=1)
        u_y_bottom = self.get_variables(w, "w_y", label=1)
        n_x, n_y = x.get_normals(label=1)
        return u_top, u_x_bottom * n_x + u_y_bottom * n_y

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 1.0
        return u_xx + u_yy + f

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        return 0.0 * x1


# domain creation
xdomain = domain.DiskBasedDomain(2, [0.0, 0.0], 1.0)
fulldomain = domain.SpaceDomain(2, xdomain)


def f(t):
    return torch.cat([torch.cos(2.0 * PI * t), torch.sin(2.0 * PI * t)], axis=1)


bc1 = domain.ParametricCurveBasedDomain(2, [[0.0, 0.5]], f)
bc2 = domain.ParametricCurveBasedDomain(2, [[0.5, 1.0]], f)

fulldomain.add_bc_subdomain(bc1)
fulldomain.add_bc_subdomain(bc2)

# start computations

pde = Poisson_2D(fulldomain)
x_sampler = sampling_pde.XSampler(pde=pde)
mu_sampler = sampling_parameters.MuSampler(
    sampler=uniform_sampling.UniformSampling, model=pde
)
sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

file_name = "testdomain_DN.pth"
# new_training = False
new_training = True

if new_training:
    (
        Path.cwd()
        / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    ).unlink(missing_ok=True)

x, mu = sampler.bc_sampling(1000)
x1, x2 = x.get_coordinates(label=0)
plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="r")
x1, x2 = x.get_coordinates(label=1)
plt.scatter(x1.detach().numpy(), x2.detach().numpy(), color="b")
plt.show()

tlayers = [40, 40, 40, 40]
network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="tanh")
pinn = pinn_x.PINNx(network, pde)

losses = pinn_losses.PinnLossesData(bc_loss_bool=True, w_res=1.0, w_bc=30.0)
optimizers = training_tools.OptimizerData(learning_rate=1.8e-2, decay=0.99)

trainer = training_x.TrainerPINNSpace(
    pde=pde,
    network=pinn,
    sampler=sampler,
    losses=losses,
    optimizers=optimizers,
    file_name=file_name,
    batch_size=8000,
)

trainer.train(epochs=1500, n_collocation=6000, n_bc_collocation=6000)
trainer.plot(50000)
