import numpy as np

#--------------------------------------------------
# search index position
#--------------------------------------------------
def search_pos(x_value, x_array):
    """ Search index position"""

    x_indx = np.where(x_array >= x_value)
    if (np.size(x_indx) != 0):
        x_indx = x_indx[0][0]
    else:
        x_indx = np.size(x_array) - 1

    return x_indx
#end def search_pos
