from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations.pdes import AbstractODE


class HamiltonianPendulum(AbstractODE):
    def __init__(self):
        super().__init__(
            nb_unknowns=2,
            time_domain=[0, 2.0],
            nb_parameters=2,
            parameter_domain=[[1.0, 1.25], [1.0, 1.25]],
        )

        self.first_derivative = True
        self.second_derivative = False
        self.t_min, self.t_max = self.time_domain[0], self.time_domain[1]
        self.data_construction = "sampled"

    def initial_condition(self, mu, **kwargs):
        c = 0.02
        lam, omega = self.get_parameters(mu)
        du0 = -0.5 * c * lam * omega
        return torch.cat(
            [
                0.5 * torch.ones_like(lam),
                du0 * torch.ones_like(lam),
            ],
            axis=1,
        )

    def residual(self, w, t, mu, **kwargs):
        c = 0.02

        lam, omega = self.get_parameters(mu)

        q, p = self.get_variables(w)
        q_t, p_t = self.get_variables(w, "w_t")

        return torch.cat(
            (q_t - p, p_t + omega**2 * q + 2 * c * lam * omega * p), axis=1
        )

    def make_data(self, n_data):
        pass

    def reference_solution(self, t, mu):
        c = 0.02
        lam, omega = self.get_parameters(mu)
        arg = torch.sqrt(1 - c**2 * lam**2)
        return torch.cat(
            [
                0.5 * torch.exp(-c * lam * omega * t) * torch.cos(arg * omega * t),
                -0.5
                * (
                    omega
                    * torch.exp(-c * lam * omega * t)
                    * (
                        arg * torch.sin(arg * omega * t)
                        + c * lam * torch.cos(arg * omega * t)
                    )
                ),
            ],
            axis=1,
        )


def main():
    ode = HamiltonianPendulum()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name = "test.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [80, 80, 40, 40, 20]
    network = pinn_t.MLP_t(ode=ode, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_t.PINNt(network, ode)

    losses = pinn_losses.PinnLossesData(
        init_loss_bool=True, w_data=0, w_res=0.25, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(
        learning_rate=1e-2, decay=0.992, switch_to_LBFGS=True
    )
    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=300, n_collocation=4000, n_init_collocation=2000, n_data=0)

    trainer.plot(reference_solution=True)


if __name__ == "__main__":
    main()
