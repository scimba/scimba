# 1D Laplacian with parametric source

We solve the PDE:

$$- u^{''}(x)= a sin(2 k\pi x) + b sin(2 (k+1)\pi x) +  csin(2 (k+2)\pi x)$$

on $x\in[0,1], a\in [0.0,1.0], b\in[0.0,0.5], c\in[0.0,0.1]$.

We learn $u(t,x,a,b,c)$.

**Parametric model:**

$$u_{net}(t,\alpha) = x (1 -x ) u_{\theta}(t,\alpha)$$


```{literalinclude} ../../examples/PINNs/elliptic/laplacian1D_pinns.py
```
