from pathlib import Path

import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.sampling.data_sampling_pde_tx as data_sampling_pde_tx
import scimba.sampling.sampling_ode as sampling_ode
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.nets.training_tools import OptimizerData
from scimba.neural_operators import deeponet_tx
from scimba.pinns import pinn_tx
from scimba.pinns.pinn_losses import PinnLossesData
from scimba.pinos import pino_tx, training_tx
from scimba.sampling.sampling_functions import (
    ParametricFunction_tx,
    ParametricFunction_x,
)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Heat_equation(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 0.03],
        xdomain=domain.SpaceDomain(1, domain.SquareDomain(1, [[0, 2]])),
        p_domain=[[0.5, 0.50001]],
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u_t = self.get_variables(w, "w_t")
        u_xx = self.get_variables(w, "w_xx")
        f = kwargs.get("f", None)
        return u_t - alpha * u_xx - f

    def bc_residual(self, w, t, x, mu, **kwargs) -> torch.Tensor:
        return self.get_variables(w)

    def initial_condition(
        self, x: torch.Tensor, mu: torch.Tensor, **kwargs
    ) -> torch.Tensor:
        t0 = 0.02
        alpha = self.get_parameters(mu)
        return (1.0 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1) ** 2) / (4 * alpha * t0)
        )

    def post_processing(
        self,
        t: torch.Tensor,
        data: domain.SpaceTensor,
        mu: torch.Tensor,
        w: torch.Tensor,
    ) -> torch.Tensor:
        x = data.x
        return self.initial_condition(x, mu) + x * (2 - x) * t * w

    def make_data(self, n_data):
        pass


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = Heat_equation(tdomain=[0.0, 0.03], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )

    class Source(ParametricFunction_tx):
        def __init__(
            self, dim_f=1, dim_x=1, dim_p=2, p_domain=[[0.5, 1.0], [0.8, 1.3]]
        ):
            super().__init__(dim_f, dim_x, dim_p, p_domain)

        def __call__(
            self, t: torch.Tensor, x: torch.Tensor, params: torch.Tensor
        ) -> torch.Tensor:
            x = x.get_coordinates()
            mu1, mu2 = self.get_parameters(params)
            return 0.1 * mu1 * torch.sin(t) * torch.exp(-((x - mu2) ** 2.0) / 0.05)

    class Boundary(ParametricFunction_x):
        def __init__(self):
            super().__init__(dim_f=1, dim_x=1, dim_p=0, p_domain=[])

        def __call__(self, x, params):
            x = x.get_coordinates()
            return 0 * x

    class Initial(ParametricFunction_x):
        def __init__(self, dim_f=1, dim_x=1, dim_p=1, p_domain=[[0.5, 0.5 + 1e-5]]):
            super().__init__(dim_f, dim_x, dim_p, p_domain)

        def __call__(self, x: torch.Tensor, params: torch.Tensor) -> torch.Tensor:
            x = x.get_coordinates()
            alpha = self.get_parameters(params)
            t0 = 0.02
            return (1.0 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
                -((x - 1) ** 2) / (4 * alpha * t0)
            )

    pde_sampler = data_sampling_pde_tx.pde_tx_data(
        sampler_t=t_sampler,
        sampler_x=x_sampler,
        sampler_params=mu_sampler,
        source=Source(),
        boundary=Boundary(),
        initial=Initial(),
        n_sensor=120,
        n_sensor_bc=80,
        n_sensor_ini=80,
    )

    no_network = deeponet_tx.DeepONetSpaceTime(
        net=pinn_tx.MLP_tx,
        pde=pde,
        pde_sampler=pde_sampler,
        lat_size=3,
        activation_type="sine",
        # encoder_type="PointNet",
        decoder_type="nonlinear",
        layers_b=[60, 40, 20, 20],
        layers_t=[20, 60, 60, 20],
    )

    network = pino_tx.PINOtx(no_network, pde)

    file_name = "test.pth"
    new_training = True
    # new_training = False

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINOSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    losses = PinnLossesData(w_res=1.0)
    optimizers = OptimizerData(learning_rate=5e-3, decay=0.99)

    trainer = training_tx.TrainerPINOSpaceTime(
        network=network,
        pde=pde,
        sampler=pde_sampler,
        batch_size=1000,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
    )

    if train:
        trainer.train(epochs=1200, n_collocation_f=5, n_collocation_tx=600)

    return network, trainer


if __name__ == "__main__":

    class reference(ParametricFunction_x):
        def __init__(self, dim_f=1, dim_x=1, dim_p=1, p_domain=[[0.5, 0.5 + 1e-5]]):
            super().__init__(dim_f, dim_x, dim_p, p_domain)

        def __call__(
            self, t: torch.Tensor, x: torch.Tensor, params: torch.Tensor
        ) -> torch.Tensor:
            x = x.get_coordinates()
            alpha = self.get_parameters(params)
            t0 = 0.02
            return (1.0 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
                -((x - 1) ** 2) / (4 * alpha * t0)
            )

    network, trainer = main()
    trainer.plot(reference_solution=reference())
