from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import pdes


class SimpleOde_H1Loss(pdes.AbstractODE):
    r"""
    .. math::

        \frac{u}{dt} + \mu u = 0

    """

    def __init__(self):
        super().__init__(
            nb_unknowns=1,
            time_domain=[0, 10.0],
            nb_parameters=1,
            parameter_domain=[[0.5, 1]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.t_min, self.t_max = self.time_domain[0], self.time_domain[1]
        self.data_construction = "sampled"

    def initial_condition(self, mu, **kwargs):
        return torch.ones_like(mu[:, 0, None])

    def residual(self, w, t, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u = self.get_variables(w)
        u_t = self.get_variables(w, "w_t")
        u_tt = self.get_variables(w, "w_tt")
        l2_res = u_t + alpha * u
        h1_res = u_tt + alpha * u_t
        return torch.cat((l2_res, h1_res), axis=1)

    def post_processing(self, t, mu, w):
        return self.initial_condition(mu) + t * w

    # construit le jeu de données autrement qu'en samplant la solution de reference
    def make_data(self, n_data):
        pass

    def reference_solution(self, t, mu):
        alpha = self.get_parameters(mu)
        return torch.exp(-alpha * t)


def main():
    ode = SimpleOde_H1Loss()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name = "test.pth"
    new_training = True
    # new_training = False

    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 20, 20, 20]
    network = pinn_t.MLP_t(ode=ode, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_t.PINNt(network, ode)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(
        learning_rate=9e-3, decay=0.992, switch_to_LBFGS=True
    )
    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=2000,
    )

    if train:
        trainer.train(epochs=500, n_collocation=2000, n_init_collocation=2000)

    trainer.plot(random=True, reference_solution=True)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
