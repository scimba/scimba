from pathlib import Path
import torch
from scimba.numericalmethods import mesh, finite_element
from scimba.equations import domain
import matplotlib.pyplot as plt

domain = domain.SquareDomain(1,[[0.0,1.0]])
Mh = mesh.UniformMesh(domain,[10])
Mh.build()

LocQ=mesh.LocalQuad1D(3)
Q = mesh.Sampling_MeshQuad1D(LocQ,Mh)

LocB1 = finite_element.LocalLagrangeBasis1D(0,Mh[5],LocQ)
LocB2 = finite_element.LocalLagrangeBasis1D(1,Mh[5],LocQ)
LocB3 = finite_element.LocalLagrangeBasis1D(2,Mh[5],LocQ)
LocB4 = finite_element.LocalLagrangeBasis1D(3,Mh[5],LocQ)
x= torch.linspace(LocB1.a,LocB1.b,100)
plt.plot(x,LocB1.forward(x))
plt.plot(x,LocB2.forward(x))
plt.plot(x,LocB3.forward(x))
plt.plot(x,LocB4.forward(x))
plt.show()

Glob1 =finite_element.GlobaleLagrangeBasis1D(0,Mh,LocQ)
Glob2 =finite_element.GlobaleLagrangeBasis1D(1,Mh,LocQ)
Glob3 =finite_element.GlobaleLagrangeBasis1D(5,Mh,LocQ)
Glob4 =finite_element.GlobaleLagrangeBasis1D(9,Mh,LocQ)
Glob5 =finite_element.GlobaleLagrangeBasis1D(10,Mh,LocQ)
x= torch.linspace(0.0,1.0,500)
plt.plot(x,Glob1.forward(x))
plt.plot(x,Glob2.forward(x))
plt.plot(x,Glob3.forward(x))
plt.plot(x,Glob4.forward(x))
plt.plot(x,Glob5.forward(x))
plt.show()

Glob11 =finite_element.GlobaleLagrangeBasis1D(11,Mh,LocQ)
Glob12 =finite_element.GlobaleLagrangeBasis1D(12,Mh,LocQ)
Glob21 =finite_element.GlobaleLagrangeBasis1D(19,Mh,LocQ)
Glob22 =finite_element.GlobaleLagrangeBasis1D(20,Mh,LocQ)
Glob31 =finite_element.GlobaleLagrangeBasis1D(29,Mh,LocQ)
Glob32 =finite_element.GlobaleLagrangeBasis1D(30,Mh,LocQ)
x= torch.linspace(0.0,1.0,500)
plt.plot(x,Glob11.forward(x))
plt.plot(x,Glob12.forward(x))
plt.plot(x,Glob21.forward(x))
plt.plot(x,Glob22.forward(x))
plt.plot(x,Glob31.forward(x))
plt.plot(x,Glob32.forward(x))
plt.show()

FE=finite_element.ScalarLagrangeFE1D(Mh,LocQ)
alpha = torch.nn.utils.parameters_to_vector(FE.parameters())

alpha = torch.zeros_like(alpha)
alpha[8] = 1.0
torch.nn.utils.vector_to_parameters(alpha, FE.parameters())
x= torch.linspace(0.0,1.0,500)
x=x[:,None]
plt.plot(x,FE.forward(x).detach().numpy())
plt.show()

alpha = torch.ones_like(alpha)
torch.nn.utils.vector_to_parameters(alpha, FE.parameters())
x= torch.linspace(0.0,1.0,500)
x=x[:,None]
plt.plot(x,FE.forward(x).detach().numpy())
plt.show()



