from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_xv, training_xv
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling, sampling_pde_txv
from scimba.equations import domain, pdes


class Lap_xv(pdes.AbstractPDExv):
    r"""

    .. math::

        \nabla (\nabla \cdot u) + u = f

    """

    def __init__(self, x_domain, v_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=x_domain,
            velocity_domain=v_domain,
            nb_parameters=1,
            parameter_domain=[[0.5, 0.5 + 1e-4]],
        )

        self.first_derivative_x = True
        self.second_derivative_x = True
        self.first_derivative_v = True
        self.second_derivative_v = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, v, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, v, mu, **kwargs):
        x = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_v1v1 = self.get_variables(w, "w_v1v1")

        f = 8 * PI**2 * alpha * torch.sin(2 * PI * x) * torch.sin(2 * PI * v)
        return u_xx + u_v1v1 + f

    def post_processing(self, x, v, mu, w):
        x = x.get_coordinates()
        return x * (1 - x) * v * (1 - v) * w

    def reference_solution(self, x, v, mu):
        x = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(2 * PI * x) * torch.sin(2 * PI * v)


def main():
    xdomain = domain.SpaceDomain(1,domain.SquareDomain(1, [[0.0, 1.0]]))
    vdomain = domain.SquareDomain(1, [[0.0, 1.0]])
    pde = Lap_xv(xdomain, vdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    v_sampler = sampling_pde_txv.VSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde_txv.PdeXVCartesianSampler(x_sampler, v_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True
    # new_training = False

    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_xv.TrainerPINNSpaceVel.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 20, 20, 20, 20]
    network = pinn_xv.MLP_xv(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_xv.PINNxv(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=5.2e-2,decay=0.99,switch_to_LBFGS=True)
    trainer = training_xv.TrainerPINNSpaceVel(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=6000,
    )

    if train:
        trainer.train(epochs=1000, n_collocation=6000)

    trainer.plot(20000,reference_solution=True)


if __name__ == "__main__":
    main()
