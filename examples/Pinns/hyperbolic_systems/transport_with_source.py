# %%
from pathlib import Path

import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_tx as pinn_tx
import scimba.pinns.training_tx as training_tx
import scimba.sampling.sampling_ode as sampling_ode
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from torch.autograd import grad

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


# +
class AdvectionSource(pdes.AbstractPDEtx):
    """
    Advection equation with source term

    .. math::
        u_t + u_x = f_x(t,x,\\alpha)

    where :math:`\\alpha` is a parameter and :math:`f` is a source term.
    """

    def __init__(
        self,
        tdomain=[0, 0.5],
        xdomain=domain.SquareDomain(1, [[0.0, 1.0]]),
        p_domain=[[0.5, 1.0], [0.9, 1.1]],
        # p_domain=[[0.4, 0.7]],
        source=False,
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=2,
            # nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = False

        self.t_min, self.t_max = self.time_domain

        self.source = source

        # if source is None:
        #     self.source = lambda x, mu: 0.0
        # else:
        #     self.source = lambda x, mu: source(self, x, mu)

    def residual(self, w, t, x, mu, **kwargs):
        u = self.get_variables(w)
        u_x = self.get_variables(w, "w_x")
        u_t = self.get_variables(w, "w_t")
        _, alpha = self.get_parameters(mu)

        if self.source is None:
            return u_t + u_x
        else:
            return u_t + u_x + alpha * u

    def bc_residual(self, w, t, x, mu, **kwargs):
        return self.get_variables(w)

    def steady_solution(self, x, mu, **kwargs):
        u0, alpha = self.get_parameters(mu)
        return u0 * torch.exp(-alpha * x.x)

    def initial_condition(self, x, mu, **kwargs):
        if self.source:
            perturbation = 0.01 * torch.exp(-100 * (x.x - 0.5) ** 2)
            return self.steady_solution(x, mu) + perturbation
        else:
            _, alpha = self.get_parameters(mu)
            return torch.exp(-100 * alpha * (x.x - 0.5) ** 2)

    def post_processing(self, t, x, mu, w):
        x_ = x.get_coordinates()
        u_ini = self.initial_condition(x, mu)

        if self.source:
            u_steady = self.initial_condition(x, mu)
            _, alpha = self.get_parameters(mu)
            flux = grad(u_steady, x.x, torch.ones_like(x.x), retain_graph=True)[0]
            source = -alpha * u_steady
            return u_ini + t * w * (flux - source)
        else:
            return u_ini + t * w * x_ * (2 - x_)

    def make_data(self, n_data):
        pass

    def reference_solution(self, t, x, mu):
        if self.source:
            u, _, __, ___ = scheme(self, mu)
            return torch.tensor(u(x.x.detach().cpu().numpy()))[:, :, 0]
        else:
            return self.initial_condition(x - t, mu)


# %%


def scheme(pde, mu):
    import numpy as np
    import scipy.interpolate as spint

    u0, alpha = pde.get_parameters(mu)
    alpha = alpha.detach().cpu().numpy()

    def update_upwind(u, beta, dt):
        # cette fonction retourne la mise à jour en temps du vecteur u par le schéma décentré
        u_im1 = np.roll(u, 1)
        u_im1[0] = u0[0].item()
        u_np1 = u - beta * (u - u_im1)
        if pde.source:
            u_np1 -= dt * alpha[0].item() * u
        return u_np1

    def time_loop_upwind(L, nx, c, t_end, CFL):
        # cette fonction calcule une approximation de u au temps t_end en utilisant le schéma décentré amont

        x = np.linspace(0, L, nx)
        dx = x[1] - x[0]

        t = 0  # temps initial
        dt = CFL * dx / c  # pas de temps
        beta = c * dt / dx  # valeur du paramètre β

        x_ = torch.tensor(x)[:, None]
        x_ = domain.SpaceTensor(x_, labels=torch.zeros_like(x_, dtype=int))
        mu = (
            torch.tensor([u0[0].item(), alpha[0].item()])[:, None]
            * torch.ones_like(x_.x)[None, :, 0]
        ).T
        u_ini = pde.initial_condition(x_, mu).detach().cpu().numpy()
        u = u_ini.copy()

        while t < t_end:
            # boucle en temps : on fait évoluer la valeur de u en utilisant update_upwind
            u = update_upwind(u, beta, dt)
            t += dt

        return x, u, u_ini

    L = 2  # taille du domaine
    nx = 1000  # nombre de points

    c = 1  # vitesse dans l'équation de transport
    t_end = pde.t_max  # temps final

    CFL = 0.45  # coefficient CFL

    x, u, u_ini = time_loop_upwind(L, nx, c, t_end, CFL)
    return spint.CubicSpline(x, u), x, u, u_ini


# %%


def main():
    # source = False
    source = True

    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = AdvectionSource(tdomain=[0.0, 1.0], xdomain=xdomain, source=source)

    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 60, 60, 40]
    network = pinn_tx.MLP_tx(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinn,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_name,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=2000, n_collocation=3000)

    return pde, network, trainer


if __name__ == "__main__":
    pde, network, trainer = main()
    trainer.plot(reference_solution=True, steady_solution=True)


# %%
