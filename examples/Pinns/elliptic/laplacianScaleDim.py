from pathlib import Path

import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pde_1d_laplacian
import scimba.pinns.pinn_losses as pinn_losses
import scimba.nets.training_tools as training_tools
from scimba.equations import domain, pdes
import time

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846

class Poisson_1D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        f = 4 * PI**2 * alpha * torch.sin(2 * PI * x1)
        return u_xx + f

    def post_processing(self, x, mu, w):
        x1 = x.get_coordinates()
        return x1 * (1 - x1) * w

    def reference_solution(self, x, mu):
        x1 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(2 * PI * x1)


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2= x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 8 * PI**2 * alpha * torch.sin(2 * PI * x1) * torch.sin(2 * PI * x2)
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(2 * PI * x1) * torch.sin(2 * PI * x2)


class Poisson_3D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2, x3 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        u_zz = self.get_variables(w, "w_zz")
        f = 12 * PI**2 * alpha * torch.sin(2 * PI * x1) * torch.sin(2 * PI * x2)* torch.sin(2 * PI * x3)
        return u_xx + u_yy + u_zz+ f

    def post_processing(self, x, mu, w):
        x1, x2, x3 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * x3 * (1 - x3) * w

    def reference_solution(self, x, mu):
        x1, x2, x3 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(2 * PI * x1) * torch.sin(2 * PI * x2) * torch.sin(2 * PI * x3)


def solve_Laplacian(pde,nb_points):
    tps1 = time.time()
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "laplacian3D.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40,40,40,40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)
    print('Number of parameters>>> :',len(torch.nn.utils.parameters_to_vector(pinn.net.parameters())))

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1.6e-2,decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        losses =losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_name,
        batch_size=nb_points,
    )
    if train:
        trainer.train(epochs=2500, n_collocation=nb_points, n_bc_collocation=0, n_data=0)

    tps2 = time.time()
    print(" >>>",tps2 - tps1)
    trainer.plot(n_visu=50000,reference_solution=True)
    # trainer.plot_derivative_mu(n_visu=500)
    # trainer.plot_derivative_xmu(n_visu=500)
 
def main():
    #xdomain = domain.SpaceDomain(1,domain.SquareDomain(1, [[0.0, 1.0]]))
    #pde = Poisson_1D(space_domain=xdomain)
    #solve_Laplacian(pde,nb_points=2000)

    #xdomain = domain.SpaceDomain(2,domain.SquareDomain(2, [[0.0, 1.0],[0.0, 1.0]]))
    #pde = Poisson_2D(space_domain=xdomain)
    #solve_Laplacian(pde,nb_points=4000)

    xdomain = domain.SpaceDomain(3,domain.SquareDomain(3, [[0.0, 1.0],[0.0, 1.0],[0.0, 1.0]]))
    pde = Poisson_3D(space_domain=xdomain)
    solve_Laplacian(pde,nb_points=8000)


if __name__ == "__main__":
    main()
