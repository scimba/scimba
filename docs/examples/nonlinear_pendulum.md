# Nonlinear Pendulum

We solve the PDE:

$$
\begin{aligned}
\frac{dq(t)}{dt} &= p(t) \\
\frac{dp(t)}{dt} &= -sin(q(t))
\end{aligned}
$$

on $t\in[0,30], \omega\in [0.9,1.0]$.

We learn $p(t,x,\omega), q(t,x,\omega)$.

**Parametric model:**

$$u_{net}(t,\alpha) =  (q_{\theta}(t,\alpha),p_{\theta}(t,\alpha))$$

```{literalinclude} ../../examples/PINNs/ode/nonlinear_pendulum.py
```


