from pathlib import Path

import torch

from scimba import device
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import ode_basic


def plot_derivative_wrt_mu(trainer, random: bool = False):
    """Plotting function; uses matplotlib.

    Plots the value of the prediction of the ODE solution,
    compared with a reference solution.
    """
    import matplotlib.pyplot as plt

    _, ax = plt.subplots(1, 3, figsize=(15, 5))
    ax[0]= trainer.losses.plot(ax[0])
    ax[0].legend()

    n_visu = 500

    t = torch.linspace(
        trainer.ode.t_min, trainer.ode.t_max, n_visu, dtype=torch.double, device=device
    )[:, None]

    shape = (n_visu, trainer.ode.nb_parameters)
    ones = torch.ones(shape)
    if random:
        mu = trainer.mu_sampler.sampling(1)
        mu = mu * ones
    else:
        mu = torch.mean(trainer.ode.parameter_domain, axis=1) * ones

    parameter_string = ", ".join(
        [f"{mu[0, i].cpu().numpy():2.2f}" for i in range(trainer.ode.nb_parameters)]
    )

    mu.requires_grad = True

    w = trainer.network.setup_w_dict(t, mu)
    trainer.network.get_first_derivatives_wrt_mu(w, mu)

    w_pred = w["w_mu_1"]

    alpha = trainer.ode.get_parameters(mu)
    w_ex = -t * torch.exp(-alpha * t)

    ax[1].plot(t.cpu(), w_ex.detach().cpu(), label="exact")
    ax[1].plot(t.cpu(), w_pred.detach().cpu(), label="prediction")

    ax[1].set_title(f"prediction, parameters = {parameter_string}")
    ax[1].legend()

    error = torch.abs(w_pred - w_ex).detach().cpu()

    ax[2].plot(t.cpu(), error)
    ax[2].set_title("prediction error")
    plt.show()


def main():
    ode = ode_basic.SimpleOde()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name = "test.pth"
    new_training = True
    # new_training = False

    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 20, 20, 20]
    network = pinn_t.DisMLP_t(ode=ode, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_t.PINNt(network, ode)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=9e-3,
        decay=0.992,switch_to_LBFGS=True)
    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=2000
    )


    if train:
        trainer.train(epochs=500, n_collocation=2000, n_init_collocation=2000)

    trainer.plot(random=True,reference_solution=True)
    plot_derivative_wrt_mu(trainer)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
