"""
Classes for neural operators:
- deep operators (t, x, tx)
- DeepONets (t, x, tx)
- Green operators (t, x, tx)
"""
