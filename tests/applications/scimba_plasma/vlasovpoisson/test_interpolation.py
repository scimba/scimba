import numpy as np

from applications.scimba_plasma.vlasovpoisson import bsl


def test_interpolation(n=64):
    """Test to check interpolation"""
    cs = bsl.BSpline(3, 0, 1, n)
    x = np.linspace(0, 1, n, endpoint=False)
    f = np.sin(x * 4 * np.pi)
    alpha = 0.2
    assert np.allclose(np.sin((x - alpha) * 4 * np.pi), cs.interpolate_disp(f, alpha))
