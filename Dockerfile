FROM python:3.11

LABEL maintainer="Matthieu Boileau <matthieu.boileau@math.unistra.fr>"
LABEL description="Docker image for Scimba project dependencies."

COPY requirements.txt ./

RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt
