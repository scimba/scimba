from pathlib import Path

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import ode_basic


def main():
    ode = ode_basic.AmortizedPendulum()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name = "test_pendulum1.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [80, 80, 40, 40, 20]
    network = pinn_t.MLP_t(ode=ode, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_t.PINNt(network, ode)

    losses = pinn_losses.PinnLossesData(
        init_loss_bool=True, w_data=0, w_res=0.25, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.992)
    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=200, n_collocation=4000, n_init_collocation=2000, n_data=0)

    trainer.plot()


if __name__ == "__main__":
    main()
