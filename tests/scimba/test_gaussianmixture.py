import os

import numpy as np
import pytest
import torch

from scimba.generativenets import gaussian_mixtures, trainer_likelihood

datasets = pytest.importorskip("sklearn.datasets")


class DatasetMoons:
    """two half-moons"""

    def sample(self, n):
        moons = datasets.make_moons(n_samples=n, noise=0.05)[0].astype(np.float32)
        return torch.tensor(moons)


def test_normalizingflow(tmp_path):
    data = DatasetMoons()
    x = data.sample(32)

    out_size = 2
    mixture = gaussian_mixtures.GaussianMixtures(dim=out_size, nb_gaussians=10)

    file_name = "test.pth"
    os.chdir(tmp_path)

    Trainer = trainer_likelihood.TrainerLikelihood(
        out_size=2,
        output_data=x,
        file_name=file_name,
        network=mixture,
        learning_rate=7e-3,
        batch_size=8,
    )
    Trainer.train(epochs=5)
    mixture.sample(num_samples=512)
