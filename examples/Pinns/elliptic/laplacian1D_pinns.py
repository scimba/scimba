from pathlib import Path

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pde_1d_laplacian


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 1.0]]))
    pde = pde_1d_laplacian.LaplacianSine(k=1, domain=xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "laplacianSine1D.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [80, 80, 60, 40, 20]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_name,
        batch_size=2500,
    )
    if train:
        trainer.train(epochs=200, n_collocation=2500, n_init_collocation=0, n_data=0)

    trainer.plot(reference_solution=True)
    # trainer.plot_derivative_mu(n_visu=500)
    # trainer.plot_derivative_xmu(n_visu=500)


if __name__ == "__main__":
    main()
