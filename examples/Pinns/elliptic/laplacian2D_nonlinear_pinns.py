import time
from math import pi as PI
from pathlib import Path

import torch
from scimba.equations import domain, pdes
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling


class NonlinearPoisson2D(pdes.AbstractPDEx):
    r"""

    .. math::

        \frac{d^2u}{dx^2} + \frac{d^2u}{dy^2} + f = 0

    """

    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.5, 1.5]],
        )

        self.first_derivative = True
        self.second_derivative = True

        self.force_compute_1st_derivatives_in_residual = True

        def f_x(w, x, mu):
            x1, x2 = x.get_coordinates()
            u = self.get_variables(w)
            return x2 * u**3 / 6

        def f_y(w, x, mu):
            x1, x2 = x.get_coordinates()
            u = self.get_variables(w)
            return x1 * u**3 / 6

        self.f_x = f_x
        self.f_y = f_y

        self.f_xx = lambda w, x, mu: self.get_variables(w) ** 2 / 8
        self.f_xy = lambda w, x, mu: self.get_variables(w) ** 2 / 16
        self.f_yy = lambda w, x, mu: self.get_variables(w) ** 2 / 8

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)

        u = self.get_variables(w)
        u_x = self.get_variables(w, "w_x")
        f_u_x = self.get_variables(w, "f_w_x")
        f_u_y = self.get_variables(w, "f_w_y")
        f_u_xx = self.get_variables(w, "f_w_xx")
        f_u_xy = self.get_variables(w, "f_w_xy")
        f_u_yy = self.get_variables(w, "f_w_yy")

        sin_x = torch.sin(2 * PI * x1)
        cos_x = torch.cos(2 * PI * x1)

        sin_y = torch.sin(2 * PI * x2)
        cos_y = torch.cos(2 * PI * x2)

        term1 = (sin_x + 2 * PI * cos_x) * sin_y * alpha
        term2 = (
            -PI
            * alpha**3
            * (sin_x**2 * sin_y**2 * (cos_x * x2 * sin_y - x1 * sin_x * cos_y))
        )
        term3 = (
            PI**2
            * alpha**2
            * (
                cos_x**2 * sin_y**2
                + cos_x * sin_x * cos_y * sin_y
                - sin_x**2 * cos_y**2
            )
        )

        rhs = term1 + term2 + term3

        return f_u_xx + f_u_xy - f_u_yy - f_u_x + f_u_y + u_x + u - rhs

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(2 * PI * x1) * torch.sin(2 * PI * x2)


def main(architecture):
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = NonlinearPoisson2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 40, 20]

    if architecture == "MLP":
        network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
        w_res = 0.1
    elif architecture == "MMLP":
        network = pinn_x.MMLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
        w_res = 0.5

    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(bc_loss_bool=True, w_res=w_res, w_bc=5.0)

    optimizers = training_tools.OptimizerData(learning_rate=2e-2, decay=0.99)

    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if train:
        start = time.perf_counter()
        trainer.train(epochs=1000, n_collocation=5000, n_bc_collocation=5000)
        print(f"Training time: {time.perf_counter() - start:.2f} seconds")

    # trainer.plot_derivative_mu(n_visu=20000)
    # trainer.plot_derivative_xmu(n_visu=20000)

    return network, trainer


if __name__ == "__main__":
    print("training an MLP")
    network_MLP, trainer_MLP = main("MLP")
    trainer_MLP.plot(20000, reference_solution=True)
    print("finished training the MLP\n")

    print("\ntraining an MMLP")
    network_MMLP, trainer_MMLP = main("MMLP")
    trainer_MMLP.plot(20000, reference_solution=True)
    print("finished training the MMLP")
