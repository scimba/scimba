# imports
from pathlib import Path

import torch

from src.com3DeepShape import geometry_sample_in_disk as geometry

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}; script is solve_shape_optimization.py")

if __name__ == "__main__":

    # ==============================================================
    # Parameters to be modified freely by the user
    # ==============================================================

    train = True
    # train = False

    deepGeoDict = {
        "pde_learning_rate": 5e-3,
        "sympnet_learning_rate": 5e-3,
        "layer_sizes": [20, 20, 40, 20, 20],
        "n_sympnet_layers": 4,
        "sympnet_layer_size": 4,
        "rho_min": 0,
        "rho_max": 1,
        "file_name": "geometry",
        "to_be_trained": train,
        "source_term": "exp",
        # "boundary_condition": "homogeneous_dirichlet",
        # "boundary_condition": "homogeneous_neumann",
        "boundary_condition": "robin",
    }

    epochs = 150
    n_collocation = 5_000
    # new_training = False
    new_training = True
    save_plots = False
    # save_plots = True

    # ==============================================================
    # End of the modifiable area
    # ==============================================================

    if train:
        if new_training:
            try:
                output_dir = Path.cwd() / "networks"
                Path(output_dir / f"{deepGeoDict['file_name']}.pth").unlink()
            except FileNotFoundError:
                pass

        network = geometry.PINNs(deepGeoDict=deepGeoDict)
        elapsed = network.train(epochs=epochs, n_collocation=n_collocation)

        print(f"Computational time: {elapsed:3.2f} sec.")

    else:
        network = geometry.PINNs(deepGeoDict=deepGeoDict)

    network.plot_result(save_plots)
