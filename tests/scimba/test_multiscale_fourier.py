from math import pi as PI
import os
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 72 * PI**2 * alpha * torch.sin(6 * PI * x1) * torch.sin(6 * PI * x2)
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(6 * PI * x1) * torch.sin(6 * PI * x2)


def train_network(network, file_path, pde, epochs, sampler, new_training=True):
    if new_training:
        (
            Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS) / file_path
        ).unlink(missing_ok=True)

    pinns = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=9.5e-3, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinns,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_path,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=epochs, n_collocation=100)


def test_fourier(tmp_path):
    os.chdir(tmp_path)
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = Poisson_2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)
    tlayers = [3, 2]
    network2 = pinn_x.Fourier_x(
        pde=pde, 
        mean_features={"x":0.0,"mu":0.0,"xmu":0.0},
        std_features={"x":1.0,"mu":0.0,"xmu":0.0},
        nb_features=tlayers[0], layer_sizes=tlayers
    )
    train_network(network2, "nnfourier", pde, 5, sampler)


def test_multiscale(tmp_path):
    os.chdir(tmp_path)
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = Poisson_2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)
    tlayers = [3, 2]

    network3 = pinn_x.MultiScale_Fourier_x(
        pde=pde, means=[0.0,0.0],stds=[1.0, 10.0], nb_features=tlayers[0], layer_sizes=tlayers
    )
    train_network(network3, "nnmultiscale", pde, 5, sampler)


def test_siren(tmp_path):
    os.chdir(tmp_path)
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = Poisson_2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)
    tlayers = [10, 10]

    network4 = pinn_x.Siren_x(pde=pde, w=1.0, layer_sizes=tlayers)
    train_network(network4, "nnsiren", pde, 5, sampler)
