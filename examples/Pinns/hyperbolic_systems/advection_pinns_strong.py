from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_tx, pinn_tx
from scimba.sampling import (
    sampling_parameters,
    sampling_pde,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import domain, pdes


class Advection_equation(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 0.4],
        xdomain=domain.SquareDomain(1, [[0, 2]]),
        p_domain=[[0.8, 1.2]],
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u_x = self.get_variables(w, "w_x")
        u_t = self.get_variables(w, "w_t")
        return u_t + alpha * u_x

    def bc_residual(self, w, t, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def initial_condition(self, x, mu, **kwargs):
        x = x.get_coordinates()
        t0 = 0.015
        alpha = self.get_parameters(mu)
        return (1 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1) ** 2) / (4 * alpha * t0)
        )

    def make_data(self, n_data):
        pass

    def post_processing(self, t, x, mu, w):
        x_ = x.get_coordinates()
        return self.initial_condition(x, mu) + t * (2.0 - x_) * x_ * w

    def reference_solution(self, t, x, mu):
        x = x.get_coordinates()
        t0 = 0.015
        alpha = self.get_parameters(mu)
        return (1 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1 - alpha * t) ** 2) / (4 * alpha * t0)
        )


# Test with the Heat equation inside the module PINNs
# where initial and BC are strongly imposed
def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = Advection_equation(tdomain=[0.0, 0.4], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 40, 20]
    network = pinn_tx.MLP_tx(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=9e-3, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinn,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_name,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=500, n_collocation=3000)

    trainer.plot(reference_solution=True)


if __name__ == "__main__":
    main()
