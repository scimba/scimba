import os

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_txv, training_txv
from scimba.equations import pde_1x1v_transport


def test_kinetic_constant_transport(tmp_path):
    pde = pde_1x1v_transport.ConstantInX()

    network = pinn_txv.MLP_txv(pde=pde, layer_sizes=[20, 20])
    pinn = pinn_txv.PINNtxv(network, pde)

    file_name = "testkinetic.pth"
    # Move to tmp_path as working directory
    os.chdir(tmp_path)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.99)
    trainer = training_txv.TrainerPinnKinetic(
        pde=pde,
        network=pinn,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=2500,
    )

    trainer.train(
        # epochs=input_conf["NN"]["epochs"],
        # n_collocation=input_conf["NN"]["n_collocation"],
        # n_data=input_conf["NN"]["n_data"],
        epochs=10,
        n_collocation=5,
        n_data=0,
    )
