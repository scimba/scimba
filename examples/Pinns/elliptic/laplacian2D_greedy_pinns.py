from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.greedy_pinn_x as greedy_pinn_x
import scimba.pinns.greedy_training_x as greedy_training_x
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[-0.5, 0.5], [-0.5, 0.5]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = -torch.exp(-((x1 - mu1) ** 2 + (x2 - mu2) ** 2) / 2) * (
            (
                (x1**2 - 2 * mu1 * x1 + mu1**2 - 5) * torch.sin(2 * x1)
                + (4 * mu1 - 4 * x1) * torch.cos(2 * x1)
            )
            * torch.sin(2 * x2)
            + torch.sin(2 * x1)
            * (
                (x2**2 - 2 * mu2 * x2 + mu2**2 - 5) * torch.sin(2 * x2)
                + (4 * mu2 - 4 * x2) * torch.cos(2 * x2)
            )
        )
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return (0.5 * PI - x1) * (x1 + 0.5 * PI) * (x2 + 0.5 * PI) * (0.5 * PI - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        g = torch.exp(-((x1 - mu1) ** 2.0 + (x2 - mu2) ** 2.0) / 2)
        return g * torch.sin(2 * x1) * torch.sin(2 * x2)


def get_nb_parameters(network):
    return sum(p.numel() for p in network.parameters() if p.requires_grad)


def Run_laplacian2D(pde, greedy=False, epochs=500):
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(greedy_training_x.TrainerGreedyPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    if greedy:
        n_collocation = [5_000, 5_000, 20_000, 40_000]
        layers_small = [20,40,40,20]
        layers_large = [20,40,40,40,20]
        list_of_nets = [
            pinn_x.MLP_x(pde=pde, layer_sizes=layers_small, activation_type="sine"),
            pinn_x.MLP_x(pde=pde, layer_sizes=layers_small, activation_type="sine"),
            pinn_x.Fourier_x(
                pde=pde,
                layer_sizes=layers_large,
                activation_type="sine",
                bool_feature_mu=True,
                nb_features=20,
                std_features={"x": 10, "mu": 10, "xmu": 0},
            ),
            pinn_x.Fourier_x(
                pde=pde,
                layer_sizes=layers_large,
                activation_type="sine",
                bool_feature_mu=True,
                nb_features=40,
                std_features={"x": 5, "mu": 5, "xmu": 0},
            ),
            # pinn_x.MLP_x(pde=pde, layer_sizes=layers_large, activation_type="sine"),
            # pinn_x.MLP_x(pde=pde, layer_sizes=layers_large, activation_type="sine"),
        ]
        for network in list_of_nets:
            print(f"Number of parameters: {get_nb_parameters(network)}")
        alpha = [1e-0, 1e-2, 1e-4, 1e-5]
        pinn = greedy_pinn_x.GreedyPinn_x(list_of_nets, pde, alpha=alpha)
        trainer_class = greedy_training_x.TrainerGreedyPINNSpace
    else:
        n_collocation = 25_000
        tlayers = [32, 64, 64, 32]
        epochs *= 4  # non-greedy PINNs: multiply epochs by number of greedy networks
        network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
        print(f"Number of parameters: {get_nb_parameters(network)}")
        pinn = pinn_x.PINNx(network, pde)
        trainer_class = training_x.TrainerPINNSpace

    losses = pinn_losses.PinnLossesData()
    optimizers = training_tools.OptimizerData(
        learning_rate=9e-3,
        decay=0.99,
        switch_to_LBFGS=False,
        switch_to_LBFGS_at=3 * epochs // 4,
    )

    trainer = trainer_class(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if new_training:
        trainer.train(epochs=epochs, n_collocation=n_collocation)

    return trainer


if __name__ == "__main__":
    xdomain = domain.SpaceDomain(
        2, domain.SquareDomain(2, [[-0.5 * PI, 0.5 * PI], [-0.5 * PI, 0.5 * PI]])
    )
    pde = Poisson_2D(xdomain)

    epochs = 2500

    trainer = Run_laplacian2D(pde, greedy=True, epochs=epochs)
    trainer.plot_2d_contourf(n_visu=128, draw_contours=True)
    trainer.plot_2d_contourf(n_visu=128, residual=True, draw_contours=True)
    trainer.plot_2d_contourf(n_visu=128, error=True, draw_contours=True)
    trainer_greedy = Run_laplacian2D(pde, greedy=True, epochs=epochs)
    trainer_greedy.plot_all_residuals_and_errors_contour_square()
