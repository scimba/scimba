import glob
import numpy as np
import os
import torch
import yaml

from argparse import ArgumentParser
from pathlib import Path
from torch.utils.tensorboard import SummaryWriter

from applications.scimba_plasma.vlasovpoisson import timer
from applications.scimba_plasma.vlasovpoisson import PINNvlasov_sampling as PINNsampling
from applications.scimba_plasma.vlasovpoisson import PINNvlasov_training as PINNtrainer
from applications.scimba_plasma.vlasovpoisson import PINNvlasov_equations as veq
from applications.scimba_plasma.vlasovpoisson import PINNvlasov_plot as PVplt

from scimba.nets.training_tools import OptimizerData
from scimba.pinns import pinn_txv
from scimba.pinns.pinn_losses import PinnLossesData

def func_train_PINNvlasov_txv(config, training_filename, writer, pde):
    network = pinn_txv.MLP_txv(pde=pde, 
                               layer_sizes=config["NN"]["layer_sizes"],
                               activation_type=config["NN"]["activation_type"]
    )
    pinn = pinn_txv.PINNtxv(network, pde)

    # Read and print the NN informations
    n_data_percent=config["NN"]["n_data_percent"]
    n_colloc_percent=config["NN"]["n_colloc_percent"]
    n_init_colloc_percent=config["NN"]["n_init_colloc_percent"]
    n_bc_colloc_percent=config["NN"]["n_bc_colloc_percent"]
    nb_simu_xv = len(pde.simu.x)*len(pde.simu.v)
    nb_simu_xvt = len(pde.simu.t)*nb_simu_xv
    print(f'Number of simulation point = {nb_simu_xvt}')
    n_data = int(n_data_percent*0.01*nb_simu_xvt)
    n_collocation = int(n_colloc_percent*0.01*nb_simu_xvt)
    n_init_collocation = int(n_init_colloc_percent*0.01*nb_simu_xv)
    n_bc_collocation = int(n_bc_colloc_percent*0.01*nb_simu_xv)
    print(f' - n_data ({n_data_percent}%) = {n_data}  ')
    print(f' - n_collocation ({n_colloc_percent}%) = {n_collocation}  ')
    print(f' - n_init_collocation ({n_init_colloc_percent}%) = {n_init_collocation}  ')
    print(f' - n_bc_collocation ({n_bc_colloc_percent}%) = {n_bc_collocation}  ')
 
    losses = PinnLossesData(
        bc_loss_bool=(n_bc_collocation>0),
        init_loss_bool=(n_init_collocation>0),
        data_loss_bool=(n_data>0), 
        w_data=config["Trainer"]["w_data"],
        w_res=config["Trainer"]["w_res"],
        w_bc=config["Trainer"]["w_bc"],
        w_init=config["Trainer"]["w_init"],
    )

    optimizers = OptimizerData(
        learning_rate=config["Trainer"]["learning_rate"],
        decay=config["Trainer"]["decay"],
        switch_to_LBFGS=config["Trainer"]["switch_to_LBFGS"],
        switch_to_LBFGS_after=config["Trainer"]["switch_to_LBFGS_after"],
    )

    trainer = PINNtrainer.TrainerPinnVlasov(
      pde=pde,
      network=pinn,
      folder_for_saved_networks=config["folder_for_saved_networks"],
      file_name=training_filename,
      sampler=sampler,
      losses=losses,
      optimizers=optimizers,
      batch_size=config["Trainer"]["batch_size"],
    )

    trainer.train(
        epochs=config["NN"]["epochs"],
        n_collocation=n_collocation,
        n_data=n_data,
        n_init_collocation=n_init_collocation,
        n_bc_collocation=n_bc_collocation
    )

    return trainer


# %%

if __name__ == "__main__":
    parser = ArgumentParser(description="PINN for Vlasov equation (1X,1V)")
    parser.add_argument(
        "-i",
        "--input_file",
        action="store",
        nargs="?",
        default=Path("config_test_PINNLandau.yaml"),
        type=Path,
        help="input file (YAML format)",
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        action="store",
        nargs="?",
        default=None,
        type=Path,
        help="output folder",
    )
    args = parser.parse_args()

    current = os.getcwd()  #os.path.dirname(os.path.realpath(__file__))

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(f"torch loaded; device is {device}")

    time = timer.Timer("Compute NN")
    time.start()

    # Open the file and load the file
    input_filename = args.input_file.name
    print("Input file={}".format(input_filename))
    with open(input_filename) as file:
        input_conf = yaml.safe_load(file)
        print(input_conf)

    # Download the Vlasov-Poisson simulation
    VPresu_filename = input_conf['VlasovPoisson']['file']
    VPresu_dir = VPresu_filename.split("data/")[-1].split(".h5")[0].upper()
    print(f'VPresu_dir= {VPresu_dir}')

    # Create if necessary the output folder
    folder_for_saved_networks = input_conf["folder_for_saved_networks"]
    if args.output_folder is None:
        n_epochs = input_conf["NN"]["epochs"]
        n_data_percent = input_conf["NN"]["n_data_percent"]
        n_colloc_percent = input_conf["NN"]["n_colloc_percent"]
        n_init_colloc_percent = input_conf["NN"]["n_init_colloc_percent"]
        n_bc_colloc_percent = input_conf["NN"]["n_bc_colloc_percent"]
        w_data = input_conf["Trainer"]["w_data"]
        w_res = input_conf["Trainer"]["w_res"]
        w_init = input_conf["Trainer"]["w_init"]
        w_bc = input_conf["Trainer"]["w_bc"]
        save_dir_name = f"{folder_for_saved_networks}/{VPresu_dir}"
        save_dir_name = f"{save_dir_name}/RESU_ND{n_data_percent}-{w_data}_NC{n_colloc_percent}-{w_res}"
        save_dir_name = f"{save_dir_name}_NINIT{n_init_colloc_percent}-{w_init}_NBC{n_bc_colloc_percent}-{w_bc}"
        save_dir = Path(save_dir_name)
    else:
        save_dir = args.output_folder
    save_dir.mkdir(parents=True, exist_ok=True)
    print(f"==> Results saved in {save_dir.absolute()}")
    input_conf["folder_for_saved_networks"] = str(save_dir.absolute())

    tensorboard_dir_name = (
        f"{folder_for_saved_networks}/runs/{VPresu_dir}/exp_{save_dir.name}"
    )
    tensorboard_dir = Path(tensorboard_dir_name)
    tensorboard_dir.mkdir(parents=True, exist_ok=True)
    print(f"==> Tensorboard traces saved in {tensorboard_dir_name}")
    writer = SummaryWriter(tensorboard_dir)

    new_load_simu=True
    try:
        time_domain = [input_conf["VlasovPoisson"]["time_init"], input_conf["VlasovPoisson"]["time_end"]]
    except:
        time_domain = []
    pde = veq.Vlasov_externalE(VPresu_filename, new_load_simu, time_domain)
    sampler = PINNsampling.PdeGrid1D1VSampler(pde)
    pde.gives_sampler(sampler)
    pde.print()

    training_filename = "training.pth"
    print(f'training_filename = {training_filename}')

    trainer = func_train_PINNvlasov_txv(
        config=input_conf,
        training_filename=training_filename,
        writer=writer,
        pde=pde,
    )
    time.stop()

    # Plot figures
    time = timer.Timer("Plot figures")
    time.start()
    PVplt.plot_loss_PINNvlasov(trainer, save_dir)
    PVplt.plot_fdistribu_PINNvlasov(trainer, save_dir, deltaf=False)
    PVplt.plot_fdistribu_PINNvlasov(trainer, save_dir, deltaf=True)

    # Save the parameters used in a YAML file
    yaml_data = yaml.dump(input_conf, default_flow_style=False)
    YAML_filename = f"{save_dir}/PINN_parameters"
    nb_YAML_files = np.size(glob.glob(f"{YAML_filename}*.yaml"))
    YAML_filename = '{:s}_{:03d}.yaml'.format(YAML_filename, nb_YAML_files)
    print(f"--> Save parameters in {YAML_filename}")
    print(yaml_data, file=open(YAML_filename,'a'))

    time.stop()

