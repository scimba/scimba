from pathlib import Path

import torch
import torch.nn as nn


from scimba.pinns import training_t, pinn_t
from scimba.sampling import (
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import ode_basic


class FourierFeatures(nn.Module):
    def __init__(self, in_size, num_freqs, freq=1.0):
        super().__init__()
        self.freq = freq
        self.linear = nn.Linear(in_size, num_freqs, bias=False)
        nn.init.xavier_normal_(
            self.linear.weight, gain=self.freq
        )  # init to avoid multiplying

    def forward(self, inputs):
        inputs = self.linear(inputs)
        return torch.cat((torch.sin(inputs), torch.cos(inputs)), dim=1)


class SymplecticPreprocessor(nn.Module):
    def __init__(self, ode, **kwargs):
        super().__init__()
        self.u0 = ode.initial_condition
        self.ff_on_params = kwargs.get("ff_params", False)
        ff_in_size = 1 if not self.ff_on_params else (1 + ode.nb_parameters)
        ff_num_freqs = kwargs.get("ff_num_freqs", 0)
        self.ff_net = FourierFeatures(
            ff_in_size, ff_num_freqs, freq=kwargs.get("ff_freq", 1.0)
        )
        self.out_size = ode.nb_unknowns + 1 + ode.nb_parameters + 2 * ff_num_freqs
        self.t_rescale = ode.time_domain
        self.mu_min = torch.Tensor([d[0] for d in ode.parameter_domain])
        self.mu_max = torch.Tensor([d[1] for d in ode.parameter_domain])

    def forward(self, t, mu):
        t = (t - self.t_rescale[0]) / (self.t_rescale[1] - self.t_rescale[0])
        mu = (mu - self.mu_min) / (self.mu_max - self.mu_min)
        inputs = torch.cat((t, mu), dim=1) if self.ff_on_params else t
        return torch.cat((self.u0(mu), t, mu, self.ff_net(inputs)), dim=1)


class TimeGradPotential(nn.Module):
    def __init__(self, y_dim, p_dim, width, **kwargs):
        super().__init__()
        self.linear = nn.Linear(y_dim, width, bias=False)
        self.bias = nn.Linear(p_dim, width)
        self.activation = kwargs.get("activation", nn.Tanh())
        self.scaling = nn.Linear(p_dim, width)

    def forward(self, y, p):
        # grad_y V(y, p) with V(y, p) = a(p) * t * sigma(K @ y + b(p))
        t = p[..., 0, None]
        z = self.activation(self.linear(y) + self.bias(p))
        return (t * self.scaling(p) * z) @ self.linear.weight


class HenonLayer(nn.Module):
    def __init__(self, y_dim, params_dim, width=5, **kwargs):
        super().__init__()
        self.dim = y_dim
        self.width = width
        self.grad_potential = TimeGradPotential(y_dim, params_dim, self.width)
        self.shift = nn.Linear(params_dim, y_dim)
        # self.shift = henonnet.ShallowMLP(params_dim, self.width, y_dim)

    def forward(self, inputs):
        x, y, p = inputs.tensor_split((self.dim, 2 * self.dim), dim=1)
        for _ in range(4):
            x, y = y + self.shift(p), -x + self.grad_potential(y, p)
        return torch.cat((x, y, p), dim=1)


class HenonPINN(nn.Module):
    def __init__(self, in_size, out_size, **kwargs) -> None:
        super().__init__()
        self.z_dim = out_size  # = space_size
        self.p_size = in_size - out_size  # = time + parameters + ff
        assert self.z_dim % 2 == 0
        self.y_dim = self.z_dim // 2
        self.widths = kwargs.get("widths", [8] * 20)
        print("in_size:", in_size)
        self.layers = [
            HenonLayer(self.y_dim, self.p_size, width=w, **kwargs) for w in self.widths
        ]
        self.layers = nn.ModuleList(self.layers)

    def forward(self, inputs: torch.Tensor):
        for layer in self.layers:
            inputs = layer(inputs)
        return inputs[:, : self.z_dim]


def main():
    ode = ode_basic.NonlinearPendulum()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    sampler = sampling_ode.OdeCartesianSampler(t_usampler, mu_usampler)

    file_name = "sympnet_pendulum.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINNTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    preproc = SymplecticPreprocessor(ode, ff_params=True, ff_num_freqs=10, ff_freq=6.0)
    network = pinn_t.PINNt(
        net=HenonPINN,
        ode=ode,
        preprocessor=preproc,
    )

    trainer = training_t.TrainerPINNTime(
        ode=ode,
        network=network,
        sampler=sampler,
        file_name=file_name,
        init_loss_bool=True,
        learning_rate=1e-2,
        decay=0.992,
        batch_size=4000,
        w_data=0,
        w_res=0.25,
        w_init=1.0,
        switch_to_LBFGS=True,
    )

    if new_training:
        trainer.train(
            epochs=1000, n_collocation=4000, n_init_collocation=2000, n_data=0
        )

    # trainer.plot()


if __name__ == "__main__":
    main()
