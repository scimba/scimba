from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_tx, pinn_tx
from scimba.sampling import (
    sampling_parameters,
    sampling_pde,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import domain, pdes


class Advection_equation(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 0.4],
        xdomain=domain.SquareDomain(1, [[0, 2]]),
        p_domain=[[0.8, 1.2]],
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u_x = self.get_variables(w, "w_x")
        u_t = self.get_variables(w, "w_t")
        return u_t + alpha * u_x

    def bc_residual(self, w, t, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def initial_condition(self, x, mu, **kwargs):
        x = x.get_coordinates()
        t0 = 0.015
        alpha = self.get_parameters(mu)
        return (1 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1) ** 2) / (4 * alpha * t0)
        )

    def make_data(self, n_data):
        pass

    def reference_solution(self, t, x, mu):
        x = x.get_coordinates()
        t0 = 0.015
        alpha = self.get_parameters(mu)
        return (1 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1 - alpha * t) ** 2) / (4 * alpha * t0)
        )


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = Advection_equation(tdomain=[0.0, 0.4], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True

    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 40, 20]
    network = pinn_tx.DisMLP_tx(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=True, init_loss_bool=True, w_res=0.2, w_bc=1.0, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(learning_rate=9e-3, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=3000,
    )

    if train:
        trainer.train(
            epochs=500,
            n_collocation=3000,
            n_bc_collocation=1000,
            n_init_collocation=1000,
            n_data=0,
        )

    trainer.plot(reference_solution=True)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
