import os
import sys
from argparse import ArgumentParser
from pathlib import Path

import h5py as h5
import matplotlib.pyplot as plt
import numpy as np
import yaml
from applications.scimba_plasma.vlasovpoisson.vlasov import VlasovPoisson

def periodic_grid1d( grid1d ):
   nx1 = len(grid1d)
   dx1 = grid1d[1] - grid1d[0]
   period_grid1d = np.zeros( (nx1+1), dtype=float )
   period_grid1d[0:nx1] = grid1d
   period_grid1d[nx1] = grid1d[-1] + dx1
   return period_grid1d

def periodic_array1d( array1d ):
   nx1 = array1d.shape
   period_array1d = np.zeros( (nx1+1), dtype=float )
   period_array1d[0:nx1] = array1d
   period_array1d[nx1] = array1d[0]
   return period_array1d

def periodic_array2d( array2d ):
   nx1, nx2 = array2d.shape
   period_array2d = np.zeros( (nx1+1, nx2+1), dtype=float )
   period_array2d[0:nx1,0:nx2] = array2d
   period_array2d[nx1,0:nx2] = array2d[0,0:nx2]
   period_array2d[:,nx2] = period_array2d[:,0] 
   return period_array2d

def periodic_array1d_evol( array1d_evol ):
   nt, nx1 = array1d_evol.shape
   period_array1d_evol = np.zeros( (nt, nx1+1), dtype=float )
   period_array1d_evol[:,0:nx1] = array1d_evol
   period_array1d_evol[:,nx1] = array1d_evol[:,0]
   return period_array1d_evol

def periodic_array2d_evol( array2d_evol ):
   nt, nx1, nx2 = array2d_evol.shape
   period_array2d_evol = np.zeros( (nt, nx1+1, nx2+1), dtype=float )
   period_array2d_evol[:,0:nx1,0:nx2] = array2d_evol
   period_array2d_evol[:,nx1,0:nx2] = array2d_evol[:,0,0:nx2]
   period_array2d_evol[:,:,nx2] = period_array2d_evol[:,:,0] 
   return period_array2d_evol


def func_vlasovpoisson( config ):

    VPtype = config["VlasovPoisson"]["type"]
    # mesh definition in (x,v,t)
    nx = config["VlasovPoisson"]["nx"]
    nv = config["VlasovPoisson"]["nv"]
    ntime = config["VlasovPoisson"]["ntime"]
    xmin = config["VlasovPoisson"]["xmin"]
    xmax = config["VlasovPoisson"]["xmax"]
    vmin = config["VlasovPoisson"]["vmin"]
    vmax = config["VlasovPoisson"]["vmax"]
    tmax = config["VlasovPoisson"]["tmax"]
    # equilibrium definition
    Teq = config["VlasovPoisson"]["Teq"]
    if VPtype == "bump_on_tail":
      V_bot = config["VlasovPoisson"]["V_bot"]
      T_bot = config["VlasovPoisson"]["T_bot"]
      eps_bot = config["VlasovPoisson"]["eps_bot"]
    # perturbation definition 
    epsilon = config["VlasovPoisson"]["epsilon"]
    kx = config["VlasovPoisson"]["kx"]

    if VPtype == "landau_damping": 
      VPtype_name = "LD"
    elif VPtype == "bump-on-tail":
      VPtype_name = "BOT"
    else:
      print(f"VlasovPoisson type = {VPtype} is not treated")
      sys.exit()

    VPresu_dir = Path("data")
    VPresu_dir.mkdir(parents=True, exist_ok=True)
    prefix_name = f"{VPresu_dir.name}/{VPtype_name}"
    suffix_name = f"eps{epsilon}_t{tmax}_Nx{nx}_Nv{nv}"
    VPresu_filename = f"{prefix_name}_{suffix_name}.h5"

    if not os.path.exists(VPresu_filename):
        # Create Vlasov-Poisson simulation
        sim = VlasovPoisson(xmin, xmax, nx, vmin, vmax, nv)

        # Initialize distribution function
        X, V = np.meshgrid(sim.x, sim.v)

        if VPtype == "landau_damping":
            feq = 1.0/np.sqrt(2.0*np.pi*Teq)* np.exp(-0.5*V*V/Teq)
        elif VPtype == "bump-on-tail":
            f1_eq = (1.0-eps_bot)/np.sqrt(2.*np.pi*Teq)*np.exp(-0.5*V*V/Teq)  
            f2_eq = eps_bot/np.sqrt(2.*T_bot) * np.exp(-(V-V_bot)*(V-V_bot)/(2.*T_bot))
            feq = f1_eq + f2_eq

        fdistribu = feq * (1.0 + epsilon*np.cos(kx*X))
        fdistribu = fdistribu.T
        feq = feq.T

        # Set time domain
        timegrid, dt = np.linspace(0.0, tmax, ntime, retstep=True)

        # Set the output distribution function and electric field
        fdistribu_evol = np.zeros((ntime, nx, nv))
        fdistribu_evol[0, :] = fdistribu
        efield_evol = np.zeros((ntime, nx))

        # Run simulation
        nrj, fdistribu_evol, efield = sim.run(fdistribu, ntime, dt, fdistribu_evol, efield_evol)

        # Plot energy
        fig1_filename = f"{prefix_name}_PotEnergy_{suffix_name}.png"
        plt.figure()
        plt.plot(timegrid, nrj, label=r"$\int E(x) dx$")
        if VPtype == "landau_damping":
            plt.plot(timegrid, -5.55-0.154*timegrid)
        plt.legend()
        print(f'Save figure: {fig1_filename}')
        plt.savefig(fig1_filename)

        # Plot distribution function
        fig2_filename = f"{prefix_name}_fdistribu_{suffix_name}.png"
        plt.figure()
        plt.pcolormesh(sim.x, sim.v, (fdistribu_evol[-1, :, :] - feq).T)
        plt.title(f"f-feq at time = {timegrid[-1]}")
        plt.colorbar()
        print(f"Save figure: {fig2_filename}")
        plt.savefig(fig2_filename)

        # Save data in HDF5 file
        x_period = periodic_grid1d(sim.x)
        v_period = periodic_grid1d(sim.v)
        feq_period = periodic_array2d(feq)
        fdistribu_evol_period = periodic_array2d_evol(fdistribu_evol)
        efield_evol_period = periodic_array1d_evol(efield_evol)

        print(f"Save the results in the HDF5 file {VPresu_filename}")
        with h5.File(VPresu_filename, "w") as fh5:
          fh5.create_dataset("simu_type", data=VPtype)
          fh5.create_dataset("xgrid", data=sim.x)
          fh5.create_dataset("vgrid", data=sim.v)
          fh5.create_dataset("timegrid", data=timegrid)
          fh5.create_dataset("Teq", data=Teq)
          fh5.create_dataset("feq", data=feq)
          fh5.create_dataset("epsilon", data=epsilon)
          fh5.create_dataset("kx", data=kx)
          fh5.create_dataset("fdistribu", data=fdistribu_evol)
          fh5.create_dataset("efield", data=efield_evol)
          fh5.create_dataset("xgrid_period", data=x_period)
          fh5.create_dataset("vgrid_period", data=v_period)
          fh5.create_dataset("feq_period", data=feq_period)
          fh5.create_dataset("fdistribu_period", data=fdistribu_evol_period)
          fh5.create_dataset("efield_period", data=efield_evol_period)
          fh5.close()
    else:
        print(f"==> {VPresu_filename} already exists")

    return VPresu_filename


# %%

if __name__ == "__main__":
    parser = ArgumentParser(description="Solve Vlasov-Poisson 1X-1V")
    parser.add_argument(
        "-i",
        "--input_file",
        action="store",
        nargs="?",
        default=Path("config_bydefault_Landau.yaml"),
        type=Path,
        help="input file (YAML format)",
    )
    parser.add_argument(
        "-o",
        "--output_folder",
        action="store",
        nargs="?",
        default=None,
        type=Path,
        help="output folder",
    )
    args = parser.parse_args()

    current = os.getcwd()

    # Open the input file
    input_filename = args.input_file.name
    print("Input file={}".format(input_filename))
    with open(input_filename) as file:
        input_conf = yaml.safe_load(file)
        print(input_conf)

    # Launch the Vlasov-Poisson simulation
    VPresu_filename = func_vlasovpoisson(input_conf)
    print(f"Vlasov-Poisson simulation saved in {VPresu_filename}")
