import time
from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Poisson_3D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=3,
            parameter_domain=[
                [0.05, 0.050001],
                [0.220000, 0.2200001],
                [0.10000, 0.100001],
            ],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2, x3 = x.get_coordinates()
        mu1, mu2, mu3 = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        u_zz = self.get_variables(w, "w_zz")

        exp = torch.exp(
            -((x1 - mu1) ** 2.0 + (x2 - mu2) ** 2.0 + ((x3 - mu3) ** 2.0)) / 2
        )
        f1 = (
            4.0
            * (-mu1 + x1) ** 1.0
            * torch.sin(2 * x2)
            * torch.sin(2 * x3)
            * torch.cos(2 * x1)
            * exp
        )
        f2 = (
            -1.0
            * (-mu1 + x1) ** 2.0
            * torch.sin(2 * x1)
            * torch.sin(2 * x2)
            * torch.sin(2 * x3)
            * exp
        )
        f3 = (
            4.0
            * (-mu2 + x2) ** 1.0
            * torch.sin(2 * x1)
            * torch.sin(2 * x3)
            * torch.cos(2 * x2)
            * exp
        )
        f4 = (
            -1.0
            * (-mu2 + x2) ** 2.0
            * torch.sin(2 * x1)
            * torch.sin(2 * x2)
            * torch.sin(2 * x3)
            * exp
        )
        f5 = (
            +4.0
            * (-mu3 + x3) ** 1.0
            * torch.sin(2 * x1)
            * torch.sin(2 * x2)
            * torch.cos(2 * x3)
            * exp
        )
        f6 = (
            -1.0
            * (-mu3 + x3) ** 2.0
            * torch.sin(2 * x1)
            * torch.sin(2 * x2)
            * torch.sin(2 * x3)
            * exp
        )
        f7 = +15.0 * torch.sin(2 * x1) * torch.sin(2 * x2) * torch.sin(2 * x3) * exp
        f = f1 + f2 + f3 + f4 + f5 + f6 + f7
        return u_xx + u_yy + u_zz + f

    def post_processing(self, x, mu, w):
        x1, x2, x3 = x.get_coordinates()
        return (
            (0.5 * PI - x1)
            * (x1 + 0.5 * PI)
            * (x2 + 0.5 * PI)
            * (0.5 * PI - x2)
            * (x3 + 0.5 * PI)
            * (0.5 * PI - x3)
            * w
        )

    def reference_solution(self, x, mu):
        x1, x2, x3 = x.get_coordinates()
        mu1, mu2, mu3 = self.get_parameters(mu)
        exp = torch.exp(-((x2 - mu2) ** 2 + (x1 - mu1) ** 2 + (x3 - mu3) ** 2) / 2)
        return torch.sin(2 * x1) * torch.sin(2 * x2) * torch.sin(2 * x3) * exp


def main():
    start = time.time()
    xdomain = domain.SpaceDomain(
        3,
        domain.SquareDomain(
            3,
            [
                [
                    -0.5 * PI,
                    0.5 * PI,
                ],
                [-0.5 * PI, 0.5 * PI],
                [-0.5 * PI, 0.5 * PI],
            ],
        ),
    )
    pde = Poisson_3D(space_domain=xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "laplacian3D.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 40, 40, 40, 40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1.8e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_name,
        batch_size=2500,
    )
    if train:
        trainer.train(epochs=3000, n_collocation=3000, n_bc_collocation=0, n_data=0)

    end = time.time()
    print(" >>>", end - start)
    trainer.plot(n_visu=50000, reference_solution=True)
    # trainer.plot_derivative_mu(n_visu=500)
    # trainer.plot_derivative_xmu(n_visu=500)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
