# Pendulum

We solve the ODE:

$$\frac{du(t)}{dt^2} + 2c\lambda\omega \frac{du(t)}{dt} + \omega^2 u(t) =0$$

on $t\in[0,20], \lambda,\omega\in [1.0,1.5]^2$.

We learn $u(t,x,\lambda,\omega)$.

**Parametric model:**

$$u_{net}(t,\alpha) = u_{\theta}(t,\alpha)$$

```{literalinclude} ../../examples/PINNs/ode/pendulum.py
```