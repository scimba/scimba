import torch
from scimba import device
from scimba.equations.domain import SpaceTensor
from scimba.sampling import grid_sampling, sampling_parameters, uniform_sampling

"""
A class for grid sampling in t, x, v, mu retrieved from the simulation +  parameter to the pde 'a' if a parameter is given in the pde model
"""
class PdeGrid1D1VSampler:
    def __init__(self, pde):
        self.pde = pde
        self.t_len = pde.simu.t.shape[0]
        self.x_len = pde.simu.x.shape[0]
        self.v_len = pde.simu.v.shape[0]
        self.E_len = pde.simu.E.shape
        self.f_len = pde.simu.f.shape
        self.with_external_field = True

        # data from simulation converted to tensor type
        self.data_t = torch.Tensor(self.pde.simu.t).to(device)[:, None]
        labels = torch.zeros(self.x_len, dtype=int)
        x = torch.Tensor(self.pde.simu.x).to(device)[:, None]
        self.data_x = SpaceTensor(torch.Tensor(x), labels)
        self.data_v = torch.Tensor(self.pde.simu.v).to(device)[:, None]
        self.data_E = torch.Tensor(self.pde.simu.E).to(device)[:, None]
        self.data_f = torch.Tensor(self.pde.simu.f).to(device)[:, None]
        self.data_feq = torch.Tensor(self.pde.simu.feq).to(device)[:, None]

        assert (self.t_len == self.E_len[0])
        assert (self.x_len == self.E_len[1])

        self.dim = 1 + pde.dimension_x + pde.dimension_v

        self.lower_bounds = [1, 1, 1]  # VG: ATTENTION Take the boundary into account
        self.upper_bounds = [
            self.t_len - 1,
            self.x_len - 1,
            self.v_len - 1,
        ]  # VG: ATTENTION Take the boundary into account

        assert (self.dim == len(self.lower_bounds))
        assert (self.dim == len(self.upper_bounds))

        """
        This sampler returns an random sampling on the indices
        """
        self.sampler = grid_sampling.LatinHypercubeSampling(
            self.dim, self.lower_bounds, self.upper_bounds
        )
        self.sampler_mu = sampling_parameters.MuSampler(
            sampler=uniform_sampling.UniformSampling, model=pde
        )


    def sampling(self, n_points):
        indices = self.sampler.sampling(n_points)
        indices_tensor = torch.tensor(indices)
        data_t = self.data_t[[i[0] for i in indices]]
        data_x = self.data_x[[i[1] for i in indices]]
        data_v = self.data_v[[i[2] for i in indices]]
        data_a = (self.data_E[indices_tensor[:, 0], 0, indices_tensor[:, 1]])[:, None]
        data_mu = self.sampler_mu.sampling(n_points)

        data_f = self.data_f[
            indices_tensor[:, 0], 0, indices_tensor[:, 1], indices_tensor[:, 2]
        ][:, None]

        data_t.requires_grad_()
        data_x.x.requires_grad_()
        data_v.requires_grad_()
        data_f.requires_grad_()

        return data_t, data_x, data_v, data_mu, data_a, data_f


    def bc_sampling_periodic(self, n_points):
        t_idx = torch.randint(0, self.t_len, size=(n_points,))
        x_idx = torch.randint(0, self.x_len, size=(n_points,))
        v_idx = torch.randint(0, self.v_len, size=(n_points,))

        data_t = self.data_t[t_idx]

        data_x_left = self.data_x[0].repeat(n_points)
        data_x_right = self.data_x[-1].repeat(n_points)
        data_x_var = self.data_x[x_idx]

        data_v_left = self.data_v[0].repeat(n_points)[:, None]
        data_v_right = self.data_v[-1].repeat(n_points)[:, None]
        data_v_var = self.data_v[v_idx]

        data_mu = self.sampler_mu.sampling(n_points)

#VG#begin: why do you need these gradients ?
#VG#        data_t.requires_grad_()
#VG#        data_x_left.x.requires_grad_()
#VG#        data_x_right.x.requires_grad_()
#VG#        data_x_var.x.requires_grad_()
#VG#        data_v_left.requires_grad_()
#VG#        data_v_right.requires_grad_()
#VG#        data_v_var.requires_grad_()
#VG#        data_mu.requires_grad_()
#VG#end: why do you need these gradients ?

        return (
            data_t,
            (data_x_left, data_x_right, data_x_var),
            (data_v_left, data_v_right, data_v_var),
            data_mu,
        )


    def ic_sampling(self, n_points):
        indices = self.sampler.sampling(n_points)
        indices_tensor = torch.tensor(indices)

        data_x = self.data_x[[i[1] for i in indices]]
        data_v = self.data_v[[i[2] for i in indices]]
        data_a = self.data_E[indices_tensor[:, 0], ..., indices_tensor[:, 1]][:, None]

        data_mu = self.sampler_mu.sampling(n_points)
        data_t = torch.zeros_like(data_x.x)

        data_f = self.data_f[
            indices_tensor[:, 0], ..., indices_tensor[:, 1], indices_tensor[:, 2]
        ]

        data_t.requires_grad_()
        data_x.x.requires_grad_()
        data_v.requires_grad_()
        data_f.requires_grad_()

        return data_t, data_x, data_v, data_mu, data_a, data_f


    def density(self, t, x, v, mu):
        return (
            self.sampler.density(t)
            * self.sampler.density(x)
            * self.sampler.density(v)
            * self.sampler.density(mu)
        )
