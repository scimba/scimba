import os

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_xv, training_xv
from scimba.sampling import (
    sampling_pde,
    sampling_parameters,
    uniform_sampling,
    sampling_pde_txv,
)
from scimba.equations import domain, pde_1d_laplacian_xv


def test_poisson_xv1D(tmp_path):
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 1.0]]))
    vdomain = domain.SquareDomain(1, [[0.0, 1.0]])
    pde = pde_1d_laplacian_xv.Lap1D_xv(xdomain, vdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    v_sampler = sampling_pde_txv.VSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde_txv.PdeXVCartesianSampler(x_sampler, v_sampler, mu_sampler)

    file_name = "test.pth"
    os.chdir(tmp_path)

    tlayers = [2, 3]
    network = pinn_xv.MLP_xv(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_xv.PINNxv(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(
        learning_rate=5.2e-2, decay=0.99, switch_to_LBFGS=True
    )
    trainer = training_xv.TrainerPINNSpaceVel(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=6000,
    )

    trainer.train(epochs=10, n_collocation=10)
