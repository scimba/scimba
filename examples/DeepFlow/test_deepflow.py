from pathlib import Path

import matplotlib.pyplot as plt
import torch
from scimba.equations.pdes import AbstractODE
from scimba.neural_operators import deeponet_t, deep_flow_t
from scimba.pinns import pinn_t
from scimba.pinos import pino_t, training_t
from scimba.sampling import (
    data_sampling_ode,
    sampling_functions,
    sampling_ode,
    sampling_parameters,
    uniform_sampling,
)
from scimba.nets.training import TrainerSupervised
from scimba.nets.training_tools import OptimizerData, SupervisedLossesData

def create_solution(x0,omega,t):
    q0=x0[None,:,0]
    p0=x0[None,:,1]
    omega_ =omega[None,:,0]
    t_=t[:,None]
    res1= q0*torch.cos(omega_*t_) + p0*torch.sin(omega_*t_)
    res2= omega_*(-q0*torch.sin(omega_*t_)+p0*torch.cos(omega_*t_))
    omega_= torch.repeat_interleave(omega_,t.shape[0],dim=0)
    return res1,res2,omega_


def create_dataset(k,q,p,omega):
    res_in=torch.stack([q,p,omega]).transpose(0,1).transpose(1,2)
    res_in = res_in[:-k]
    res_in=res_in.reshape(res_in.shape[0]*res_in.shape[1],res_in.shape[2])
    res_out=torch.stack([q,p]).transpose(0,1).transpose(1,2)
    res_out = res_out[k:]
    res_out=res_out.reshape(res_out.shape[0]*res_out.shape[1],res_out.shape[2])
    return res_in,res_out


class Oscillator(AbstractODE):

    """
        \dot{q}=p
        \dot{p}=-\omega q
    """

    def __init__(self):
        super().__init__(
            nb_unknowns=2,
            time_domain=[0, 10.0],
            nb_parameters=1,
            parameter_domain=[[0.9999, 1]],
        )

        self.first_derivative = True
        self.second_derivative = False
        self.t_min, self.t_max = self.time_domain[0], self.time_domain[1]

    def initial_condition(self, mu, **kwargs):
        pass

    def residual(self, w, t, mu, **kwargs):
        pass

def main():
    rollout=1
    ode = Oscillator()
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    w_initial_sampler = uniform_sampling.UniformSampling(2, [[1.0, 2.0],[0.0,0.2]])


    t=torch.linspace(0,10,100)
    mu= mu_sampler.sampling(5)
    w0= w_initial_sampler.sampling(5)
    q,p,omega=create_solution(w0,mu,t)
    x,y=create_dataset(rollout,q,p,omega)

    flow = deep_flow_t.DiscreteFlow(2,1,flowtype="mlp",rollout=rollout)

    file_name = "test_flow.pth"
    new_training = True
    train = True

    if new_training:
        (
            Path.cwd()
            / Path(TrainerSupervised.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    losses = SupervisedLossesData()
    optimizers = OptimizerData(learning_rate=2e-2,decay=0.992)
    trainer =  TrainerSupervised(3,x,2,y,network=flow,optimizers=optimizers,losses=losses,file_name=file_name)

    if train:
        trainer.train(epochs=1000)

    solution = flow.inference(x[0][None,:],100)
    print(solution.shape)

    fig, axes = plt.subplots(1,2)
    axes[0].plot(t,q[:,0].detach().numpy())
    axes[1].plot(t,p[:,0].detach().numpy())
    axes[0].plot(t,solution[:,0,0].detach().numpy())
    axes[1].plot(t,solution[:,0,1].detach().numpy())
    plt.show()

    return 0


if __name__ == "__main__":
    res = main()