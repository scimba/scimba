import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.neuralgalerkin.neural_galerkin_x as ng
import scimba.pinns.pinn_x as pinn_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

PI = 3.14159265358979323846


class AdvectionDiffusion(pdes.AbstractPDEx):
    def __init__(
        self,
        xdomain=domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]])),
        p_domain=[],
    ):
        super().__init__(
            nb_unknowns=2,
            space_domain=xdomain,
            nb_parameters=0,
            parameter_domain=p_domain,
        )

        self.first_derivative = True
        self.second_derivative = False

    def bc_residual(self, w, x, mu, **kwargs):
        p, u = self.get_variables(w)
        return p, u

    def residual(self, w, x, mu, **kwargs):
        p_x, u_x = self.get_variables(w, "w_x")
        return -u_x, -p_x

    def post_processing(self, x, mu, w):
        x = x.get_coordinates()
        return x * (2 - x) * w


def init(x, mu):
    x = x.get_coordinates()

    D = 0.02
    coeff = 1 / (4 * PI * D) ** 0.5

    p_plus_u = coeff * torch.exp(-((x - 1) ** 2) / (4 * D))
    p_minus_u = coeff * torch.exp(-((x - 1) ** 2) / (4 * D))
    p = (p_plus_u + p_minus_u) / 2
    u = (p_plus_u - p_minus_u) / 2

    return torch.cat((p, u), axis=1)


def sol_ref(t, x, mu):
    x = x.get_coordinates()

    D = 0.02
    coeff = 1 / (4 * PI * D) ** 0.5

    p_plus_u = coeff * torch.exp(-((x - t - 1) ** 2) / (4 * D))
    p_minus_u = coeff * torch.exp(-((x + t - 1) ** 2) / (4 * D))
    p = (p_plus_u + p_minus_u) / 2
    u = (p_plus_u - p_minus_u) / 2

    p_plus_u_x = (-(x - t - 1) / (2 * D)) * p_plus_u
    p_minus_u_x = (-(x + t - 1) / (2 * D)) * p_minus_u
    p_x = (p_plus_u_x + p_minus_u_x) / 2
    u_x = (p_plus_u_x - p_minus_u_x) / 2

    return torch.cat((p, u, p_x, u_x), axis=1)


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = AdvectionDiffusion(xdomain=xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )

    tlayers = [24, 24, 24, 24]
    # tlayers = [5]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="tanh")
    pinn = pinn_x.PINNx(network, pde, init_net_bool=False)

    ### create the model
    model = ng.NeuralGalerkin_x(
        pde,
        x_sampler,
        mu_sampler,
        pinn,
        scheme="rk2",
        type_init=1,
        n_points=2200,
        epoch_initial_train=800,
        lr_initial_train=5e-2,
    )

    ## solve the problem
    model.compute_initial_data(w0=init)
    model.time_loop(dt=0.0025, T=0.25, sol_exact=sol_ref)

    model.plot(n_visu=1000, sol_exact=sol_ref)


if __name__ == "__main__":
    main()
