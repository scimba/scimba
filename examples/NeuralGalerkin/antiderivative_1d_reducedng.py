"""
    Using Reduced Neural Galerkin to solve the equation below:
    .. math::

    \frac{du}{dx} = f(x)

    where f(x) = sin(2 * pi * mu * x) and u(x=0) = 0
"""

from pathlib import Path

import scimba.equations.domain as domain
import scimba.neuralgalerkin.reduced_ng as reduced_ng
import scimba.sampling.sampling_functions as sampling_functions
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import pde_1d_antiderivative
from scimba.neural_operators import deeponet_x
from scimba.pinns import pinn_x
from scimba.pinos import pino_x, training_x

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

xdomain = domain.SpaceDomain(1,domain.SquareDomain(1, [[0.0, 1.0]]))
p_domain = [[0.5, 1.500001]]
p_domain_test = [[1.0, 1.000001]]
pde = pde_1d_antiderivative.Antiderivative(domain=xdomain, p_domain=p_domain,)
pde_test = pde_1d_antiderivative.Antiderivative(domain=xdomain, p_domain=p_domain_test,)
x_sampler = sampling_pde.XSampler(pde)
mu_sampler = sampling_parameters.MuSampler(
    sampler=uniform_sampling.UniformSampling, model=pde
)
mu_sampler_test = sampling_parameters.MuSampler(
    sampler=uniform_sampling.UniformSampling, model=pde_test
)
sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)
sampler_test = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler_test)

file_name = "ReNG_x_linear_z6.pth"

nb_sensor=500
lat_size=6

# Sampler used for computing some data (x_i, u_i) with i=1:nb_sensor based on the
# analytical solution in case the initial data is not provided
u_sampler = sampling_functions.FunctionsSampler(
    1,
    p_domain=p_domain,
    sampler=sampler,
    t_sampler=None,
    x_sampler=x_sampler,
    f=pde.reference_solution,
    nb_sensor=nb_sensor,
)


new_training = True
if new_training:
    (
        Path.cwd()
        / Path(training_x.TrainerPINOSpace.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    ).unlink(missing_ok=True)


no_network = deeponet_x.DeepONetSpace(
    net=pinn_x.MLP_x,
    pde=pde,
    nb_sensor=nb_sensor,
    lat_size=lat_size,
    encoder_type="PointNet",
    decoder_type="linear",
    activation_type="sine",
    layers_b=[20, 40, 40, 20],
    layers_t=[20, 40, 40, 20]
)

network = pino_x.PINOx(no_network, pde)

### create the model
model = reduced_ng.ReducedNeuralGalerkin_x(
    pde_x=pde,
    x_sampler=x_sampler,
    mu_sampler=mu_sampler,
    mu_sampler_test=mu_sampler_test,
    u_sampler=u_sampler,
    AE=network,
    file_name=file_name,
    n_points=1500,
    epochs=1000,
    n_data_f=4,
    n_data_x=250,
    w_data=1.0,
    w_res=0.0,
    switch_to_LBFGS=False,
)

# Train the autodecoder to compute the initial latent variable z for the
# Reduced Neural Galerkin algorithm
model.compute_initial_data()

u_sampler_test = sampling_functions.FunctionsSampler(
    1,
    p_domain=p_domain_test,
    sampler=sampler_test,
    t_sampler=None,
    x_sampler=x_sampler,
    f=pde_test.reference_solution,
    nb_sensor=nb_sensor,
)

model.check_autoencoder(u_sampler=u_sampler)

(vecx,vecu,x,mu_collocation,out) = u_sampler_test.sampling(n_f=1, n_points=1)

# Encode the data to obtain the initial condition
z0 = no_network.forward_encoder(vecx, vecu)[0]

# Update the latent variables using the Reduced Neural Galerkin algorithm
z = model.iterative_method(z0, iter_max=3)

model.plot(n_visu=500)

