from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_x, pinn_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.sdf = space_domain.large_domain.sdf

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 1.0
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return self.sdf(x) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        return x1


class Circle2(domain.SignedDistance):
    def __init__(self):
        super().__init__(dim=2)

    def sdf(self, x):
        x1, x2 = x.get_coordinates()
        ones = torch.ones_like(x1)
        res = ((x1 - 0.5) ** 2 + (x2 - 0.5) ** 2) - ones
        return res


def test_sdf(sdf, bound_box):
    xdomain = domain.SpaceDomain(2, domain.SignedDistanceBasedDomain(2, bound_box, sdf))

    pde = Poisson_2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 40, 40, 40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=5e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if new_training:
        trainer.train(epochs=500, n_collocation=5000)

    trainer.plot(80000, reference_solution=True)
    trainer.plot_2d_contourf(
        n_visu=768,
        n_contour=250)
    # trainer.plot_derivative_mu(n_visu=20000)


def main():
    sdf = Circle2()
    test_sdf(sdf, [[-1.5, 2.5], [-1.5, 2.5]])
    #sdf = domain.PolygonalApproxSignedDistance(
    #    2, [[0.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.3, 0.0]], threshold=0.015
    #)
    #test_sdf(sdf, [[-0.5, 1.5], [-0.5, 1.5]])
    #sdf = domain.PolygonalApproxSignedDistance(
    #    2, [[0.0, 0.0], [0.0, 1.0], [1.0, 1.0], [1.0, 0.0]], threshold=0.01
    #)
    #test_sdf(sdf, [[-0.2, 1.2], [-0.2, 1.2]])
    #sdf = domain.PolygonalApproxSignedDistance(
    #    2,
    #    [[0.0, 0.0], [0.0, 1.0], [0.75, 1.5], [1.0, 1.0], [1.0, 0.0], [0.75, -0.5]],
    #    threshold=0.015,
    #)
    #test_sdf(sdf, [[-1.0, 2.0], [-1.0, 2.0]])
    #sdf = domain.PolygonalApproxSignedDistance(
    #    2,
    #    [[0.0, -0.5], [0.0, 0.5], [0.5, 0.5], [0.5, 0.0], [1.0, 0.0], [1.0, -0.5]],
    #    threshold=0.015,
    #)
    #test_sdf(sdf, [[-1.0, 2.0], [-1.0, 2.0]])


if __name__ == "__main__":
    main()
