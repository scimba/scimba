import numpy as np
import pytest

from applications.scimba_plasma.vlasovpoisson import bsl


@pytest.mark.parametrize("xmin, xmax, nx", [(0, 10, 100), (0, 10, 200)])
def test_advection(xmin, xmax, nx):
    x = np.linspace(xmin, xmax, nx)
    f0 = np.exp(-((x - 5) ** 2))

    f = np.copy(f0)

    dt = 0.1
    adv = bsl.BSpline(3, xmin, xmax, nx)

    for _ in range(100):
        f = adv.interpolate_disp(f, dt)

    assert np.allclose(f0, f)
