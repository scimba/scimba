# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.5.1] - 2024-10-28

### Fixed

- fix the doc badge
- fix the twine upload issue on public scimba project (#96)

### Removed

- Remove scikit-learn from the examples dependencies (#87)

### Changed

- Skip some tests if scikit-learn is not installed (#87)
- Reorganize test directories (#91)
- Use tmp_path fixture in tests (#92)
- Move to pytorch 2.3.1 (#89)

## [0.5] - 2024-10-24

### Changed

- refactored the SympNet and HenonNet classes
- refactored the MLP and RBFnet classes
- refactored the classes related to Neural Galerkin

### Fixed

- several inconsistencies in the Vlasov-Poisson application
- several files were not properly formatted with black
- some relative imports, rather than abolute

### Added

- application for shape optimization
- application for hybrid Newton's method
- various tests and examples to increase coverage
- supervised loss
- automatic creation of SpaceTensor from space coordinates
- loss functions for generative nets
- activation functions: hat, regularized hat, swish
- abstract, generic loss function
- abstract, generic trainer
- Gaussian mix generator with the keops library (optional dependency)
- Fourier features on the parameters and the velocity variable
- third derivatives with respect to space and parameters
- possibility of including third derivatives in the loss function
- contour plots for two-dimensional time-independent problems

### Removed

- the notebooks folder
- the numericalmethods folder (for now, with refactoring pending)

## [0.4.2] - 2024-04-30

### Changed

- Sphinx doc can now support myst-markdown
- Update the homepage URL

### Fixed

- Various inconsistencies in the documentation

### Added

- Edit link in the documentation
- Badges for CI, coverage, and release in the documentation

### Removed

- pylint from the CI pipeline that slowed down the build

## [0.4.1] - 2024-04-15

### Changed

- Release description comes from tag message
- New releases are now uploaded to PYPi instead of the gitlab registry

## [0.4.0] - 2024-04-15

### Added

- Overhaul of domains with labels and normals: support for
    - boundaries or manifolds given by a class
    - holes
    - subdomains
    - multiple boundary conditions
- Overhaul of samplings and interfaces for neural operators
- Addition of loss and optimizer classes, with very simple loss balancing
- Addition of multiscale and SIREN networks
- Separation of the general kinetic classes (e.g. pinn_txv) from the Vlasov application
- Domain learning through a level set function
- Concatenation of several examples and tests in single files
- Further improvements to the documentation, type hints, and test coverage

## [0.3.0] - 2023-12-18

### Added

- PINNs system modification for greater network flexibility
- Fourier network for PINNs
- greater test converage
- possibility of using a pointNet (permutation and invariant point) as a DeepOnet encoder
- ODE learning by the differentiable physics method
- polygonal domains via a level set function
- support for nonlinear terms in derivatives in PINNs
- discontinuous MLP
- Neural Galerkin: initialization learning
- Neural Galerkin: the xv case

## [0.2.2] - 2023-10-17

### Added

- docstrings for various modules (#28)

### Fixed

- Missing module members in the documentation (#28)
- typos in README.md
- Pin torch version to 2.0.1 to avoid the stateless API change in 2.1.0 (#32)

## [0.2.1] - 2023-09-28

### Changed

- build doc in tests (#13)
- Use `autosummary` for doc generation (#28)
- simplify imports syntax

## [0.2.0] - 2023-09-27

### Added

- Version number is now dynamically generated
- a GitLab-CI pipeline is used for testing (#6)
- Documentation is deployed with GitLab Pages (#13)
- Code coverage is produced and deployed with GitLab Pages (#19)

### Changed

- Packaging is now done with `pyproject.toml` (#17)
- Use an `src/` layout (#18)
- `torch.set_default_device` and `torch.set_default_dtype` are now global (#27)

### Fixed

- `.gitignore` was not general enough
