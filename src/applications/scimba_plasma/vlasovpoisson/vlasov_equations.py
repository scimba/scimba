import os

import h5py
import numpy as np
import torch
from scimba.equations.domain import SquareDomain
from scimba.equations.pdes import AbstractPDEtxv


class Vlasov_externalE(AbstractPDEtxv):
    r"""
    Equation:

    .. math::

      \frac{df}{dt} + v \frac{df}{dx} + E \frac{df}{dv} = 0

    Domain: :math:` x\in [0, 2 pi/k]`, with :math:`k = 1/2 ; t\in[0, 50] ; v \in [-6, 6]`

    BC: periodic boundary condition in x
    IC:

    .. math::

       f_0(x,v) = ( 1 + eps * \cos(k x) ) / ( sqrt(2\pi \sigma^2) ) * exp(- \frac{v^2}{2\sigma^2} )

    We learn  :math:`u_{\theta}(t,x,v)`
    parametric model :math:`f_{net} = f_0 + t u_{\theta}(t,x,v)`
    """

    def __init__(
        self,
        filename_simu,
        loss_with_BC=True,
        loss_with_IC=True,
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=[],
            space_domain=SquareDomain(1, [[0.0, 1.0]]),
            velocity_domain=SquareDomain(1, [[-6.0, 6.0]]),
            nb_parameters=0,
            parameter_domain=[],
            data_construction="grid_sampled",  # "sampled",
        )

        """
        remarque tmp: dans inputs qui rentre dans la fonction forward on concatenne [t,x,v,mu,a]
        --> IL faut donc adapter la taille de in_features dans le network qui est MLP
        -> mettre in_size = sum(dim_t+_x+_v+mu+_a)
        -> voir fichier MLP !!
        """
        self.simu = LoadSimu(filename_simu)

        self.time_domain = self.simu.time_domain
        self.space_domain = self.simu.space_domain
        self.dimension_x = self.space_domain.dim
        self.velocity_domain = self.simu.velocity_domain
        self.dimension_v = self.velocity_domain.dim

        self.k = self.simu.kx
        self.eps = self.simu.epsilon
        self.sigma = np.sqrt(self.simu.Teq)

        self.t_min, self.t_max = self.time_domain

        self.first_derivative_t = True
        self.first_derivative_x = True
        self.first_derivative_v = True

        self.loss_with_BC = loss_with_BC
        self.loss_with_IC = loss_with_IC

    def residual(self, w, t, x, v, mu, **kwargs):
        """
        for this equation, 'a' represents the electric field
        """
        a = kwargs.get("a")
        u_t = self.get_variables(w, "w_t")
        u_x = self.get_variables(w, "w_x")
        u_v = self.get_variables(w, "w_v1")
        return u_t + v * u_x + a * u_v

    def bc_residual(self, w, t, x, v, mu, **kwargs):
        u = self.get_variables(w, "w")
        return u

    def bc_residual_space(self, w, t, x, v, mu, **kwargs):
        u = self.get_variables(w, "w")
        return u
    
    def bc_residual_vel(self, w, t, x, v, mu, **kwargs):
        u = self.get_variables(w, "w")
        return u

    def initial_condition(self, x, v, mu, **kwargs):
        x = x.get_coordinates()
        return (
            (1.0 + self.eps * torch.cos(self.k * x))
            * (1.0 / (np.sqrt(2 * torch.pi * self.sigma**2)))
            * torch.exp(-(v**2) / (2.0 * self.sigma**2))
        )

    def initial_condition_simu(self, ix, iv, imu):
        return self.simu.f[0, ix, iv]

    def reference_solution(self, t, x, v, mu):
        pass

    def gives_sampler(self, sampler):
        self.sampler = sampler

    def make_data(self, n_points):
        t, x, v, mu, a, f = self.sampler.sampling(n_points)
        return t, x, v, mu, a, f


class LoadSimu:
    def __init__(self, h5filename):
        if os.path.exists(h5filename):
            with h5py.File(h5filename, "r") as h5f:
                self.x = h5f["xgrid"][()]
                self.v = h5f["vgrid"][()]
                self.t = h5f["timegrid"][()]
                self.feq = h5f["feq"][()]
                self.Teq = h5f["Teq"][()]
                self.epsilon = h5f["epsilon"][()]
                self.kx = h5f["kx"][()]
                self.f = h5f["fdistribu"][()]
                self.E = h5f["efield"][()]
                self.time_domain = [self.t[0], self.t[-1]]
                self.space_domain = SquareDomain(1, [[self.x[0], self.x[-1]]])
                self.velocity_domain = SquareDomain(1, [[self.v[0], self.v[-1]]])
        else:
            raise FileNotFoundError(f"{h5filename} not found")

    def print_variables(self):
        pass


class DeltaVlasov_externalE(AbstractPDEtxv):
    r"""
    Equation:

    .. math::

      \frac{df}{dt} + v \frac{df}{dx} + E \frac{df}{dv} = 0

    Domain: :math:` x\in [0, 2 pi/k]`, with :math:`k = 1/2 ; t\in[0, 50] ; v \in [-6, 6]`

    BC: periodic boundary condition in x
    IC:

    .. math::

       f_0(x,v) = ( 1 + eps * \cos(k x) ) / ( sqrt(2\pi \sigma^2) ) * exp(- \frac{v^2}{2\sigma^2} )

    We learn  :math:`u_{\theta}(t,x,v)`
    parametric model :math:`f_{net} = f_0 + t u_{\theta}(t,x,v)`
    """

    def __init__(
        self,
        filename_simu,
        loss_with_BC=True,
        loss_with_IC=True,
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=[],
            space_domain=SquareDomain(1, [[0.0, 1.0]]),
            velocity_domain=SquareDomain(1, [[-6.0, 6.0]]),
            nb_parameters=0,
            parameter_domain=[],
            data_construction="grid_sampled",  # "sampled",
        )

        """
        remarque tmp: dans inputs qui rentre dans la fonction forward on concatenne [t,x,v,mu,a]
        --> IL faut donc adapter la taille de in_features dans le network qui est MLP
        -> mettre in_size = sum(dim_t+_x+_v+mu+_a)
        -> voir fichier MLP !!
        """
        self.simu = LoadSimu(filename_simu)

        self.time_domain = self.simu.time_domain
        self.space_domain = self.simu.space_domain
        self.dimension_x = self.space_domain.dim
        self.velocity_domain = self.simu.velocity_domain
        self.dimension_v = self.velocity_domain.dim

        self.k = self.simu.kx
        self.eps = self.simu.epsilon
        self.sigma = np.sqrt(self.simu.Teq)
        self.t_min, self.t_max = self.time_domain

        self.first_derivative_t = True
        self.first_derivative_x = True
        self.first_derivative_v = True

        self.loss_with_BC = loss_with_BC
        self.loss_with_IC = loss_with_IC

    def residual(self, w, t, x, v, mu, **kwargs):
        """
        for this equation, 'a' represents the electric field
        """
        a = kwargs.get("a")
        u_t = self.get_variables(w, "w_t")
        u_x = self.get_variables(w, "w_x")
        u_v = self.get_variables(w, "w_v1")
        return u_t + v * u_x + a * u_v

    def bc_residual(self, w, t, x, v, mu, **kwargs):
        u = self.get_variables(w, "w")
        return u

    def initial_condition(self, x, v, mu, **kwargs):
        x = x.get_coordinates()
        return (
            (1.0 + self.eps * torch.cos(self.k * x))
            * (1.0 / (np.sqrt(2 * torch.pi * self.sigma**2)))
            * torch.exp(-(v**2) / (2.0 * self.sigma**2))
        )

    def initial_condition_simu(self, ix, iv, imu):
        return self.simu.f[0, ix, iv]

    def reference_solution(self, t, x, v, mu):
        pass

    def post_processing(self, t, x, v, mu, w):
        return (1.0 / (np.sqrt(2 * torch.pi * self.sigma**2))) * torch.exp(
            -(v**2) / (2.0 * self.sigma**2)
        ) + w

    def gives_sampler(self, sampler):
        self.sampler = sampler

    def make_data(self, n_points):
        t, x, v, mu, a, f = self.sampler.sampling(n_points)
        return t, x, v, mu, a, f


# class LoadSimu:
#    def __init__(self, h5filename):
#        if os.path.exists(h5filename):
#            with h5py.File(h5filename, "r") as h5f:
#                self.x = h5f["xgrid"][()]
#                self.v = h5f["vgrid"][()]
#                self.t = h5f["timegrid"][()]
#                self.f = h5f["fdistribu"][()]
#                self.feq = h5f["feq"][()]
#                self.E = h5f["efield"][()]
#                self.time_domain = [self.t[0], self.t[-1]]
#                self.space_domain = SquareDomain(1, [[self.x[0], self.x[-1]]])
#                self.velocity_domain = SquareDomain(1, [[self.v[0], self.v[-1]]])
#        else:
#            raise FileNotFoundError(f"{h5filename} not found")
#
#    def print_variables(self):
#        pass
