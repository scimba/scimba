from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes
from shape2D import Bean, run_shape2D

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

current = Path.cwd()


class SDEikonalLap(domain.SignedDistance):
    def __init__(self, threshold: float = 0.0):
        super().__init__(2, threshold)

        # self.bound_box = [[form.bord_a,form.bord_b],[form.bord_a2,form.bord_b2]]

        self.eik_pinns, self.form_trainer = run_shape2D(new_training=False)
        self.pde = self.eik_pinns.pde

        self.mu = torch.tensor([])

    def sdf(self, x):
        """Level set function for the circle domain

        :param X: (x,y) coordinates
        :return: Level set function evaluated at (x,y)
        """
        return self.eik_pinns(x, self.mu)


class Poisson2DSD(pdes.AbstractPDEx):
    def __init__(self, space_domain: domain.SignedDistance):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=0,
            parameter_domain=[],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 1.0
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        mul = self.space_domain.large_domain.sdf(x)
        return mul * w

    # def get_mul(self,x):
    #     return self.space_domain.sdf(x)

    def reference_solution(self, x, mu):
        print("shape : ", torch.ones_like(x).shape)
        return torch.ones_like(x)


def run_laplacian2D(new_training=False):
    form = Bean()
    sd_function = SDEikonalLap()
    xdomain = domain.SpaceDomain(
        2, domain.SignedDistanceBasedDomain(2, form.bound_box, sd_function)
    )
    pde = Poisson2DSD(xdomain)

    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test_poisson.pth"

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = 6 * [64]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=0.007, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=2000,
    )

    if new_training:
        trainer.train(epochs=1000, n_collocation=2000, n_data=0)

    class_name = form.__class__.__name__
    results_dir = Path.cwd() / Path("results")
    if not results_dir.exists():
        results_dir.mkdir()
    trainer.plot(20000)

    return pinn, trainer


if __name__ == "__main__":
    pinn, trainer = run_laplacian2D()
