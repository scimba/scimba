from pathlib import Path

import scimba.nets.training as training
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes
from scimba.numericalmethods import finite_element, mesh

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class LaplacianSine(pdes.AbstractPDEx):
    r"""

    .. math::

        \frac{d^2v}{dx^2} + 4\pi^2 \sin{2\pi k x}

    """

    def __init__(self, k, domain=domain.SquareDomain(1, [[0.0, 1.0]])):
        super().__init__(
            nb_unknowns=1,
            space_domain=domain,
            nb_parameters=1,
            parameter_domain=[[1.0, 1.000001]],
        )

        self.k = k
        self.first_derivative = True
        self.second_derivative = False

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        f = (2.0 * self.k * torch.pi) ** 2.0 * torch.sin(2 * self.k * torch.pi * x)
        u_x = self.get_variables(w, "w_x")
        u = self.get_variables(w, "w")
        return 0.5 * u_x * u_x - f * u

    def make_data(self, n_data):
        pass

    def reference_solution(self, x, mu):
        k0 = 2 * self.k * torch.pi
        return torch.sin(x * k0)


def main():
    xdomain = domain.SquareDomain(1, [[0.0, 1.0]])
    pde = LaplacianSine(k=1, domain=xdomain)
    Mh = mesh.UniformMesh(xdomain, [50])
    Mh.build()
    LocQ = mesh.LocalQuad1D(3)
    x_sampler = sampling_pde.XSampler(pde=pde)  # mesh.Sampling_MeshQuad1D(LocQ,Mh)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "laplacianSine1D_fe.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    network = finite_element.ScalarLagrangeFE1D_Parameters(Mh, LocQ)
    pinn = pinn_x.PINNx(network, pde)

    residual_loss = training.MassLoss()
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        residual_f_loss=residual_loss,
        file_name=file_name,
        bc_loss_bool=True,
        learning_rate=1.2e-1,
        decay=1.0,
        batch_size=2000,
        w_data=0,
        w_res=1,
        w_bc=100,
    )

    trainer.train(epochs=800, n_collocation=2000, n_bc_collocation=2000, n_data=0)
    trainer.plot()


if __name__ == "__main__":
    main()
