from pathlib import Path

# Import modules
import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_tx as pinn_tx
import scimba.pinns.training_tx as training_tx
import scimba.sampling.sampling_ode as sampling_ode
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class AdvectionDiffusion2D(pdes.AbstractPDEtx):
    def __init__(self, tdomain, xdomain, p_domain=[[1.0, 1.00001]]):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def bc_residual(self, w, t, x, mu):
        u = self.get_variables(w)
        return u

    def initial_condition(self, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        x1_t = 0.4
        x2_t = 0.0
        sig0 = 0.1
        c_t = 1.0
        sig2 = sig0 * sig0
        f = torch.exp(-((x1 - x1_t) ** 2.0 + (x2 - x2_t) ** 2.0) / (sig2))
        return c_t * f + 1.0

    def residual(self, w, t, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        u_t = self.get_variables(w, "w_t")
        u_x = self.get_variables(w, "w_x")
        u_xx = self.get_variables(w, "w_xx")
        u_y = self.get_variables(w, "w_y")
        u_yy = self.get_variables(w, "w_yy")
        ax = -2.0 * PI * x2  # mu[:,0,None]
        ay = 2.0 * PI * x1  # mu[:,0,None]
        D = 0.02  # mu[:,1,None]
        return u_t + (ax * u_x + ay * u_y - D * (u_xx + u_yy))

    def reference_solution(self, t, x, mu):
        x1, x2 = x.get_coordinates()
        x1_t = 0.4 * torch.cos(2.0 * PI * t)
        x2_t = 0.4 * torch.sin(2.0 * PI * t)
        D = 0.02  # mu[:,1,None]
        sig0 = 0.1
        c_t = sig0 * sig0 / (4 * D * t + sig0 * sig0)
        sig2 = 4 * D * t + sig0 * sig0
        f = torch.exp(-((x1 - x1_t) ** 2.0 + (x2 - x2_t) ** 2.0) / (sig2))
        return (c_t * f + 1.0)[:, None]


# Test with the Heat equation inside the module PINNs
# where initial and BC are strongly imposed
def main():
    xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.0, 0.0], 0.7))
    pde = AdvectionDiffusion2D(tdomain=[0.0, 0.25], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [26, 26, 26, 26]
    network = pinn_tx.MLP_tx(pde=pde, layer_sizes=tlayers, activation_type="tanh")
    pinn = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=False, init_loss_bool=True, w_res=0.1, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=3000,
    )
    if new_training:
        trainer.train(epochs=2000, n_collocation=4000, n_init_collocation=4000)

    trainer.plot(2000, reference_solution=True)


if __name__ == "__main__":
    main()
