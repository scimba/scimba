import copy
from pathlib import Path

import torch
from scimba import device
from scimba.nets.training_tools import OptimizerData
from scimba.pinns.pinn_losses import PinnLossesData


class TrainerPinnVlasov:
    """
    In this class, we train a network that depends on t,x,v with a PINN method.
    """

    FOLDER_FOR_SAVED_NETWORKS = "networks"
    DEFAULT_FILE_NAME = "PINN_txv"
    DEFAULT_BATCH_SIZE = 128

    def __init__(self, pde, network, **kwargs):
        self.pde = pde
        self.network = network
        self.sampler = self.pde.sampler

        self.optimizers = kwargs.get("optimizers", OptimizerData(**kwargs))
        self.losses = kwargs.get("losses", PinnLossesData(**kwargs))

        self.folder_for_saved_networks = kwargs.get(
            "folder_for_saved_networks", self.FOLDER_FOR_SAVED_NETWORKS
        )
        Path(self.folder_for_saved_networks).mkdir(parents=True, exist_ok=True)

        file_name = kwargs.get("file_name", self.DEFAULT_FILE_NAME)
        self.file_path = Path.cwd() / Path(self.folder_for_saved_networks) / file_name

        #VG?#self.batch_size = kwargs.get("batch_size", 1000)
        self.batch_size = kwargs.get("batch_size", self.DEFAULT_BATCH_SIZE)

        self.create_network()
        self.load(self.file_path)

        self.t_collocation = None
        self.x_collocation = None
        self.v_collocation = None
        self.bc_x_collocation = None
        self.bc_v_collocation = None
        self.mu_collocation = None
        self.a_collocation = None

        self.boundary = "periodic"

    def __call__(self, t, x, v, mu):
        return self.net(t, x, v, mu)

    def create_network(self):
        """
        Create the neural network and associated optimizers.

        This function creates four elements:
            - self.net, the neural network
            - the optimizer data which init the onr or two optimizer and the
            parameters associated
        """
        self.net = self.network.to(device)
        self.optimizers.create_first_opt(self.net.parameters())

    def load(self, file_name: str):
        """Load the network and optimizers from a file.

        :params file_name: name of the .pth file containing the network, losses and optimizers
        :type file_name: str
        """
        try:
            try:
                checkpoint = torch.load(file_name)
            except RuntimeError:
                checkpoint = torch.load(file_name, map_location=torch.device("cpu"))

            self.net.load_state_dict(checkpoint["model_state_dict"])
            self.optimizers.load(self.net.parameters(), checkpoint)
            self.losses.load(checkpoint)

            self.to_be_trained = False
            print("network loaded")

        except FileNotFoundError:
            self.to_be_trained = True
            print("network was not loaded from file: training needed")

    def save(
        self,
        file_name: str,
        epoch: int,
        net_state: dict,
        loss: torch.Tensor,
    ):
        """Save the network and optimizers to a file."""
        dic1 = {epoch: epoch, "model_state_dict": net_state}
        dic2 = self.optimizers.dict_for_save()
        dic3 = self.losses.dict_for_save(loss)
        dic1.update(dic2)
        dic1.update(dic3)
        torch.save(dic1, file_name)

    def residual(self, t, x, v, mu, **kwargs):
        #  TODO: maybe some of those need "retain_graph=True" in the case
        #        of a xy_derivative (not yet implemented)

        w = self.network.setup_w_dict(t, x, v, mu)
        #  u contains the derivatives
        self.network.get_first_derivatives(w, t, x, v)
        return self.pde.residual(w, t, x, v, mu, **kwargs)

    def bc_residual(self, t, x, v, mu, **kwargs):
        w = self.network.setup_w_dict(t, x, v, mu)
        return self.pde.bc_residual(w, t, x, v, mu, **kwargs)

    def train(self, **kwargs):
        epochs = kwargs.get("epochs", 500)
        n_collocation = kwargs.get("n_collocation", 1_000)
        n_bc_collocation = kwargs.get("n_bc_collocation", 0)
        n_init_collocation = kwargs.get("n_init_collocation", 0)
        n_data = kwargs.get("n_data", 0)

        """
        Define data for computing the data_loss function
        """
        if self.losses.data_loss_bool and n_data > 0:
            (
                self.input_t,
                self.input_x,
                self.input_v,
                self.input_mu,
                self_input_a,
                self.output,
            ) = self.pde.make_data(n_data)

        try:
            best_loss_value = self.losses.loss.item()
        except AttributeError:
            best_loss_value = 1e10

        if n_collocation == 0:
            m = self.input_x.shape[0]
        if n_data == 0:
            m = n_collocation
        if n_data > 0 and n_collocation > 0:
            m = min(self.input_x.shape[0], n_collocation)  # VG ?: Why this choice ?

        epoch = 0
        self.epoch_tot = epoch
        for epoch in range(epochs):
            permutation = torch.randperm(m)
            self.epoch_tot = self.epoch_tot + 1
            for i in range(0, m, self.batch_size):
                indices = permutation[i : i + self.batch_size]

                def closure():
                    if self.optimizers.second_opt_activated:
                        self.optimizers.second_opt.zero_grad()
                    else:
                        self.optimizers.first_opt.zero_grad()

                    self.losses.init_losses()

                    """
                    Computation of the loss function of the residual (the pde)
                    """
                    if n_collocation > 0:
                        (
                            self.t_collocation,
                            self.x_collocation,
                            self.v_collocation,
                            self.mu_collocation,
                            self.a_collocation,
                            _,
                        ) = self.sampler.sampling(n_collocation)

                        f_out = self.residual(
                            self.t_collocation,
                            self.x_collocation,
                            self.v_collocation,
                            self.mu_collocation,
                            a=self.a_collocation,
                        )

                        weights = self.sampler.density(
                            self.t_collocation,
                            self.x_collocation,
                            self.v_collocation,
                            self.mu_collocation,
                        )
                        zeros = torch.zeros_like(f_out)
                        self.losses.update_residual_loss(
                            self.losses.residual_f_loss(f_out / weights, zeros)
                        )

                    """
                    Computation of the loss function of the boundary condition
                    --> Imposed periodic boundary conditions in x and v
                    """
                    if self.losses.bc_loss_bool and n_bc_collocation > 0:
                        (
                            batch_bc_t,
                            batch_bc_x,  # (data_x_left, data_x_right, data_x_var)
                            batch_bc_v,  # (data_v_left, data_v_right, data_v_var)
                            batch_bc_mu,
                        ) = self.sampler.bc_sampling_periodic(n_bc_collocation)

                        bc_out_x_left = self.bc_residual(
                            batch_bc_t,
                            batch_bc_x[0],  # data_x_left
                            batch_bc_v[2],  # data_v_var
                            batch_bc_mu,
                            **kwargs,
                        )
                        bc_out_x_right = self.bc_residual(
                            batch_bc_t,
                            batch_bc_x[1],  # data_x_right
                            batch_bc_v[2],  # data_v_var
                            batch_bc_mu,
                            **kwargs,
                        )
                        bc_out_v_left = self.bc_residual(
                            batch_bc_t,
                            batch_bc_x[2],  # data_x_var
                            batch_bc_v[0],  # data_v_left
                            batch_bc_mu,
                            **kwargs,
                        )
                        bc_out_v_right = self.bc_residual(
                            batch_bc_t,
                            batch_bc_x[2],  # data_x_var
                            batch_bc_v[1],  # data_v_right
                            batch_bc_mu,
                            **kwargs,
                        )

                        res1 = self.losses.bc_f_loss(bc_out_x_left, bc_out_x_right)
                        res2 = self.losses.bc_f_loss(bc_out_v_left, bc_out_v_right)
                        self.losses.update_bc_loss(res1 + res2)

                    """
                    Computation of the loss function of the initial condition
                    """
                    if self.losses.init_loss_bool and n_init_collocation > 0:
                        (
                            batch_ic_t,
                            batch_ic_x,
                            batch_ic_v,
                            batch_ic_mu,
                            batch_ic_a,
                            bi,
                        ) = self.sampler.ic_sampling(n_init_collocation)

                        w = self.network.setup_w_dict(
                            batch_ic_t, batch_ic_x, batch_ic_v, batch_ic_mu
                        )
                        res = self.losses.init_f_loss(w["w"], bi)
                        self.losses.update_init_loss(res)

                    """
                    Computation of the loss function of the data
                    """
                    if self.losses.data_loss_bool:
                        batch_t, batch_x, batch_v, batch_mu, batch_w = (
                            self.input_t[indices],
                            self.input_x[indices],
                            self.input_v[indices],
                            self.input_mu[indices],
                            self.output[indices],
                        )

                        prediction = self.network.get_w(
                            batch_t, batch_x, batch_v, batch_mu
                        )
                        self.losses.update_data_loss(
                            self.losses.data_f_loss(prediction, batch_w)
                        )

                    self.losses.compute_full_loss(self.optimizers, epoch)
                    self.losses.loss.backward(retain_graph=True)
                    return self.losses.loss

                if self.optimizers.second_opt_activated:
                    self.optimizers.second_opt.step(closure)
                else:
                    closure()
                    self.optimizers.first_opt.step()
                    self.optimizers.scheduler.step()

            self.losses.update_histories()

            if epoch % 500 == 0:
                print(
                    f"epoch {epoch: 5d}: current loss = {self.losses.loss.item():5.2e}"
                )

            if (self.losses.loss.item() < best_loss_value) | (epoch == 0):
                string = (
                    f"epoch {epoch: 5d}: best loss = {self.losses.loss.item():5.2e}"
                )
                if self.optimizers.second_opt_activated:
                    string += "; LBFGS activated"
                print(string)

                best_loss = self.losses.loss.clone()
                best_loss_value = best_loss.item()
                best_net = copy.deepcopy(self.net.state_dict())
                self.optimizers.update_best_opt()

            if epoch == 0:
                initial_loss = self.losses.loss.item()
            else:
                self.optimizers.test_activation_second_opt(
                    self.net.parameters(),
                    self.losses.loss_history,
                    self.losses.loss.item(),
                    initial_loss,
                    epoch=epoch,
                )

        print(f"epoch {epochs: 5d}: current loss = {self.losses.loss.item():5.2e}")
        try:
            self.save(
                self.file_path,
                epoch,
                best_net,
                best_loss,
            )
            print("load network:", self.file_path)
            self.load(self.file_path)
        except UnboundLocalError:
            print("save not work")
            pass
