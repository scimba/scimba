#!/bin/bash

#--------------------------------------------------------------
# Bash script used to launch `ex_PINN_Vlasov_1x1v.py` 
#  interactively or in batch depending on ARCH variable
#
# Rk: On Adastra GPU partition -> export ARCH="adastra_MI250"
#  To be able to use `idr_accelerate` you need to install it
#     in your PYTHON environment
# pip3 install transformers deepspeed accelerate datasets torchmetrics idr-torch idr_accelerate
#--------------------------------------------------------------
#
if [ $# -lt 1 ]
then
   echo " Syntaxe:  subscimba.sh <INPUTFILE_YAML>"
   exit
fi

arg=$#
INPUTFILE_YAML=$1
INPUTFILE_YAML_noext=$(basename ${INPUTFILE_YAML})
INPUTFILE_YAML_noext=${INPUTFILE_YAML_noext%.*}
go_file_cmd="go_${INPUTFILE_YAML_noext}.out"
cat <<EOF > ${go_file_cmd}
#!/bin/bash
EOF

CASENAME=`basename ${INPUTFILE_YAML_noext} | tr '[:lower:]' '[:upper:]'`
CASENAME="D_${CASENAME}"
RESDIR="$PWD/${CASENAME}"
if [ ! -d ${RESDIR} ]
then
   mkdir -p ${RESDIR}
fi

OUTPUT_FILE="${RESDIR}/scimba_pinn.out"

case ${ARCH} in
adastra_MI250)
cat <<EOF >> ${go_file_cmd}
#SBATCH --account=cin4469
#SBATCH --job-name=scimba
#SBATCH --constraint=MI250
#SBATCH --nodes=1
#SBATCH --exclusive
#SBATCH --output=%A.out
#SBATCH --time=02:00:00

#VG#module load rocm/6.0.0 
#VG#module load pytorch-gpu/py3

module list

# Set ROCm environment variables
export ROCM_PATH=/opt/rocm-5.2.3
export HIP_PATH=$ROCM_PATH
export LD_LIBRARY_PATH=$ROCM_PATH/lib:$LD_LIBRARY_PATH
export PATH=$ROCM_PATH/bin:$PATH

# Use a Python venv unless you install everything in your home.
source ~/work_cin4469/bin/scimba_devel_env/bin/activate
echo "python " `which python`

srun \
    --ntasks-per-node=1 \
    --gpus-per-task=1 \
    --cpu-bind=none \
    --mem-bind=none \
    --label \
    -- python3 ex_PINNvlasov_1x1v.py -i ${INPUTFILE_YAML} -o ${RESDIR} >> ${OUTPUT_FILE}

EOF
CMD_SUB="sbatch ${go_file_cmd}"
;;

jeanzay)
cat <<EOF >> ${go_file_cmd}
#SBATCH --job-name=scimba
#SBATCH --nodes=1                    # we request one node
#SBATCH --ntasks-per-node=1          # with one task per node (= number of GPUs here)
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-task=10
##SBATCH --ntasks=1
#SBATCH --time=02:00:00
#SBATCH --partition=gpu_p2l
#SBATCH -o scimba%j.out
#SBATCH -e scimba%j.err
#SBATCH --qos=qos_gpu-dev
#SBATCH -A jeo@v100
set +v

module load pytorch-gpu/py3/2.2.0
source /linkhome/rech/gencea04/ulu57gz/jeo_work/bin/scimba_env/bin/activate

srun python ex_PINNvlasov_1x1v.py -i ${INPUTFILE_YAML} -o ${RESDIR} >> ${OUTPUT_FILE}
EOF
CMD_SUB="sbatch ${go_file_cmd}"
;;
*)
cat <<EOF > ${go_file_cmd}
python3 ex_PINNvlasov_1x1v.py -i ${INPUTFILE_YAML} -o ${RESDIR} >> ${OUTPUT_FILE} &
EOF
CMD_SUB="./${go_file_cmd}"
esac

chmod +x ${go_file_cmd}
${CMD_SUB}

