import random
import time
from math import pi as PI
from pathlib import Path

import numpy as np
import torch

from scimba.equations import domain, pdes
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling


class PoissonSquare2D_StrongBC(pdes.AbstractPDEx):
    def __init__(self, space_domain, frequency=1):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[1.0, 2.0], [1.0, 2.0]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.frequency = frequency

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def post_processing(self, x, mu, w, **kwargs):
        x1, x2 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * w

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        u = self.get_variables(w)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        k = self.frequency
        f = (
            α
            * β
            * (β - 2 * k**2 * PI**2)
            * torch.sin(k * PI * x1)
            * torch.sin(k * PI * x2)
        )
        return u_xx + u_yy + β * u - f

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        k = self.frequency
        return α * β * torch.sin(k * PI * x1) * torch.sin(k * PI * x2)


class PoissonSquare2D_WeakBC(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[1.0, 2.0], [1.0, 2.0]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        u = self.get_variables(w)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = α * β * (β - 2 * PI**2) * torch.sin(PI * x1) * torch.sin(PI * x2)
        return u_xx + u_yy + β * u - f

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        return α * β * torch.sin(PI * x1) * torch.sin(PI * x2)


class PoissonDisk2D_StrongBC(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[1.0, 2.0], [1.0, 2.0]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def post_processing(self, x, mu, w, **kwargs):
        x1, x2 = x.get_coordinates()
        r = self.space_domain.large_domain.radius
        x1_0, x2_0 = self.space_domain.large_domain.center
        return (r**2 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2) * w

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        u = self.get_variables(w)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")

        r = self.space_domain.large_domain.radius
        x1_0, x2_0 = self.space_domain.large_domain.center

        arg = r**2 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2
        sin = torch.sin(arg)
        cos = torch.cos(arg)

        f = α * β * (β * sin - 4 * (((x1 - x1_0) ** 2 + (x2 - x2_0) ** 2) * sin + cos))

        return u_xx + u_yy + β * u - f

    def reference_solution(self, x, mu):
        α, β = self.get_parameters(mu)
        x1, x2 = x.get_coordinates()
        r = self.space_domain.large_domain.radius
        x1_0, x2_0 = self.space_domain.large_domain.center
        return α * β * torch.sin(r**2 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2)


class PoissonDisk2D_WeakBC(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[1.0, 2.0], [1.0, 2.0]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        α, β = self.get_parameters(mu)
        u = self.get_variables(w)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")

        r = self.space_domain.large_domain.radius
        x1_0, x2_0 = self.space_domain.large_domain.center

        arg = r**2 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2
        sin = torch.sin(arg)
        cos = torch.cos(arg)

        f = α * β * (β * sin - 4 * (((x1 - x1_0) ** 2 + (x2 - x2_0) ** 2) * sin + cos))

        return u_xx + u_yy + β * u - f

    def reference_solution(self, x, mu):
        α, β = self.get_parameters(mu)
        x1, x2 = x.get_coordinates()
        r = self.space_domain.large_domain.radius
        x1_0, x2_0 = self.space_domain.large_domain.center
        return α * β * torch.sin(r**2 - (x1 - x1_0) ** 2 - (x2 - x2_0) ** 2)


def disk_to_potato(x):
    x1, x2 = (x[:, i, None] for i in range(2))
    x = x1 - 0.5 * x2**2 + 0.3 * torch.sin(x2)
    y = x2 + 0.1 * x + 0.12 * torch.cos(x)
    return torch.cat((x, y), axis=1)


def inverse_disk_to_potato(x):
    x1, x2 = (x[:, i, None] for i in range(2))
    y = x2 - 0.1 * x1 - 0.12 * torch.cos(x1)
    x = x1 + 0.5 * y**2 - 0.3 * torch.sin(y)
    return torch.cat((x, y), axis=1)


def Jacobian_disk_to_potato(x):
    x1, x2 = (x[:, i, None] for i in range(2))
    raise ValueError("Jacobian_disk_to_potato is not implemented")
    return 0, 0, 0, 0


def compute_holey_domain():
    xdomain = domain.DiskBasedDomain(
        2,
        [0.0, 0.0],
        1.0,
        mapping=disk_to_potato,
        inverse=inverse_disk_to_potato,
        Jacobian=Jacobian_disk_to_potato,
    )
    fulldomain = domain.SpaceDomain(2, xdomain)

    def f(t):
        return torch.cat([torch.cos(2.0 * PI * t), torch.sin(2.0 * PI * t)], axis=1)

    bc1 = domain.ParametricCurveBasedDomain(2, [[0.0, 0.5]], f)
    bc2 = domain.ParametricCurveBasedDomain(2, [[0.5, 1.0]], f)

    fulldomain.add_bc_subdomain(bc1)
    fulldomain.add_bc_subdomain(bc2)

    # Hole and its signed distance function (sdf)
    class Hole(domain.SignedDistance):
        def __init__(self):
            super().__init__(dim=2)

        def sdf(self, x):
            x1, x2 = x.get_coordinates()
            center = [-0.2, -0.2]
            radius = 0.2
            return (x1 - center[0]) ** 2 + (x2 - center[1]) ** 2 - radius**2

    # parametric curve that describes the hole
    def bchole(t):
        center = [-0.2, -0.2]
        radius = 0.2
        return torch.cat(
            [
                center[0] - radius * torch.cos(2 * PI * t),
                center[1] - radius * torch.sin(2 * PI * t),
            ],
            axis=1,
        )

    # Inclusion subdomain and its signed distance function (sdf)
    class Inclusion(domain.SignedDistance):
        def __init__(self):
            super().__init__(dim=2)

        def sdf(self, x):
            x1, x2 = x.get_coordinates()
            center = [0.2, 0.2]
            radius = 0.2
            return (x1 - center[0]) ** 2 + (x2 - center[1]) ** 2 - radius**2

    # parametric curve that describes the inclusion
    def bcinclusion(t):
        center = [0.2, 0.2]
        radius = 0.2
        return torch.cat(
            [
                center[0] - radius * torch.cos(2 * PI * t),
                center[1] - radius * torch.sin(2 * PI * t),
            ],
            axis=1,
        )

    sdf = Hole()
    hole = domain.SignedDistanceBasedDomain(2, [[0.0, 1.0], [0.0, 1.0]], sdf)
    bc_hole = domain.ParametricCurveBasedDomain(2, [[0.0, 1.0]], bchole)
    hole.add_bc_subdomain(bc_hole)

    sdf = Inclusion()
    inc = domain.SignedDistanceBasedDomain(2, [[0.0, 1.0], [0.0, 1.0]], sdf)
    bc_inc = domain.ParametricCurveBasedDomain(2, [[0.0, 1.0]], bcinclusion)
    inc.add_bc_subdomain(bc_inc)

    fulldomain.add_hole(hole)

    return fulldomain


def Run_laplacian2D(
    pde, bc_loss=False, w_bc=0, w_res=1.0, epochs=2500, file_name="test", Fourier=False
):
    random.seed(1337)
    np.random.seed(1337)
    torch.manual_seed(1337)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name += ".pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 20, 20, 20, 20]

    if Fourier:
        network = pinn_x.Fourier_x(
            pde=pde,
            mean_features={"x": 0.0, "mu": 0.0, "xmu": 0.0},
            std_features={"x": 2.5, "mu": 0.0, "xmu": 0.0},
            nb_features=20,
            layer_sizes=tlayers,
            activation_type="sine",
        )
    else:
        network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")

    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(bc_loss_bool=bc_loss, w_res=w_res, w_bc=w_bc)
    optimizers = training_tools.OptimizerData(learning_rate=1.2e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
    )

    if not bc_loss:
        if new_training:
            trainer.train(epochs=epochs, n_collocation=5000, n_data=0)
    else:
        if new_training:
            trainer.train(
                epochs=epochs, n_collocation=5000, n_bc_collocation=1000, n_data=0
            )

    return network, trainer


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    plt.rcParams["axes.facecolor"] = (251 / 255, 251 / 255, 251 / 255)
    plt.rcParams["figure.facecolor"] = (251 / 255, 251 / 255, 251 / 255)

    times = []

    # Poisson on square with strong BC

    file_name = "strong_square"

    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = PoissonSquare2D_StrongBC(xdomain)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, error=True, seed=seed, random=True
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on square with weak BC

    file_name = "weak_square"

    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = PoissonSquare2D_WeakBC(xdomain)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde, bc_loss=True, w_bc=100)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, error=True, seed=seed, random=True
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on disk with strong BC

    file_name = "strong_disk"

    xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.5, 0.5], 1.0))
    pde = PoissonDisk2D_StrongBC(xdomain)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, error=True, seed=seed, random=True
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on potato with weak BC

    file_name = "weak_potato"

    xdomain = domain.SpaceDomain(
        2,
        domain.DiskBasedDomain(
            2,
            [0.0, 0.0],
            1.0,
            mapping=disk_to_potato,
            inverse=inverse_disk_to_potato,
            Jacobian=Jacobian_disk_to_potato,
        ),
    )
    pde = PoissonDisk2D_WeakBC(xdomain)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde, bc_loss=True, w_bc=100, file_name=file_name)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, residual=True, seed=seed, random=True, limit_res=0.1
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on holed potato with weak BC

    file_name = "weak_holed_potato"

    xdomain = compute_holey_domain()
    pde = PoissonDisk2D_WeakBC(xdomain)

    start = time.perf_counter()
    network_wh_potato, trainer_wh_potato = Run_laplacian2D(pde, bc_loss=True, w_bc=100)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer_wh_potato.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer_wh_potato.plot_2d_contourf(
            draw_contours=True, seed=seed, random=True
        )
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer_wh_potato.plot_2d_contourf(
            draw_contours=True, residual=True, seed=seed, random=True, limit_res=0.05
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on square with strong BC, high frequency, Fourier features

    file_name = "strong_square_high_frequency"

    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = PoissonSquare2D_StrongBC(xdomain, frequency=8)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde, Fourier=True)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, error=True, seed=seed, random=True
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # Poisson on square with strong BC, high frequency, no Fourier features

    file_name = "strong_square_high_frequency_no_Fourier"

    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = PoissonSquare2D_StrongBC(xdomain, frequency=8)

    start = time.perf_counter()
    network, trainer = Run_laplacian2D(pde)
    times.append(time.perf_counter() - start)

    fig, ax = plt.subplots()
    trainer.losses.plot(ax)
    fig.savefig(f"losses_{file_name}.pdf")

    for seed in range(1, 4):
        fig, ax = trainer.plot_2d_contourf(draw_contours=True, seed=seed, random=True)
        fig.savefig(f"prediction_{file_name}_{seed}.pdf")
        fig, ax = trainer.plot_2d_contourf(
            draw_contours=True, error=True, seed=seed, random=True
        )
        fig.savefig(f"error_{file_name}_{seed}.pdf")

    # print elapsed times

    for elapsed_time in times:
        print(f"elapsed time: {elapsed_time:.2f} seconds")
