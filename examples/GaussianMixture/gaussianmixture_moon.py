from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import torch

try:
    from sklearn import datasets
except ModuleNotFoundError:
    exit("Please install scikit-learn")

from scimba.generativenets import (
    gaussian_mixtures,
    generativelosses,
    trainer_likelihood,
)
from scimba.nets.training_tools import OptimizerData


class DatasetMoons:
    """two half-moons"""

    def sample(self, n):
        moons = datasets.make_moons(n_samples=n, noise=0.05)[0].astype(np.float32)
        return torch.from_numpy(moons)


def main():
    data = DatasetMoons()

    x = data.sample(1024)
    plt.figure(figsize=(6, 6))
    plt.scatter(x[:, 0], x[:, 1], s=5, alpha=0.5)
    plt.axis("equal")
    plt.show()

    out_size = 2
    mixture = gaussian_mixtures.GaussianMixtures(dim=out_size, nb_gaussians=100)

    file_name = "mixture_gaussian.pth"
    (
        Path.cwd()
        / Path(trainer_likelihood.TrainerLikelihood.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    ).unlink(missing_ok=True)

    losses = generativelosses.GenerativeLossesData()
    optimizers = OptimizerData(learning_rate=2.2e-2, decay=0.99)
    Trainer = trainer_likelihood.TrainerLikelihood(
        out_size=2,
        output_data=x,
        network=mixture,
        losses=losses,
        optimizers=optimizers,
        batch_size=128,
    )
    Trainer.train(epochs=1200)

    z = mixture.sample(num_samples=512)
    z = z.detach().numpy()
    plt.subplot(122)
    plt.scatter(x[:, 0].detach().cpu(), x[:, 1].detach().cpu(), c="b", s=5, alpha=0.5)
    plt.scatter(z[:, 0], z[:, 1], c="r", s=5, alpha=0.5)
    plt.legend(["data", "z->x"])
    plt.axis("scaled")
    plt.title("z -> x")
    plt.show()


if __name__ == "__main__":
    main()
