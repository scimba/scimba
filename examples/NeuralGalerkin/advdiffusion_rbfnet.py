import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.neuralgalerkin.neural_galerkin_x as ng
import scimba.pinns.pinn_x as pinn_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

PI = 3.14159265358979323846


class AdvectionDiffusion(pdes.AbstractPDEx):
    def __init__(
        self, xdomain=domain.SquareDomain(1, [[0.0, 1.0]]), p_domain=[[1.0, 1.00001]]
    ):
        super().__init__(
            nb_unknowns=1,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative = True
        self.second_derivative = True

    def bc_residual(self, w, x, mu):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        u_x = self.get_variables(w, "w_x")
        u_xx = self.get_variables(w, "w_xx")
        a = 1.0  # mu[:,0,None]
        D = 0.05  # mu[:,1,None]
        return -(a * u_x - D * u_xx)


def sol_ref(t, x, mu):
    x = x.get_coordinates()
    a = 1.0  # mu[:,0,None]
    D = 0.05  # mu[:,1,None]
    sig0 = 0.1
    c_t = sig0 / ((4 * D * t + sig0 * sig0) ** 0.5)
    sig2 = 4 * D * t + sig0 * sig0
    f = torch.exp(-((x - 0.6 - a * t) ** 2.0) / (sig2))
    return c_t * f + 1.0


def projector_heatsol(net):
    state_dict = net.state_dict()
    m = state_dict["net.net.layers.0.activation.0.m"]
    transformed_m = torch.tensor([0.6, 1.0])
    m.copy_(transformed_m)
    sig = state_dict["net.net.layers.0.activation.0.sig"]
    transformed_sig = 0.1
    sig.copy_(transformed_sig)
    w = state_dict["net.net.layers.0.layer.weight"]
    transformed_w = 1.0
    w.copy_(transformed_w)
    b = state_dict["net.net.layers.0.layer.bias"]
    transformed_b = 1.0
    b.copy_(transformed_b)


def main():
    xdomain = domain.SpaceDomain(1,domain.SquareDomain(1, [[0.0, 1.2]]))
    pde = AdvectionDiffusion(xdomain=xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    network = pinn_x.RBFNet_x(pde=pde, 
                                sampler=sampler,
                                nb_func=1,
                                type_g="isotropic",
                                norm=2)
    pinn = pinn_x.PINNx(network, pde)
    print(network)

    ### create the model
    model = ng.NeuralGalerkin_x(
        pde, x_sampler, mu_sampler, pinn, scheme="rk2", n_points=1200
    )

    ## solve the problem
    print(model.network.state_dict())
    model.compute_initial_data(projector_heatsol)
    print(model.network.state_dict())
    model.time_loop(dt=0.0008, T=0.1)

    model.plot(n_visu=10000, sol_exact=sol_ref)


if __name__ == "__main__":
    main()
