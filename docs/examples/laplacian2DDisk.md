# 2D Laplacian on Disk

We solve the PDE:

$$- \Delta u = \alpha$$

on $x \in Disc(0,1), \alpha\in [0.5,1.0]$.

We learn $u(t,x,\alpha)$.

**Parametric model:**

$$u_{net}(t,\alpha) = u_{\theta}(t,\alpha)$$

```{literalinclude} ../../examples/Pinns/elliptic/laplacian2D_disk_ritz.py
```
