import copy as cp

import pylab as plt
import scimba.nets.mlp as mlp
import scimba.odelearning.abstract_ode as abstractode
import scimba.odelearning.generation_ode_data as generation_ode_data
import torch
import torch.nn as nn


class SIRflux(nn.Module):
    def __init__(self, beta):
        super().__init__()

        self.beta = torch.nn.Parameter(torch.tensor(beta), requires_grad=True)
        self.nb_unknowns = 3
        self.nb_params = 1

    def forward(self, t, state, mu):
        gamma = self.get_parameters(mu)
        S, I, R = self.get_variables(state)

        dS = -self.beta * S * I
        dI = self.beta * S * I - gamma * I
        dR = gamma * I

        return self.set_derivatives(dS, dI, dR)


class SIRflux_beta_t(nn.Module):
    def __init__(self, beta, net):
        super().__init__()

        self.net = cp.deepcopy(net)
        self.beta = beta
        self.nb_unknowns = 3
        self.nb_params = 1

    def forward(self, t, state, mu):
        t = self.get_t(t)
        gamma = self.get_parameters(mu)
        S, I, R = self.get_variables(state)

        dS = -self.net(t) * S * I
        dI = self.net(t) * S * I - gamma * I
        dR = gamma * I

        return self.set_derivatives(dS, dI, dR)


class SIR_ode(abstractode.ClassicODE):
    def random_init_data(self):
        S = 0.9 + 0.1 * torch.rand(1)
        I = 1.0 - S
        R = 0.0
        return [S, I, R]

    def random_params(self):
        gamma = 0.3 + 0.01 * torch.rand(1)
        return [gamma]

    def plot_data(self, time, data):
        data = data.detach().numpy()
        fig, axs = plt.subplots(2, 2, figsize=(14, 10))
        axs[0, 0].plot(time, data[..., 0])
        axs[0, 0].set_title("S dynamic")
        axs[0, 1].plot(time, data[..., 1])
        axs[0, 1].set_title("I dynamic")
        axs[1, 0].plot(time, data[..., 2])
        axs[1, 0].set_title("R dynamic")
        plt.show()


def main():
    network = mlp.GenericMLP(1, 1, layer_sizes=[10, 20, 20, 10])
    flux1 = SIRflux(0.7)
    flux2 = SIRflux_beta_t(0.7, network)

    ode1 = SIR_ode(flux1)
    ode2 = SIR_ode(flux2)

    ts = torch.linspace(
        start=0.0,
        end=20.0,
        steps=50,  # number of time steps per time batch
    )

    # one batch of initial conditions
    initial_conditions = ode1.generate_initial_conditions(5)
    mu = ode1.generate_parameters(5)
    print("first ode")
    ode1.time_scheme(initial_conditions, ts, mu)
    print(ode1.solution.shape)
    ode1.plot_data(ts, ode1.solution)

    print("second ode")
    ode2.time_scheme(initial_conditions, ts, mu)
    ode2.plot_data(ts, ode2.solution)
    print(",jjj", ts.shape)
    print("llll", ode2.solution.shape)

    data_ode1 = generation_ode_data.DataGenerate(ode1, 20, 10, 15)
    data_ode1.create_dataset(100)
    data_ode1.print_data()


if __name__ == "__main__":
    main()
