# required: torchcubicspline
# pip install git+https://github.com/patrick-kidger/torchcubicspline.git

import time
from pathlib import Path

import numpy as np
import torch
from generate_data import DataGenerator
from scimba.equations import domain, pdes
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from torchcubicspline import NaturalCubicSpline, natural_cubic_spline_coeffs

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
torch.set_default_device(device)
torch.set_default_dtype(torch.float)


class SplineWithCall(NaturalCubicSpline):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __call__(self, x):
        return self.evaluate(x[:, 0])


def interpolate_data(gen, X, Y):
    assert X.shape[0] == 1, "interpolation only available for single data"

    f = torch.tensor(X[:, :, 0], dtype=torch.float).T * gen.normalisation_for_f
    alpha = torch.tensor(X[:, :, 1], dtype=torch.float).T
    U = torch.tensor(Y[:, :, 0], dtype=torch.float).T
    x = torch.tensor(gen.mesh.m, dtype=torch.float)

    spline_f = SplineWithCall(natural_cubic_spline_coeffs(x, f))
    spline_alpha = SplineWithCall(natural_cubic_spline_coeffs(x, alpha))
    spline_U = SplineWithCall(natural_cubic_spline_coeffs(x, U))

    return spline_f, spline_alpha, spline_U


class NonLinearElliptic(pdes.AbstractPDEx):
    r"""

    .. math::

        \frac{d^2u}{dx^2} + \frac{d^2u}{dy^2} + f = 0

    """

    def __init__(self, alpha0, k, f, Uref, p=4):
        super().__init__(
            nb_unknowns=1,
            space_domain=domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 1.0]])),
            nb_parameters=0,
            parameter_domain=[0.0, 0.0],
        )

        self.alpha0 = alpha0
        self.p = p

        self.k = k
        self.f = f
        self.Uref = Uref

        self.first_derivative = True
        self.second_derivative = True

        def anisotropy_matrix(w, x, mu):
            u = self.get_variables(w)
            x = x.get_coordinates()

            return self.k(x) * u**self.p

        self.anisotropy_matrix = anisotropy_matrix

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w) - 0.5

    def post_processing(self, x, mu, w):
        x = x.get_coordinates()
        return 0.5 + 4 * x**2 * (1 - x) ** 2 * w

    def residual(self, w, x, mu, **kwargs):
        x = x.get_coordinates()

        u = self.get_variables(w)
        dx_k_dx_u = self.get_variables(w, "div_K_grad_w")

        return u - self.alpha0 * dx_k_dx_u - self.f(x)

    def reference_solution(self, x, mu):
        return self.Uref(x.get_coordinates())


def define_and_train(alpha0, nx, seed=2, int_deep=False):
    torch.manual_seed(seed)
    np.random.seed(seed)

    gen = DataGenerator(nx, alpha0, seed=seed)
    X, Y = gen.make_XY(1)
    f, k, Uref = interpolate_data(gen, X, Y)

    pde = NonLinearElliptic(alpha0, k, f, Uref)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    if int_deep:
        file_name = "temp.pth"
    else:
        file_name = f"net_seed_{seed}_alpha_{alpha0}.pth"

    full_path = (
        Path.cwd()
        / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    )

    if int_deep:
        train = True
        full_path.unlink(missing_ok=True)
    else:
        train = not full_path.is_file()

    tlayers = [20, 40, 40, 40, 20]

    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="tanh")

    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(
        data_loss_bool=True, w_res=1e-6 / alpha0, w_data=10
    )

    optimizers = training_tools.OptimizerData(learning_rate=1e-2, decay=0.99)

    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    n_epochs = 400 if int_deep else 10_000

    if train:
        start = time.perf_counter()
        trainer.train(epochs=n_epochs, n_collocation=2_000, n_data=2_000)
        print(f"Training time: {time.perf_counter() - start:.2f} seconds")

    return gen, X, Y, network, trainer


if __name__ == "__main__":
    gen, X, Y, network, trainer = define_and_train(8, 200)
    trainer.plot(20000, reference_solution=True)
