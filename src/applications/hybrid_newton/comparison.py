# required: torchcubicspline, pandas
# pip install git+https://github.com/patrick-kidger/torchcubicspline.git


import contextlib
import itertools

import numpy as np
import torch
from generate_data import DataGenerator
from pinn import define_and_train
from scimba.equations.domain import SpaceTensor
from solve_Newton import MAXITER, solve_Newton, unpack_single_XY


def solve_naïve(Uref, G, line_search="armijo", coeff=1, random_type=None):
    assert random_type in [None, "random", "random_from_Uref", "random_added_to_Uref"]

    if random_type == "random":
        U0 = np.ones_like(Uref) + np.random.normal(0, 0.25, Uref.shape)
    elif random_type == "random_from_Uref":
        var = Uref.var()
        U0 = np.ones_like(Uref) + np.random.normal(0, var, Uref.shape)
    elif random_type == "random_added_to_Uref":
        U0 = Uref + np.random.normal(0, 0.25, Uref.shape)
    else:
        U0 = np.ones_like(Uref) * coeff

    return solve_Newton(G, U0, line_search=line_search, verbose=False, maxiter=MAXITER)


def solve_PINN(gen, trainer, Uref, G):
    x = torch.tensor(gen.mesh.m, dtype=torch.float)[:, None]

    mu = torch.empty((x.shape[0], 0))
    x = SpaceTensor(x)

    w_pred = trainer.network.setup_w_dict(x, mu)
    U0 = w_pred["w"].detach().cpu().T
    return solve_Newton(G, U0, verbose=False, maxiter=MAXITER)


def update_tables(Newton_result, Uref, i, alpha0, nx, table, dico=None):
    sol, n_iter, cpu_time = Newton_result
    table[i] = n_iter
    if dico is not None:
        dico[(alpha0, nx)] = (n_iter, cpu_time, sol, Uref)


def compare_to_pinns(seed=2, verbose=False):
    # set constants
    all_alpha0 = [2, 5, 8]
    all_nx = [100, 200, 400, 600]

    n_alpha0 = len(all_alpha0)
    n_nx = len(all_nx)

    classical_full = {}
    classical = np.zeros(n_alpha0 * n_nx)
    pinn_full = {}
    pinn = np.zeros(n_alpha0 * n_nx)
    int_deep_full = {}
    int_deep = np.zeros(n_alpha0 * n_nx)

    for i, (alpha0, nx) in enumerate(itertools.product(all_alpha0, all_nx)):
        if verbose:
            print(f"alpha0 = {alpha0}, nx = {nx}")

        # train networks
        with contextlib.redirect_stdout(None):
            gen, X, Y, _, trainer = define_and_train(alpha0, nx, seed=seed)
            _, _, _, _, trainer_int_deep = define_and_train(
                alpha0, nx, int_deep=True, seed=seed
            )

        # get Newton solution from data
        f, alpha, Uref, G = unpack_single_XY(gen, X, Y)

        # naïve initial guess
        res = solve_naïve(Uref, G)
        update_tables(res, Uref, i, alpha0, nx, classical, classical_full)

        # PINN initial guess
        res = solve_PINN(gen, trainer, Uref, G)
        update_tables(res, Uref, i, alpha0, nx, pinn, pinn_full)

        # Int-Deep initial guess
        res = solve_PINN(gen, trainer_int_deep, Uref, G)
        update_tables(res, Uref, i, alpha0, nx, int_deep, int_deep_full)

    # nicely package everything

    classical = classical.reshape((n_alpha0, n_nx)).T
    pinn = pinn.reshape((n_alpha0, n_nx)).T
    int_deep = int_deep.reshape((n_alpha0, n_nx)).T
    res = np.array([classical, pinn, int_deep])

    if verbose:
        print()
        print("classical initialization")
        print(classical)
        print()
        print("PINN initialization")
        print(pinn)
        print()
        print("Int-Deep initialization")
        print(int_deep)
        print()
        print("gains (PINN)")
        print(classical / pinn)
        print()
        print("gains (Int-Deep)")
        print(classical / int_deep)

    return res


def compare_initializations(seed=2, verbose=False):
    # set constants
    all_alpha0 = [2, 5, 8]
    all_nx = [100, 200, 400, 600]

    n_alpha0 = len(all_alpha0)
    n_nx = len(all_nx)
    n_tests = n_alpha0 * n_nx

    n_coeffs = 5
    coeffs = np.linspace(0.5, 1.5, n_coeffs)
    n_initial_conditions = n_coeffs + 3

    results = np.zeros((n_initial_conditions, n_tests))

    for i, (alpha0, nx) in enumerate(itertools.product(all_alpha0, all_nx)):
        if verbose:
            print(f"alpha0 = {alpha0}, nx = {nx}")

        # generate data
        gen = DataGenerator(nx, alpha0, seed=seed)
        X, Y = gen.make_XY(1)
        f, alpha, Uref, G = unpack_single_XY(gen, X, Y)

        for j, coeff in enumerate(coeffs):
            # solve with Newton
            res = solve_naïve(Uref, G, coeff=coeff)
            update_tables(res, Uref, i, alpha0, nx, results[j])

        # solve with Newton, no line search
        res = solve_naïve(Uref, G, random_type="random")
        update_tables(res, Uref, i, alpha0, nx, results[-3])

        # solve with Newton, random initialization
        res = solve_naïve(Uref, G, random_type="random_from_Uref")
        update_tables(res, Uref, i, alpha0, nx, results[-2])

        # solve with Newton, initialization from Uref
        res = solve_naïve(Uref, G, random_type="random_added_to_Uref")
        update_tables(res, Uref, i, alpha0, nx, results[-1])

    # nicely package everything
    res = []
    for j, result in enumerate(results):
        res.append(result.reshape((n_alpha0, n_nx)).T)

    if verbose:
        for j in range(n_initial_conditions):
            print()
            if j < n_coeffs:
                name = f"coeff = {coeffs[j]}"
            elif j == n_coeffs:
                name = "1 + random"
            elif j == n_coeffs + 1:
                name = "1 + random with good mean and variance"
            elif j == n_coeffs + 2:
                name = "Uref + random"
            print(name)
            print(res[j])

    return np.array(res)


if __name__ == "__main__":
    res_pinns = compare_to_pinns(verbose=True)
    print()
    res_init = compare_initializations(seed=2, verbose=True)
