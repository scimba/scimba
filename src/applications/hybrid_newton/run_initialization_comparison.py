import multiprocessing as mp

import numpy as np
from comparison import MAXITER, compare_initializations


def manipulate_results(results, n_simu):
    means = np.mean(results, axis=0)
    failure_rates = 100 * np.sum(results >= MAXITER, axis=0) / n_simu

    n_init = 8
    n_nx = 4
    n_alpha0 = 3

    res = np.empty((n_init, n_nx, 2 * n_alpha0))

    res[..., ::2] = means
    res[..., 1::2] = failure_rates

    res = res.transpose(1, 0, 2)

    return res


if __name__ == "__main__":
    n_simu = 40

    seeds = 42 + np.arange(n_simu)
    with mp.Pool(processes=n_simu) as pool:
        results = pool.map(compare_initializations, seeds)

    results = np.array(results)

    np.save("initialization_comparison.npy", results)

    res = manipulate_results(results, n_simu)
    print(res)


# results below

""" tables for each N

$0.5$ & 2000 & 100 & 2000 & 100 & 2000 & 100 \\
$0.75$ & 1686 & 72.5 & 1670 & 72.5 & 1804 & 80 \\
$1$ & 280  & 0   & 1293 & 30  & 1824 & 75 \\
$1.25$ & 1435 & 42.5 & 1993 & 97.5 & 2000 & 100 \\
$1.5$ & 1960 & 95  & 2000 & 100 & 2000 & 100 \\
$1 + \mathcal{N}_{0, 0.25}$ & 362  & 5   & 1244 & 27.5 & 1791 & 70 \\
$1 + \mathcal{N}_{0, \sigma_\text{exact}}$ & 285  & 0   & 1277 & 32.5 & 1801 & 75 \\
$U_\text{exact} + \mathcal{N}_{0, 0.25}$ & 285  & 10  & 338  & 7.5  & 580  & 20

$0.5$ & 2000 & 100  & 2000 & 100  & 2000 & 100 \\
$0.75$ & 1923 & 95   & 1902 & 92.5 & 1951 & 95 \\
$1$ & 351  & 0    & 781  & 5    & 1288 & 32.5 \\
$1.25$ & 1184 & 25   & 1943 & 95   & 1990 & 97.5 \\
$1.5$ & 1955 & 95   & 2000 & 100  & 2000 & 100 \\
$1 + \mathcal{N}_{0, 0.25}$ & 394  & 0    & 889  & 2.5  & 1453 & 40 \\
$1 + \mathcal{N}_{0, \sigma_\text{exact}}$ & 356  & 0    & 791  & 0    & 1384 & 32.5 \\
$U_\text{exact} + \mathcal{N}_{0, 0.25}$ & 312  & 5    & 304  & 0    & 525  & 7.5

$0.5$ & 2000 & 100 & 2000 & 100 & 2000 & 100 \\
$0.75$ & 2000 & 100 & 2000 & 100 & 2000 & 100 \\
$1$ & 1157 & 5   & 1888.375 & 70  & 2000 & 100 \\
$1.25$ & 1246   & 2.5   & 1941 & 85  & 2000 & 100 \\
$1.5$ & 1483  & 27.5  & 1974   & 92.5  & 2000 & 100 \\
$1 + \mathcal{N}_{0, 0.25}$ & 1307 & 15  & 1959  & 77.5  & 1997 & 97.5  \\
$1 + \mathcal{N}_{0, \sigma_\text{exact}}$ & 1181 & 2.5   & 1931 & 80  & 2000 & 100 \\
$U_\text{exact} + \mathcal{N}_{0, 0.25}$ & 642   & 2.5   & 1033 & 10  & 1249 & 7.5 \\

$0.5$ & 2000 & 100  & 2000 & 100  & 2000 & 100 \\
$0.75$ & 1987 & 97.5 & 2000 & 100  & 2000 & 100 \\
$1$ & 1736 & 45   & 2000 & 100  & 2000 & 100 \\
$1.25$ & 1934 & 87.5 & 2000 & 100  & 2000 & 100 \\
$1.5$ & 1982 & 87.5 & 2000 & 100  & 2000 & 100 \\
$1 + \mathcal{N}_{0, 0.25}$ & 1724 & 42.5 & 2000 & 100  & 2000 & 100 \\
$1 + \mathcal{N}_{0, \sigma_\text{exact}}$ & 1684 & 35   & 2000 & 100  & 2000 & 100 \\
$U_\text{exact} + \mathcal{N}_{0, 0.25}$ & 1018 & 2.5  & 1463 & 5    & 1739 & 25 \\
"""
