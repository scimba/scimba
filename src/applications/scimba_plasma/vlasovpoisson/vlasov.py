import numpy as np
from tqdm import tqdm

from . import bsl as bs


class VlasovPoisson:
    """
    Solve the 2D Vlasov-Poisson problem in (x,v).
    ATTENTION: x must be periodic
    """

    def __init__(self, xmin, xmax, nx, vmin, vmax, nv):
        # Grid
        self.nx = nx
        self.x, self.dx = np.linspace(xmin, xmax, nx, endpoint=False, retstep=True)
        self.nv = nv
        self.v, self.dv = np.linspace(vmin, vmax, nv, endpoint=False, retstep=True)

        # Distribution function
        self.f = np.zeros((nx, nv))

        # Interpolators for advection
        self.cs_x = bs.BSpline(3, xmin, xmax, nx)
        self.cs_v = bs.BSpline(3, vmin, vmax, nv)

        # Modes for Poisson equation
        self.modes = np.zeros(nx)
        k = 2 * np.pi / (xmax - xmin)
        self.modes = k * nx * bs.fftfreq(nx)
        self.modes += self.modes == 0  # avoid division by zero

    def advection_x(self, dt):
        for j in range(self.nv):
            alpha = dt * self.v[j]
            self.f[:, j] = self.cs_x.interpolate_disp(self.f[:, j], alpha)

    def advection_v(self, efield, dt):
        for i in range(self.nx):
            alpha = dt * efield[i]
            self.f[i, :] = self.cs_v.interpolate_disp(self.f[i, :], alpha)

    def compute_rho(self):
        return self.dv * np.sum(self.f, axis=1)

    def compute_efield(self, rho):
        # compute Ex using that ik*Ex = rho
        rhok = bs.fft(rho) / self.modes
        return np.real(bs.ifft(-1j * rhok))

    def run(self, f, nstep, dt, F, efield):
        self.f = f
        nrj = []
        self.advection_x(0.5 * dt)
        for istep in tqdm(range(nstep)):
            rho = self.compute_rho()
            efield[istep] = self.compute_efield(rho)
            self.advection_v(efield[istep], dt)
            self.advection_x(dt)
            nrj.append(0.5 * np.log(np.sum(efield[istep] * efield[istep]) * self.dx))
            F[istep, :] = self.f
        return nrj, F, efield

    def test_run(self, f, nstep, dt):
        self.f = f
        nrj = []
        self.advection_x(0.5 * dt)
        for istep in tqdm(range(nstep)):
            rho = self.compute_rho()
            efield = self.compute_efield(rho)
            self.advection_v(efield, dt)
            self.advection_x(dt)
            nrj.append(0.5 * np.log(np.sum(efield * efield) * self.dx))

        return nrj
