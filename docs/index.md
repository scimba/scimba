# Welcome to Scimba documentation!

[![pipeline status](https://gitlab.inria.fr/scimba/scimba/badges/main/pipeline.svg)](https://gitlab.inria.fr/scimba/scimba/-/commits/main)
[![coverage report](https://gitlab.inria.fr/scimba/scimba/badges/main/coverage.svg)](https://sciml.gitlabpages.inria.fr/scimba/coverage)
[![Latest Release](https://gitlab.inria.fr/scimba/scimba/-/badges/release.svg)](https://gitlab.inria.fr/scimba/scimba/-/releases)
[![Doc](https://img.shields.io/badge/doc-sphinx-blue)](hhttps://scimba.gitlabpages.inria.fr/scimba/)


This project gathers some tools for scientific machine learning, some
examples on classical PDE. It is meant to be used for research projects
in SciML.

```{toctree}
:maxdepth: 2
installation.md
```

## Available Packages

### General neural networks

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: General neural networks
   :recursive:

   scimba.nets
   scimba.generativenets
   scimba.largenets
```

### Physics-informed methods

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: Physics-informed methods
   :recursive:

    scimba.equations
    scimba.sampling
    scimba.pinns
    scimba.neuralgalerkin
    scimba.neural_operators
    scimba.pinos
```


### Differentiable physics approaches

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: Differentiable physics approaches
   :recursive:

    scimba.odelearning
    scimba.numericalmethods
```

## Examples

```{toctree}
:caption: Representative examples for PINNs

examples/simple_ode
examples/pendulum
examples/nonlinear_pendulum
examples/laplacian1D
examples/laplacian2DDisk
```

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`

