# Timer.py

import time

class TimerError(Exception):
    """A custom exception used to report errors in use of Timer class"""

class Timer:
    def __init__(self,TimerName=None,AutomaticPrint=True):
        self._start_time = None
        self._elapsed_time = None
        self._name = TimerName
        self._automatic_print = AutomaticPrint

    def start(self):
        """Start a new timer"""
        if self._start_time is not None:
            raise TimerError(f"Timer is running. Use .stop() to stop it")

        self._start_time = time.perf_counter()

    def stop(self):
        """Stop the timer, and report the elapsed time (if AutomaticPrint=True)"""
        if self._start_time is None:
            raise TimerError(f"Timer is not running. Use .start() to start it")

        self._elapsed_time = time.perf_counter() - self._start_time
        self._start_time = None
        if self._automatic_print:
            if self._name is not None:
                print(f"Elapsed time for {self._name}: {self._elapsed_time:0.4f} seconds")
            else:
                print(f"Elapsed time: {self._elapsed_time:0.4f} seconds")

    def print(self):
        """Print the elapsed time (required if AutomaticPrint=False)"""
        if self._name is not None:
            print(f"Elapsed time for {self._name}: {self._elapsed_time:0.4f} seconds")
        else:
            print(f"Elapsed time: {self._elapsed_time:0.4f} seconds")
