from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self):
        super().__init__(
            nb_unknowns=1,
            space_domain=domain.SpaceDomain(
                2, domain.SquareDomain(2, [[-PI / 2, PI / 2], [-PI / 2, PI / 2]])
            ),
            nb_parameters=2,
            parameter_domain=[[-0.5, 0.5], [-0.5, 0.5]],
        )

        self.first_derivative = True
        self.second_derivative = True

        self.derivatives_wrt_mu = True
        self.coeff_derivative_mu = 1

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)

        sin_1 = torch.sin(2 * x1)
        sin_2 = torch.sin(2 * x2)
        cos_1 = torch.cos(2 * x1)
        cos_2 = torch.cos(2 * x2)

        # classical PDE residual

        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = -torch.exp(-((x1 - mu1) ** 2 + (x2 - mu2) ** 2) / 2) * (
            ((x1**2 - 2 * mu1 * x1 + mu1**2 - 5) * sin_1 + (4 * mu1 - 4 * x1) * cos_1)
            * sin_2
            + sin_1
            * ((x2**2 - 2 * mu2 * x2 + mu2**2 - 5) * sin_2 + (4 * mu2 - 4 * x2) * cos_2)
        )
        residual = u_xx + u_yy + f

        # derivative of residual with respect to mu1

        u_xx_mu1 = self.get_variables(w, "w_xx_mu1")
        u_yy_mu1 = self.get_variables(w, "w_yy_mu1")

        df_dmu1 = -torch.exp(-((x1 - mu1) ** 2 + (x2 - mu2) ** 2) / 2) * (
            (
                x1 * x2**2
                - mu1 * x2**2
                - 2 * mu2 * x1 * x2
                + 2 * mu1 * mu2 * x2
                + x1**3
                - 3 * mu1 * x1**2
                + mu2**2 * x1
                + 3 * mu1**2 * x1
                - 12 * x1
                - mu1 * mu2**2
                - mu1**3
                + 12 * mu1
            )
            * sin_1
            * sin_2
            - 4 * (x1**2 - 2 * mu1 * x1 + mu1**2 - 1) * cos_1 * sin_2
            - 4 * (x1 * x2 - mu1 * x2 - mu2 * x1 + mu1 * mu2) * sin_1 * cos_2
        )

        residual_mu1 = u_xx_mu1 + u_yy_mu1 + df_dmu1

        # derivative of residual with respect to mu2

        u_xx_mu2 = self.get_variables(w, "w_xx_mu2")
        u_yy_mu2 = self.get_variables(w, "w_yy_mu2")

        df_dmu2 = -torch.exp(-((x1 - mu1) ** 2 + (x2 - mu2) ** 2) / 2) * (
            (
                x2**3
                - 3 * mu2 * x2**2
                + x1**2 * x2
                - 2 * mu1 * x1 * x2
                + 3 * mu2**2 * x2
                + mu1**2 * x2
                - 12 * x2
                - mu2 * x1**2
                + 2 * mu1 * mu2 * x1
                - mu2**3
                - mu1**2 * mu2
                + 12 * mu2
            )
            * sin_1
            * sin_2
            - 4 * (x1 * x2 - mu1 * x2 - mu2 * x1 + mu1 * mu2) * cos_1 * sin_2
            - 4 * (x2**2 - 2 * mu2 * x2 + mu2**2 - 1) * sin_1 * cos_2
        )

        residual_mu2 = u_xx_mu2 + u_yy_mu2 + df_dmu2

        return torch.sqrt(
            residual**2
            + self.coeff_derivative_mu**2 * (residual_mu1**2 + residual_mu2**2)
        )

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return (0.5 * PI - x1) * (x1 + 0.5 * PI) * (x2 + 0.5 * PI) * (0.5 * PI - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        g = torch.exp(-((x1 - mu1) ** 2.0 + (x2 - mu2) ** 2.0) / 2)
        return g * torch.sin(2 * x1) * torch.sin(2 * x2)


def main(pde, epochs=2000):
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 60, 60, 40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData()
    optimizers = training_tools.OptimizerData(learning_rate=1.2e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if new_training:
        trainer.train(epochs=epochs, n_collocation=10000, n_data=0)

    trainer.plot(20000, reference_solution=True)

    return network, trainer


if __name__ == "__main__":
    # Laplacien strong Bc on Square with nn
    pde = Poisson_2D()

    network, trainer = main(pde, epochs=500)
    trainer.plot_2d_contourf(draw_contours=True)
