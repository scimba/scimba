from pathlib import Path

import scimba.equations.domain as domain
import scimba.equations.pdes as pdes
import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_tx as pinn_tx
import scimba.pinns.training_tx as training_tx
import scimba.sampling.sampling_ode as sampling_ode
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


# +
class LLG_eq(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 1000],
        xdomain=domain.SpaceDomain(1, domain.SquareDomain(1, [[-80.0, 80.0]])),
        p_domain=[[0.1, 0.100001], [-0.2, -0.199999]],
    ):
        super().__init__(
            nb_unknowns=3,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=2,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha, H = self.get_parameters(mu)
        m1, m2, m3 = self.get_variables(w, "w")
        m1_t, m2_t, m3_t = self.get_variables(w, "w_t")
        m1_xx, m2_xx, m3_xx = self.get_variables(w, "w_xx")

        mdm_1 = m2 * m3_xx - m3 * m2_xx
        mdm_2 = m3 * m1_xx - m1 * m3_xx
        mdm_3 = m1 * m2_xx - m2 * m1_xx

        eq_1 = (
            m1_t
            - mdm_1
            + alpha
            * (
                m2 * mdm_3
                - m3 * mdm_2
                - m2**2.0 * m1
                - m3**2.0 * m1
                - m2**2.0 * H
                - m3**2.0 * H
            )
        )
        eq_2 = (
            m2_t
            - mdm_2
            - m3 * m1
            - m3 * H
            + alpha * (m3 * mdm_1 - m1 * mdm_3 + m1**2.0 * m2 + m1 * m2 * H)
        )
        eq_3 = (
            m3_t
            - mdm_3
            + m2 * m1
            + m2 * H
            + alpha * (m1 * mdm_2 - m2 * mdm_1 + m3 * m1**2.0 + m1 * m3 * H)
        )

        return eq_1, eq_2, eq_3

    def post_processing(self, t, x, mu, w):
        m1, m2, m3 = self.prepare_variables(w)
        norm = torch.sqrt(m1 * m1 + m2 * m2 + m3 * m3)
        return torch.cat((m1 / norm, m2 / norm, m3 / norm), axis=1)

    def bc_residual(self, w, t, x, mu, **kwargs):
        m1_x, m2_x, m3_x = self.get_variables(w, "w_x")
        return m1_x, m2_x, m3_x

    def initial_condition(self, x, mu, **kwargs):
        x = x.get_coordinates()
        return torch.cat(
            (
                torch.cos(2.0 * torch.arctan(torch.exp(-x - 80.0 / 3.0))),
                torch.sin(2.0 * torch.arctan(torch.exp(-x - 80.0 / 3.0))),
                torch.zeros_like(x),
            ),
            axis=1,
        )

    def make_data(self, n_data):
        pass

    def reference_solution(self, t, x, mu):
        x = x.get_coordinates()
        alpha, H = self.get_parameters(mu)
        return torch.cat(
            (
                torch.cos(
                    2.0 * torch.arctan(torch.exp(-x + alpha * H * t - 80.0 / 3.0))
                ),
                torch.sin(
                    2.0 * torch.arctan(torch.exp(-x + alpha * H * t - 80.0 / 3.0))
                )
                * torch.cos(-H * t),
                torch.sin(
                    2.0 * torch.arctan(torch.exp(-x + alpha * H * t - 80.0 / 3.0))
                )
                * torch.sin(-H * t),
            ),
            axis=1,
        )


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[-80.0, 80.0]]))
    pde = LLG_eq(tdomain=[0.0, 10.0], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test_llg.pth"
    new_training = True
    # new_training = False

    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    # tlayers = [120, 120, 120, 120, 120]
    tlayers = [3, 2]

    network = pinn_tx.MLP_tx(pde=pde, layer_sizes=tlayers, activation_type="silu")
    pinn = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=True, init_loss_bool=True, w_res=0.04, w_bc=0.1, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(learning_rate=2.5e-2, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=3000,
    )

    if train:
        trainer.train(
            epochs=1000,
            n_collocation=3000,
            n_bc_collocation=2000,
            n_init_collocation=2000,
            n_data=0,
        )

    trainer.plot(20000, reference_solution=True)


if __name__ == "__main__":
    main()
