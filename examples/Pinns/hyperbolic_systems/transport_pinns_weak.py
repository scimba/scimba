from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_tx, pinn_tx
from scimba.sampling import (
    sampling_parameters,
    sampling_pde,
    uniform_sampling,
    sampling_ode,
)
from scimba.equations import domain, pdes


class Transport_equation(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 0.3],
        xdomain=domain.SpaceDomain(1, domain.SquareDomain(1, [[0, 2]])),
        p_domain=[[0.4, 0.7]],
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t =True
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.cross_derivative = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u_x = self.get_variables(w, "w_x")
        u_t = self.get_variables(w, "w_t")
        u_tt = self.get_variables(w, "w_tt")
        u_xx = self.get_variables(w, "w_xx")
        u_tx = self.get_variables(w, "w_tx")
        return u_t + alpha * u_x, 0.05*(u_tt+alpha*u_tx), 0.05*(u_tx+alpha*u_xx)

    def bc_residual(self, w, t, x, mu, **kwargs):
        uL = self.get_variables(w,"w",label=0)
        uR = self.get_variables(w,"w",label=1)
        return uR-uL

    def initial_condition(self, x, mu, **kwargs):
        x = x.get_coordinates()
        t0 = 0.02
        return (
            (1.0 / (4.0 * PI * t0) ** 0.5)
            * torch.exp(-((x - 1) ** 2) / (4.0 * t0))
            * (torch.sin(12.0 * PI * x) + 1.0)
        )

    def make_data(self, n_data):
        pass

    def reference_solution(self, t, x, mu):
        x = x.get_coordinates()
        t0 = 0.02
        alpha = self.get_parameters(mu)
        return (
            (1 / ((4.0 * PI * t0) ** 0.5))
            * torch.exp(-((x - 1 - alpha * t) ** 2.0) / (4 * t0))
            * (torch.sin(12.0 * PI * (x - alpha * t)) + 1.0)
        )


def main():
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = Transport_equation(tdomain=[0.0, 0.3], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    file_name = "test.pth"
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_tx.TrainerPINNSpaceTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [80, 80, 80, 80]
    # network = pinn_tx.MLP_tx(
    #    pde=pde, layer_sizes=tlayers, activation_type="sine"
    # )
    network = pinn_tx.Fourier_tx(
        pde=pde,
        layer_sizes=tlayers,
        activation_type="sine",
        nb_features=12,
        list_type_features={"x": True, "t": False, "tx": False, "mu": False, "txmu":False},
        std_features={"x":5.0, "t": 0.0, "tx": 0.0, "mu": 0.0, "txmu":0.0}
    )
    pinns = pinn_tx.PINNtx(network, pde)

    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=True, init_loss_bool=True, w_res=0.04, w_bc=0.1, w_init=1.0
    )
    optimizers = training_tools.OptimizerData(learning_rate=9e-3, decay=0.99)
    trainer = training_tx.TrainerPINNSpaceTime(
        pde=pde,
        network=pinns,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=3000,
    )
    if new_training:
        trainer.train(
            epochs=2000,
            n_collocation=6000,
            n_bc_collocation=1000,
            n_init_collocation=3000,
            n_data=0,
        )
    trainer.plot(reference_solution=True)


if __name__ == "__main__":
    main()
