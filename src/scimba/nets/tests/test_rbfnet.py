import torch
from torch import nn

from scimba.nets.rbfnet import RBFScalarLayer,RBFLayer

def test_rbfscalar():
    mu= torch.tensor([[1.0,2.0],[0.0,0.0],[1.0,-1.0]])
    x= torch.tensor([[1.4,2.1],[-0.1,0.3],[-3.0,-1.0],[-0.1,1.3],[1.4,0.7]])
    
    layer = RBFScalarLayer(
        in_size=2,
        points=mu,
        nb_func=3,
        type_g="isotropic",
        type_rbf="gausssian")
    c=layer.forward(x)

    layer = RBFScalarLayer(
        in_size=2,
        points=mu,
        nb_func=3,
        type_g="anistropic",
        type_rbf="gausssian")
    c2=layer.forward(x)
    assert((c.shape[0]==5 and c.shape[1]==1))
    assert((c2.shape[0]==5 and c2.shape[1]==1))

def test_rbflayer():
    mu= torch.tensor([[1.0,2.0],[0.0,0.0],[1.0,-1.0]])
    x= torch.tensor([[1.4,2.1],[-0.1,0.3],[-3.0,-1.0],[-0.1,1.3],[1.4,0.7]])
    
    layer = RBFLayer(
        in_size=2,
        out_size=2,
        points=mu,
        nb_func=3,
        type_g="isotropic",
        type_rbf="gausssian")
    c=layer.forward(x)
    assert((c.shape[0]==5 and c.shape[1]==2))

if __name__ == "__main__":
    test_rbfscalar()
    test_rbflayer()