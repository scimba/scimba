REMINDER: How to install correctly the SCIMBA library
```
export SCIMBAPATH=$HOME/scimba
git clone git@gitlab.inria.fr:sciml/scimba.git $SCIMBAPATH
export PYTHONPATH=$SCIMBAPATH:$PYTHONPATH
cd $SCIMBAPATH
pip install -e .
```
To test if the installation is correct, run the test (corresponding to all scripts starting with 'test_' prefix
```
pytest
#Remark: `pytest <nameOfTestScript.py>` to test a specific test.
```

The scripts dedicated to VlasovPoisson are `applications/scimba_plasma/vlasovpoisson/` folder.
```
cd $SCIMBAPATH/applications/scimba_plasma/vlasovpoisson
```
For more details, look at `README_VlasovPoisson.md` in this folder. 

# How to run the code on Adastra computer
1. Install pytorch for GPU in your Python environment with the command
`pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/rocm5.6`
2. Run the script `applications/scimba_plasma/vlasovpoisson/scimba_GPU.sh` as
```
cd applications/scimba_plasma/vlasovpoisson
sbatch scimba_batch_GPU.sh
```
