from pathlib import Path

import matplotlib.pyplot as plt
import scimba.nets.mlp as mlp
import torch
import torch.distributions as D
from scimba.generativenets import gaussian_mixtures, trainer_likelihood, generativelosses
from scimba.nets import training_tools


class TimeGaussian:
    def __init__(self, mu_0, sig2_0):
        self.mu_0 = mu_0
        self.sig2_0 = sig2_0

    def sample(self, t, n):
        comp = D.Normal(
            torch.tensor([self.mu_0]), torch.tensor([self.sig2_0 * 2 * (1.0 + t)])
        )
        return comp.sample((n,))

    def density(self, x, t):
        comp = D.Normal(
            torch.tensor([self.mu_0]), torch.tensor([self.sig2_0 * 2 * (1.0 + t)])
        )
        return torch.exp(comp.log_prob(x))


def main():
    data = TimeGaussian(0.5, 0.1)

    x = torch.linspace(-3, 3, 500)
    plt.figure(figsize=(4, 4))
    plt.plot(x.detach().cpu(), data.density(x, 0.0).detach().cpu(), "b")
    plt.plot(x.detach().cpu(), data.density(x, 0.3).detach().cpu(), "r")
    plt.plot(x.detach().cpu(), data.density(x, 0.8).detach().cpu(), "g")
    plt.show()

    nt = 120
    nx = 250
    tab_t = torch.linspace(0.01, 1.0, nt)
    x = data.sample(0.0, nx)
    t_data = torch.zeros(nx)
    for t in tab_t:
        x0 = data.sample(t, nx)
        x = torch.cat([x, x0], axis=0)
        t_loc = torch.ones(nx) * t
        t_data = torch.cat([t_data, t_loc], axis=0)
    t_data = t_data[:, None]
    print(t_data.size())
    print(x.size())

    out_size = 1
    cond_size = 1
    mixture = gaussian_mixtures.ConditionalGaussianMixtures(
        net=mlp.GenericMLP, dim=out_size, dim_conditional=cond_size, nb_gaussians=1
    )

    yy = torch.linspace(-3, 3, 500)
    plt.figure(figsize=(4, 4))
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.0).detach().cpu(),
        "b",
    )
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.3).detach().cpu(),
        "r",
    )
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.8).detach().cpu(),
        "g",
    )
    plt.show()

    file_name = "cond_mixture_gaussian.pth"
    (
        Path.cwd()
        / Path(trainer_likelihood.TrainerLikelihood.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    ).unlink(missing_ok=True)

    losses = generativelosses.GenerativeLossesData()
    optimizers = training_tools.OptimizerData(learning_rate=8.3e-4, decay=0.99)
    Trainer = trainer_likelihood.TrainerLikelihood(
        out_size=1,
        output_data=x,
        conditional=True,
        file_name=file_name,
        cond_size=1,
        cond_data=t_data,
        network=mixture,
        losses=losses,
        optimizers = optimizers,
        batch_size=1000,
    )
    Trainer.train(epochs=200)

    yy = torch.linspace(-3, 3, 500)
    plt.figure(figsize=(4, 4))
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.0).detach().cpu(),
        "b",
    )
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.3).detach().cpu(),
        "r",
    )
    plt.plot(
        yy.detach().cpu(),
        mixture.density(yy[:, None], torch.ones(500, 1) * 0.8).detach().cpu(),
        "g",
    )
    plt.show()


if __name__ == "__main__":
    main()
