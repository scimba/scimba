from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class SinMultiScale1D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[2.0, 2.0 + 1e-4], [20.0, 20.0 + 1e-4]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def reference_solution(self, x, mu):
        x1 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        return torch.sin(mu1 * PI * x1) + 0.2 * torch.sin(mu2 * PI * x1)

    def post_processing(self, x, mu, w):
        x1 = x.get_coordinates()
        return x1 * (1.0 - x1) * w

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")

        f = PI**2 * mu1 * mu1 * torch.sin(
            mu1 * PI * x1
        ) + 0.04 * PI**2 * mu2 * mu2 * torch.sin(mu2 * PI * x1)
        return u_xx + f


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.50000, 0.500001]],
        )

        self.first_derivative = True
        self.second_derivative = True

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = 128 * PI**2 * alpha * torch.sin(8 * PI * x1) * torch.sin(8 * PI * x2)
        return u_xx + u_yy + f

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(8 * PI * x1) * torch.sin(8 * PI * x2)


def train_network(network, file_path, pde, epochs, sampler, new_training=True, dim=2):
    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_path
        ).unlink(missing_ok=True)

    pinns = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(learning_rate=1.2e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinns,
        losses=losses,
        optimizers=optimizers,
        sampler=sampler,
        file_name=file_path,
        batch_size=4000,
    )

    if new_training:
        trainer.train(epochs=epochs, n_collocation=4000)

    if dim==2:
        trainer.plot_2d_contourf(
            n_visu=768,
            n_contour=250,
            random=False,
            colormap=None,
            draw_contours=True,
            n_drawn_contours=12,
            error=False,
            residual=False,
        )
    else:
        trainer.plot(reference_solution=True)
        trainer.plot_2d_contourf(draw_contours=True, error=True)


def main():
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = Poisson_2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name1 = "testnn.pth"
    file_name2 = "testff.pth"
    file_name3 = "testmff.pth"
    file_name4 = "testsiren.pth"
    file_name5 = "testmff1d.pth"
    file_name6 = "testnnhat.pth"

    # classical MLP

    tlayers = [40, 40, 40]
    network0 = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    train_network(network0, file_name1, pde, 2000, sampler)

    # MLP with Fourier features

    tlayers = [40, 40, 40]
    network2 = pinn_x.Fourier_x(pde=pde, 
                                mean_features={"x":0.0,"mu":0.0,"xmu":0.0},
                                std_features={"x":6.0,"mu":0.0,"xmu":0.0},
                                nb_features=20, 
                                layer_sizes=tlayers)
    train_network(network2, file_name2, pde, 2000, sampler)

    # multiscale MLP with Fourier features

    tlayers = [40, 40, 40]
    network3 = pinn_x.MultiScale_Fourier_x(pde=pde,
                                           means=[0.0,0.0], 
                                           stds=[1.5, 10.0], 
                                           nb_features=20, 
                                           layer_sizes=tlayers)
    train_network(network3, file_name3, pde, 2000, sampler)

    # SIREN

    tlayers = [40, 40, 40]
    network4 = pinn_x.Siren_x(pde=pde, w=1.0, layer_sizes=tlayers)
    train_network(network4, file_name4, pde, 2000, sampler)

    # multiscale MLP with Fourier features, 1D example

    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 1.0]]))
    pde = SinMultiScale1D(space_domain=xdomain)

    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    fourier = pinn_x.MultiScale_Fourier_x(
        pde=pde, 
        means=[0.0,0.0],
        stds=[1.0, 60.0], 
        nb_features=20,
        layer_sizes=[40, 40, 40, 40]
    )

    train_network(fourier, file_name5, pde, 4000, sampler,dim=1)

    # classical MLP with regularized hat activations

    tlayers = [40, 40, 40]
    network0 = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="regularized_hat")
    train_network(network0, file_name6, pde, 2000, sampler)


if __name__ == "__main__":
    main()
