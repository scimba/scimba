import time

import numpy as np
import scipy.optimize as sco
from generate_data import DataGenerator

MAXITER = 2000


def solve_Newton(G, U0, line_search="armijo", verbose=True, maxiter=MAXITER):
    options = {
        "maxiter": maxiter,
        "disp": verbose,
        "tol_norm": lambda x: np.linalg.norm(x),
        "line_search": line_search,
        "ftol": 1e-6,
        "fatol": 1e-6,
        "jac_options": {"inner_maxiter": 1000},
    }

    try:
        start_time = time.time()
        sol_ref = sco.root(G, U0, method="krylov", options=options)
        elapsed = time.time() - start_time

        if sol_ref.success:
            niter = sol_ref.nit
        else:
            niter = maxiter
        return sol_ref.x, niter, elapsed

    except ValueError:
        return U0, maxiter, 0


def unpack_single_XY(gen, X, Y, i=0):
    f = X[None, i, :, 0]
    alpha = X[None, i, :, 1]
    Uref = Y[None, i, :, 0]
    G = gen.get_G_for_scipy(f, alpha)
    return f, alpha, Uref, G


def print_results(n_iter, cpu_time, sol, Uref):
    string = f"n_iter: {n_iter}, cpu_time: {cpu_time:.2f}"
    string += f", error: {np.linalg.norm(sol - Uref):.2e}"
    print(string)


def generate_and_solve(N, alpha0, n_xy, verbose=False):
    gen = DataGenerator(N, alpha0)
    X, Y = gen.make_XY(n_xy)

    for i in range(n_xy):
        f, alpha, Uref, G = unpack_single_XY(gen, X, Y, i)

        U0 = np.ones_like(Uref)
        sol, n_iter, cpu_time = solve_Newton(G, U0, verbose=verbose)
        print_results(n_iter, cpu_time, sol, Uref)


if __name__ == "__main__":
    for nx in [100, 200, 400]:
        for alpha in [2, 5, 8]:
            print(f"nx = {nx}, alpha = {alpha}")
            generate_and_solve(nx, alpha, 5, verbose=False)
