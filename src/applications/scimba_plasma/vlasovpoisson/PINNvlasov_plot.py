import matplotlib.pyplot as plt
import numpy as np
import torch
from scimba.equations.domain import SpaceTensor


def plot_loss_PINNvlasov(trainer, savedir):
    fontsizelegend = 15

    # Plot loss history
    fig0, ax0 = plt.subplots(1, 1, figsize=(10, 5))
    ax0 = trainer.losses.plot(ax0)
    ax0.set_xlim(0, len(trainer.losses.loss_history) - 1)
    ax0.legend(loc="upper right", fontsize=fontsizelegend)

    figname = f"{savedir}/fig_loss_epochs{trainer.epoch_tot}.png"
    print(f"==> Save {figname}")
    plt.savefig(figname)

    # Plot weight history
    if trainer.losses.adaptive_weights is not None:
        fig1, ax1 = plt.subplots(1, 1, figsize=(10, 5))
        ax1.plot(trainer.losses.w_data_history, label="data")
        ax1.plot(trainer.losses.w_init_history, label="init")
        ax1.plot(trainer.losses.w_bc_history, label="bc")
        ax1.set_xlim(0, len(trainer.losses.w_data_history) - 1)
        ax1.legend(loc="upper right", fontsize=fontsizelegend)

        figname = f"{savedir}/fig_loss_weights_epochs{trainer.epoch_tot}.png"
        print(f"==> Save {figname}")
        plt.savefig(figname)


def plot_fdistribu_PINNvlasov(trainer, savedir, deltaf=False):
    if not deltaf:
        ideltaf = 0.0
        value_name = "f"
    else:
        ideltaf = 1.0
        value_name = "deltaf"

    fontsizetitle = 15
    fontsizelegend = 12
    fontsizelabel = 15
    fontsizeticks = 15

    i = 0
    j = 0
    t_simu = trainer.sampler.data_t[:, None]
    indx_time_series = [0, int(len(t_simu) / 2), len(t_simu) - 1]
    fig1, ax = plt.subplots(3, 3, figsize=(15, 10))

    for indx_time in indx_time_series:
        t_simu = trainer.sampler.data_t  # [:, None]
        x_simu = trainer.sampler.data_x  # [:, None]
        v_simu = trainer.sampler.data_v  # [:, None]
        f_simu = trainer.sampler.data_f.cpu()
        feq_simu = trainer.sampler.data_feq.cpu()
        tplot = t_simu[indx_time]
        t = tplot * torch.ones_like(x_simu.x)
        indx_v = int(trainer.sampler.v_len / 2) + 1
        vplot = v_simu[indx_v]
        v = vplot * torch.ones_like(x_simu.x)
        mu = torch.zeros((x_simu.shape[0], 0))

        u_pred_x = trainer.network.get_w(t, x_simu, v, mu)
        feq_simu_vfixed = feq_simu[:, 0, indx_v]

        ax[i, j].plot(
            x_simu.x.detach().cpu(),
            f_simu[indx_time, 0, :, indx_v] - ideltaf * feq_simu_vfixed,
            label="simulation",
            c="k",
        )
        ax[i, j].plot(
            x_simu.x.detach().cpu(),
            u_pred_x.detach().cpu() - ideltaf * feq_simu_vfixed[:, None],
            label="prediction",
            c="r",
        )
        ax[i, j].set_title(
            f"{value_name} at t= {str(np.round(tplot.cpu().numpy()[0],2))} and v= {str(np.round(vplot.cpu().numpy()[0],2))}",
            fontsize=fontsizetitle,
        )
        ax[i, j].set_xlabel(r"$x$", fontsize=fontsizelabel)
        ax[i, j].set_ylabel(r"$f$", fontsize=fontsizelabel)
        ax[i, j].legend(loc="upper right", fontsize=fontsizelegend)

        # %%
        t = tplot * torch.ones_like(v_simu)
        ind_x = int(trainer.sampler.x_len / 2) + 1
        xplot = x_simu[ind_x]
        x = SpaceTensor(xplot.x * torch.ones_like(v_simu), xplot.labels)
        mu = torch.zeros((v_simu.shape[0], 0))

        u_pred_v = trainer.network.get_w(t, x, v_simu, mu)

        feq_simu_xfixed = feq_simu[ind_x, :]
        ax[i + 1, j].plot(
            v_simu.detach().cpu(),
            (f_simu[indx_time, :, ind_x, :] - ideltaf * feq_simu_xfixed).transpose(
                1, 0
            ),
            label="simulation",
            c="k",
        )
        ax[i + 1, j].plot(
            v_simu.detach().cpu(),
            u_pred_v.detach().cpu() - ideltaf * feq_simu_xfixed.transpose(1, 0),
            label="prediction",
            c="r",
        )
        ax[i + 1, j].set_title(
            f"{value_name} at t= {str(np.round(tplot.cpu().numpy()[0],2))} and x= {str(np.round(xplot.x.cpu().numpy()[0],2))}",
            fontsize=fontsizetitle,
        )
        ax[i + 1, j].set_xlabel(r"$v$", fontsize=fontsizelabel)
        ax[i + 1, j].set_ylabel(r"$f$", fontsize=fontsizelabel)
        ax[i + 1, j].legend(loc="upper right", fontsize=fontsizelegend)

        # %%
        t_simu = trainer.sampler.data_t
        x_simu = trainer.sampler.data_x.x[:, 0]
        v_simu = trainer.sampler.data_v[:, 0]
        X, V = torch.meshgrid(x_simu, v_simu, indexing="ij")
        x, v = torch.flatten(X)[:, None], torch.flatten(V)[:, None]
        t = tplot * torch.ones_like(x)
        mu = torch.zeros((x.shape[0], 0))

        x_ = SpaceTensor(x, torch.zeros_like(x, dtype=torch.int))
        u_pred_xv = trainer.network.get_w(t, x_, v, mu).detach()
        u_pred_xv = torch.reshape(u_pred_xv, (x_simu.size()[0], v_simu.size()[0]))
        if not deltaf:
            p = ax[i + 2, j].pcolormesh(
                X.cpu(), V.cpu(), u_pred_xv.cpu(), shading="nearest"
            )
            ax[i + 2, j].set_title(
                f"f_pred at t = {str(np.round(tplot.cpu().numpy()[0],2))}",
                fontsize=fontsizetitle,
            )
        else:
            p = ax[i + 2, j].pcolormesh(
                X.cpu(),
                V.cpu(),
                (f_simu[indx_time, :, :] - u_pred_xv.cpu())[0, :],
                shading="nearest",
            )
            ax[i + 2, j].set_title(
                f"f_simu-f_pred at t = {str(np.round(tplot.cpu().numpy()[0],2))}",
                fontsize=fontsizetitle,
            )

        ax[i + 2, j].set_xlabel(r"$x$", fontsize=fontsizelabel)
        ax[i + 2, j].set_ylabel(r"$v$", fontsize=fontsizelabel)
        fig1.colorbar(p, ax=ax[i + 2, j])
        plt.xticks(fontsize=fontsizeticks)
        plt.yticks(fontsize=fontsizeticks)

        j = j + 1

    if deltaf:
        figname = f"{savedir}/fig_deltaf_epochs{trainer.epoch_tot}.png"
    else:
        figname = f"{savedir}/fig_fdistribu_epochs{trainer.epoch_tot}.png"
    print(f"==> save {figname}")
    plt.savefig(figname)
