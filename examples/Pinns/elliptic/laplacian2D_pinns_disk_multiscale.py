from pathlib import Path

import torch

from scimba.equations import domain, pdes
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling


class PoissonDisk2D(pdes.AbstractPDEx):
    def __init__(self, space_domain, k=2):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=0,
            parameter_domain=[[0.5, 0.5 + 1e-4]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.k = k

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def post_processing(self, x, mu, w, **kwargs):
        x1, x2 = x.get_coordinates()
        return (1 - x1**2 - x2**2) * w

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")

        arg = torch.pi * self.k * (1 - x1**2 - x2**2)
        sin = torch.sin(arg)
        cos = torch.cos(arg)

        f = 4 * torch.pi * self.k * (torch.pi * self.k * (x1**2 + x2**2) * sin + cos)

        return u_xx + u_yy + f

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        x1_0, x2_0 = self.space_domain.large_domain.center
        return torch.sin(self.k * torch.pi * (1 - x1**2 - x2**2))


def Run_laplacian2D(pde, bc_loss_bool=False, w_bc=0, w_res=1.0, Fourier=False):
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 20, 20, 20, 20]

    if Fourier:
        network = pinn_x.Fourier_x(
            pde=pde,
            mean_features={"x": 0.0, "mu": 0.0, "xmu": 0.0},
            std_features={"x": 6.0, "mu": 0.0, "xmu": 0.0},
            nb_features=20,
            layer_sizes=tlayers,
            activation_type="sine",
        )
    else:
        network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")

    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=bc_loss_bool, w_res=w_res, w_bc=w_bc
    )
    optimizers = training_tools.OptimizerData(learning_rate=1.2e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if not bc_loss_bool:
        if new_training:
            trainer.train(epochs=600, n_collocation=5000, n_data=0)
    else:
        if new_training:
            trainer.train(
                epochs=600, n_collocation=5000, n_bc_collocation=1000, n_data=0
            )

    trainer.plot(20000, reference_solution=True)
    # trainer.plot_derivative_mu(n_visu=20000)

    return network, trainer


if __name__ == "__main__":
    # Laplacian on circle with nn
    xdomain = domain.SpaceDomain(2, domain.DiskBasedDomain(2, [0.0, 0.0], 1.0))

    for k in [2, 8]:
        for Fourier in [False, True]:
            # define the PDE and train the network
            pde = PoissonDisk2D(xdomain, k=k)
            network_disk, trainer_disk = Run_laplacian2D(pde, Fourier=Fourier)

            # test contour plots on disk
            trainer_disk.plot_2d_contourf(draw_contours=True)
            trainer_disk.plot_2d_contourf(draw_contours=True, error=True)
            trainer_disk.plot_2d_contourf(draw_contours=True, residual=True)
