from pathlib import Path

from scimba.equations import pdes
from scimba.pinns import pinn_txv
from scimba.pinns.training_txv import TrainerPinnKinetic
import scimba.pinns.pinn_losses as pinn_losses
import scimba.nets.training_tools as training_tools
import torch
from scimba.equations.domain import SpaceDomain, SquareDomain
from scimba.equations.domain import SpaceTensor
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.sampling_pde_txv as sampling_pde_txv
import scimba.sampling.uniform_sampling as uniform_sampling
import scimba.sampling.sampling_ode as sampling_ode

class LinearVlasov(pdes.AbstractPDEtxv):
    """
    Advection equation class

    - Equation:

      .. math::

         \frac{df}{dt} + v \cdot \frac{df}{dx} = 0

    - Domain: :math:`x\in [0,1], t\in[0,0.1], v \in [-6,6]`
    - BC: periodic boundary condition in x
    - IC:

        .. math::

           f_0(x,v) = sin(2 \pi x) \cdot  1 / ( sqrt(2 \pi \sigma^2) ) \cdot exp(-v^2 / (2 \sigma^2) )

    - Reference solution: :math:`f_0(x-vt,v)`
    - We learn:  :math:`u_{\theta}(t,x,v)`
    - parametric model: :math:`f_{net} = f_0 + t \cdot u_{\theta}(t,x,v)`

    """

    def __init__(self,time_domain,space_domain,vel_domain):
        super().__init__(
            nb_unknowns=1,
            time_domain=time_domain,
            space_domain=space_domain,
            velocity_domain=vel_domain,
            nb_parameters=0,
            parameter_domain=[],
            data_construction="sampled",
        )

        self.first_derivative_t = True
        self.first_derivative_x = True
        self.first_derivative_v = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, v, mu, **kwargs):
        x = x.get_coordinates()
        u_t = self.get_variables(w, "w_t")
        u_x = self.get_variables(w, "w_x")
        u_v = self.get_variables(w, "w_v1")
        return u_t + v * u_x + torch.sin(x)* u_v

    def bc_residual_space(self, w, t, x, v, mu, **kwargs):
        # TODO: separate bc_residual for x and v
        u_l = self.get_variables(w, "w",label=0)
        u_r = self.get_variables(w, "w",label=1)

        return u_r-u_l
    
    def bc_residual_vel(self, w, t, x, v, mu, **kwargs):
        # TODO: separate bc_residual for x and v
        u = self.get_variables(w, "w")
        return u


    def initial_condition(self, x, v, mu):
        x = x.get_coordinates()

        sigma = 1.0
        return ( 1.0
            / (2 * torch.pi * sigma**2)**0.5
            * torch.exp(-(v**2) / (2.0 * sigma**2))
        )

tdomain=[0.0,2.0]
xdomain = SpaceDomain(1, SquareDomain(1, [[0.0, 2.0*torch.pi]]))
vdomain= SquareDomain(1, [[-6.0, 6.0]])
pde = LinearVlasov(time_domain=tdomain,space_domain=xdomain,vel_domain =vdomain)

t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
x_sampler = sampling_pde.XSampler(pde=pde)
v_sampler = sampling_pde_txv.VSampler(pde=pde)
mu_sampler = sampling_parameters.MuSampler(
    sampler=uniform_sampling.UniformSampling, model=pde
)
sampler = sampling_pde_txv.PdeTXVCartesianSampler_Periodic1D1V(t_sampler,x_sampler,v_sampler, mu_sampler)

#network = pinn_txv.MLP_txv(pde=pde, layer_sizes=[80, 80, 80, 80,80])
network =pinn_txv.RBFNet_txv_keops(pde,sampler,nb_func=120)
pinn = pinn_txv.PINNtxv(network, pde)

file_name ="test_kinetic.pth"
new_training = True

if new_training:
    (
        Path.cwd()
        / Path(TrainerPinnKinetic.FOLDER_FOR_SAVED_NETWORKS)
        / file_name
    ).unlink(missing_ok=True)

losses = pinn_losses.PinnLossesData(bc_loss_bool=True, init_loss_bool=True,w_res=0.9,w_bc=3.0,w_init=2)
optimizers = training_tools.OptimizerData(learning_rate=1.9e-2,decay=0.999)
trainer = TrainerPinnKinetic(
    pde=pde,
    sampler=sampler,
    network=pinn,
    losses =losses,
    optimizers=optimizers,
    file_name=file_name,
    batch_size=9000,
)

trainer.train(
    epochs=3000,
    n_collocation=9000,
    n_bc_collocation=3000,
    n_init_collocation=3000
)

trainer.plot(n_visu=60000)
