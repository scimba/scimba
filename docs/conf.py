from scimba import __version__

project = "Scimba"
copyright = "2023, IRMA"
author = "Emmanuel Franck"
version = __version__
release = __version__

extensions = [
    "myst_parser",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.napoleon",
    "sphinx.ext.autosummary",
    "sphinx.ext.mathjax",
    "sphinx_copybutton",
]

myst_enable_extensions = [
    "amsmath",
    "attrs_inline",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist",
]

autosummary_generate = True  # Turn on sphinx.ext.autosummary

templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

html_theme = "furo"
html_theme_options = {
    "source_edit_link": "https://gitlab.inria.fr/scimba/scimba/-/tree/main/docs",
}
html_static_path = ["_static"]
html_css_files = ["styles/furo.css", "custom.css"]
add_module_names = False
autodoc_default_options = {"members": True}  # documetation of all members
