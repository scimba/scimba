from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes
import time

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Helmholtz_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[2.0,2.0001]],
        )

        self.first_derivative = True
        self.second_derivative = True
        #self.third_derivative = True

       # self.coeff_third_derivative = 0.1

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        k = self.get_parameters(mu)


        # compute residual
        u = self.get_variables(w, "w")
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        sigma=0.03
        f = 100*torch.exp(-((x1) ** 2 + (x2-0.9)**2)/(2.0*sigma**2.0))/((2.0*torch.pi)*sigma)

        omega = 2.0*torch.pi*k
        res = omega**2.0*u+ u_xx + u_yy + f
        return res

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return (1.0 - x1) * (x1 + 1.0) * (x2 + 1.0) * (1.0 - x2) * w



def RunHelmholtz_2D(pde, bc_loss_bool=False, w_bc=0, w_res=1.0):
    tps1 = time.time()
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test_fe6.pth"
    new_training = False
    # new_training = True
    training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 60, 60, 60, 40]
    #network = pinn_x.Fourier_x(pde=pde, layer_sizes=tlayers, activation_type="sine",bool_feature_mu=True,
    #            nb_features=30,
    #            std_features={"x": 10, "mu": 0, "xmu": 0})
    network = pinn_x.MultiScale_Fourier_x(pde=pde,
                                           means=[0.0,0.0], 
                                           stds=[4.0, 10.0], 
                                           nb_features=40, 
                                           layer_sizes=tlayers,
                                           activation_type="sine")

    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=bc_loss_bool, w_res=w_res, w_bc=w_bc
    )
    optimizers = training_tools.OptimizerData(
        learning_rate=9.9e-3, decay=0.99, switch_to_LBFGS=True, switch_to_LBFGS_at=500
    )
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if not bc_loss_bool:
        if training:
            trainer.train(epochs=1500, n_collocation=5000, n_data=0)
    else:
        if training:
            trainer.train(
                epochs=12, n_collocation=5000, n_bc_collocation=2000, n_data=0
            )

    tps2 = time.time()
    print(" >>>",tps2 - tps1)
    trainer.plot(20000, random=True, reference_solution=False)

    return network, trainer


if __name__ == "__main__":
    # Laplacien strong Bc on Square with nn
    xdomain = domain.SpaceDomain(
        2, domain.SquareDomain(2, [[-1.0, 1.0], [-1.0, 1.0]])
    )
    pde = Helmholtz_2D(xdomain)

    network, trainer = RunHelmholtz_2D(pde)

    # test contour plots on square
    trainer.plot_2d_contourf(draw_contours=True)
    trainer.plot_2d_contourf(draw_contours=True, error=False)
