from math import pi as PI
from pathlib import Path

import torch

from scimba.nets import training_tools
from scimba.pinns import pinn_losses, training_x, pinn_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling
from scimba.equations import domain, pdes


class Nonlinear2DSystem(pdes.AbstractPDEx):
    r"""

    .. math::

        TODO

    """

    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=2,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[0.5, 1.0], [0.5, 1.0]],
        )

        self.first_derivative = True
        self.second_derivative = True

        def f_x(w, x, mu):
            u, v = self.get_variables(w)
            return torch.cat([u**2 / (2 * PI), u * v / (2 * PI)], axis=1)

        def f_y(w, x, mu):
            u, v = self.get_variables(w)
            return torch.cat([u * v / (2 * PI), v**2 / (2 * PI)], axis=1)

        def f_xx(w, x, mu):
            x1, x2 = x.get_coordinates()
            alpha, beta = self.get_parameters(mu)
            u, v = self.get_variables(w)
            return torch.cat(
                [-x2 * u / (alpha * PI**2), -x2 * v / (beta * PI**2)], axis=1
            )

        def f_xy(w, x, mu):
            u, v = self.get_variables(w)
            return torch.cat([u**2 / (4 * PI**2), v**2 / (4 * PI**2)], axis=1)

        def f_yy(w, x, mu):
            x1, x2 = x.get_coordinates()
            alpha, beta = self.get_parameters(mu)
            u, v = self.get_variables(w)
            return torch.cat(
                [x1 * v / (beta * PI**2), x1 * u / (alpha * PI**2)], axis=1
            )

        self.f_x = f_x
        self.f_y = f_y
        self.f_xx = f_xx
        self.f_xy = f_xy
        self.f_yy = f_yy

    def bc_residual(self, w, x, mu, **kwargs):
        u, v = self.get_variables(w)
        return u, v

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        alpha, beta = self.get_parameters(mu)

        u, v = self.get_variables(w)
        f_u_x, f_v_x = self.get_variables(w, "f_w_x")
        f_u_y, f_v_y = self.get_variables(w, "f_w_y")
        f_u_xx, f_v_xx = self.get_variables(w, "f_w_xx")
        f_u_xy, f_v_xy = self.get_variables(w, "f_w_xy")
        f_u_yy, f_v_yy = self.get_variables(w, "f_w_yy")

        sin_x = torch.sin(PI * x1)
        cos_x = torch.cos(PI * x1)

        sin_y = torch.sin(PI * x2)
        cos_y = torch.cos(PI * x2)

        rhs_1 = (
            sin_x
            * sin_y
            * (
                sin_x * cos_y * alpha * beta
                + cos_x * sin_y * alpha**2
                + cos_x * cos_y * alpha**2
                + alpha
                + x2
                - x1
            )
        )

        rhs_2 = (
            sin_x
            * sin_y
            * (
                sin_x * cos_y * beta**2
                + cos_x * cos_y * beta**2
                + cos_x * sin_y * alpha * beta
                + beta
                + x2
                - x1
            )
        )

        return (
            f_u_xx + f_u_xy + f_u_yy + f_u_x + f_u_y + u - rhs_1,
            f_v_xx + f_v_xy + f_v_yy + f_v_x + f_v_y + v - rhs_2,
        )

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha, beta = self.get_parameters(mu)
        return torch.cat(
            (
                alpha * torch.sin(PI * x1) * torch.sin(PI * x2),
                beta * torch.sin(PI * x1) * torch.sin(PI * x2),
            ),
            axis=1,
        )


def main():
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = Nonlinear2DSystem(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "testnonlinearsystem.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 60, 40, 20]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(bc_loss_bool=True, w_res=0.01, w_bc=5.0)
    optimizers = training_tools.OptimizerData(learning_rate=2e-2, decay=0.99)
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if train:
        trainer.train(epochs=1000, n_collocation=5000, n_bc_collocation=5000)

    trainer.plot(20000, reference_solution=True)
    # trainer.plot_derivative_mu(n_visu=20000)
    # trainer.plot_derivative_xmu(n_visu=20000)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
