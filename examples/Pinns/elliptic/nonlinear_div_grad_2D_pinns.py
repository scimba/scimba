from math import pi as PI
from pathlib import Path

import torch
from scimba.equations import domain, pdes
from scimba.nets import training_tools
from scimba.pinns import pinn_losses, pinn_x, training_x
from scimba.sampling import sampling_parameters, sampling_pde, uniform_sampling


class NonlinearDivGrad2D(pdes.AbstractPDEx):
    r"""

    .. math::

        div(K grad u) + 10 * u = f

    """

    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=1,
            parameter_domain=[[0.5, 1.5]],
        )

        self.first_derivative = True
        self.second_derivative = True

        def anisotropy_matrix(w, x, mu):
            u = self.get_variables(w)
            x1, x2 = x.get_coordinates()
            alpha = self.get_parameters(mu)

            return torch.cat((u**2, -(x1 + 1) * u, -alpha * u, u**2), axis=1)

        self.anisotropy_matrix = anisotropy_matrix

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        α = self.get_parameters(mu)

        u = self.get_variables(w)
        div_K_grad_u = self.get_variables(w, "div_K_grad_w")

        sin_x = torch.sin(PI * x1)
        cos_x = torch.cos(PI * x1)

        sin_y = torch.sin(PI * x2)
        cos_y = torch.cos(PI * x2)

        rhs = (
            -sin_x
            * sin_y
            * α
            * (
                2 * PI**2 * sin_x**2 * sin_y**2 * α**2
                - 2 * PI**2 * cos_x**2 * sin_y**2 * α**2
                - 2 * PI**2 * sin_x**2 * cos_y**2 * α**2
                + 2 * PI**2 * cos_x * cos_y * α**2
                + PI * sin_x * cos_y * α
                + 2 * PI**2 * x1 * cos_x * cos_y * α
                + 2 * PI**2 * cos_x * cos_y * α
                - 10
            )
        )

        return div_K_grad_u + 10 * u - rhs

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        alpha = self.get_parameters(mu)
        return alpha * torch.sin(PI * x1) * torch.sin(PI * x2)

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return x1 * (1 - x1) * x2 * (1 - x2) * w


def main():
    xdomain = domain.SpaceDomain(2, domain.SquareDomain(2, [[0.0, 1.0], [0.0, 1.0]]))
    pde = NonlinearDivGrad2D(xdomain)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test.pth"
    # new_training = False
    new_training = True

    train = True
    # train = False

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [20, 40, 60, 60, 40, 20]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)

    losses = pinn_losses.PinnLossesData(w_res=1.0)
    optimizers = training_tools.OptimizerData(
        learning_rate=2e-2, decay=0.99, switch_to_LBFGS=True
    )
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=5000,
    )

    if train:
        trainer.train(epochs=75, n_collocation=10000)

    trainer.plot(20000, reference_solution=True)
    trainer.plot_derivative_mu(n_visu=20000)
    trainer.plot_derivative_xmu(n_visu=20000)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
