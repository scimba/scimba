import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.distributions import MultivariateNormal

try:
    from sklearn import datasets
except ModuleNotFoundError:
    exit("Please install scikit-learn")

from scimba.generativenets import (
    generativelosses,
    normalizingflows,
    simpleflows,
    trainer_likelihood,
)
from scimba.nets import mlp
from scimba.nets.training_tools import OptimizerData


class DatasetMoons:
    """two half-moons"""

    def sample(self, n):
        moons = datasets.make_moons(n_samples=n, noise=0.05)[0].astype(np.float32)
        return torch.from_numpy(moons)


def main():
    data = DatasetMoons()

    x = data.sample(1024)
    plt.figure(figsize=(4, 4))
    plt.scatter(x[:, 0], x[:, 1], s=5, alpha=0.5)
    plt.axis("equal")
    plt.show()

    out_size = 2
    cond_size = 0
    prior = MultivariateNormal(torch.zeros(out_size), torch.eye(out_size))
    tlayers = [20, 20, 20, 20]
    # flows = [gn.simpleflows.AffineConstantFlow(net=mlp.GenericMLP,dim=out_size,dim_conditional=cond_size) for _ in range(8)]

    flows = [
        simpleflows.RealNVPFlow(
            net=mlp.GenericMLP,
            dim=out_size,
            dim_conditional=cond_size,
            parity=i % 2,
            layer_sizes=tlayers,
        )
        for i in range(5)
    ]
    normalizedflow = normalizingflows.NormalizingFlow(prior, flows)

    losses = generativelosses.GenerativeLossesData()
    optimizers = OptimizerData(learning_rate=1.2e-2, decay=0.99)
    Trainer = trainer_likelihood.TrainerLikelihood(
        out_size=2,
        output_data=x,
        network=normalizedflow,
        losses=losses,
        optimizers=optimizers,
        batch_size=128,
    )
    Trainer.train(epochs=1000)

    zs = normalizedflow.sample(x=torch.zeros(512, 0), num_samples=512)
    z = zs[-1]
    z = z.detach().cpu()
    z0 = normalizedflow.sample_prior(num_samples=512)
    z0 = z0.detach().cpu()
    plt.subplot(122)
    plt.scatter(x[:, 0].detach().cpu(), x[:, 1].detach().cpu(), c="b", s=5, alpha=0.5)
    plt.scatter(z[:, 0].detach().cpu(), z[:, 1].detach().cpu(), c="r", s=5, alpha=0.5)
    plt.scatter(z0[:, 0].detach().cpu(), z0[:, 1].detach().cpu(), c="g", s=5, alpha=0.5)
    plt.legend(["data", "z->x"])
    plt.axis("scaled")
    plt.title("z -> x")
    plt.show()


if __name__ == "__main__":
    main()
