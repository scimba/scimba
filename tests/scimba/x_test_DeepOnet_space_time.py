from math import pi as PI
import os

import torch

from scimba.equations import domain, pdes
from scimba.sampling import (
    sampling_pde,
    sampling_parameters,
    uniform_sampling,
    sampling_ode,
    sampling_functions,
)
from scimba.neural_operators import deeponet_tx
from scimba.pinns import pinn_tx
from scimba.pinos import pino_tx, training_tx


class Heat_equation(pdes.AbstractPDEtx):
    def __init__(
        self,
        tdomain=[0, 0.03],
        xdomain=domain.SpaceDomain(1, domain.SquareDomain(1, [[0, 2]])),
        p_domain=[[0.5, 0.50001]],
    ):
        super().__init__(
            nb_unknowns=1,
            time_domain=tdomain,
            space_domain=xdomain,
            nb_parameters=1,
            parameter_domain=p_domain,
        )

        self.first_derivative_t = True
        self.second_derivative_t = False
        self.first_derivative_x = True
        self.second_derivative_x = True
        self.t_min, self.t_max = self.time_domain

    def residual(self, w, t, x, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u_t = self.get_variables(w, "w_t")
        u_xx = self.get_variables(w, "w_xx")
        f = kwargs.get("f", None)
        return u_t - alpha * u_xx - f

    def bc_residual(self, w, t, x, mu, **kwargs):
        u = self.get_variables(w)
        return u

    def initial_condition(self, x, mu, **kwargs):
        t0 = 0.02
        alpha = self.get_parameters(mu)
        return (1.0 / torch.sqrt(4.0 * PI * alpha * t0)) * torch.exp(
            -((x - 1) ** 2) / (4 * alpha * t0)
        )

    def bc_add(self, t, x, mu, w):
        return self.initial_condition(x, mu)

    def bc_mul(self, t, x, mu):
        return x * (2 - x) * t

    def make_data(self, n_data):
        pass


def test_DeepOnet_tx(tmp_path):
    xdomain = domain.SpaceDomain(1, domain.SquareDomain(1, [[0.0, 2.0]]))
    pde = Heat_equation(tdomain=[0.0, 0.03], xdomain=xdomain)
    t_sampler = sampling_ode.TSampler(sampler=uniform_sampling.UniformSampling, ode=pde)
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )

    sampler = sampling_pde.PdeTXCartesianSampler(t_sampler, x_sampler, mu_sampler)

    def func(t, x, mu):
        res = 0.1 * mu[0, 0] * torch.sin(t) * torch.exp(-((x - mu[0, 1]) ** 2.0) / 0.05)
        return res

    f_sampler = sampling_functions.FunctionsSampler(
        2,
        [[0.5, 1.0], [0.8, 1.3]],
        sampler=sampler,
        t_sampler=t_sampler,
        x_sampler=x_sampler,
        f=func,
        n_sensor=4,
    )

    no_network = deeponet_tx.DeepONetSpaceTime(
        net=pinn_tx.MLP_tx,
        pde=pde,
        n_sensor=f_sampler.N,
        lat_size=3,
        decoder_type="nonlinear",
        layers_b=[2, 3],
        ayers_t=[3, 2],
    )

    network = pino_tx.PINOtx(no_network, pde)

    file_name = "deeponet_space_time.pth"
    os.chdir(tmp_path)

    trainer = training_tx.TrainerPINOSpaceTime(
        network=network,
        pde=pde,
        sampler=f_sampler,
        learning_rate=5e-3,
        decay=0.99,
        batch_size=500,
        file_name=file_name,
    )

    trainer.train(epochs=2, n_collocation_f=5, n_collocation_tx=10)
