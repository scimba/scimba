"""
Abstract classes for equations (ODEs and PDEs), with some examples, and definitions of space and time domains.
"""
