# imports
from pathlib import Path

import torch

from src.com1PINNs import poisson_sample_in_disk as poisson

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}; script is solve_Poisson.py")

if __name__ == "__main__":

    # ==============================================================
    # Parameters to be modified freely by the user
    # ==============================================================

    train = True
    # train = False

    PINNsDict = {
        "learning_rate": 5e-3,
        "layer_sizes": [10, 20, 40, 20, 10],
        "rho_min": 0,
        "rho_max": 1,
        "file_name": "dirichlet",
        # "symplecto_name": "ellipse",
        "to_be_trained": train,
        "source_term": "one",
        "boundary_condition": "homogeneous_dirichlet",
    }

    epochs = 500
    n_collocation = 2_000
    # new_training = False
    new_training = True
    save_plots = False
    # save_plots = True

    # ==============================================================
    # End of the modifiable area
    # ==============================================================

    if train:
        if new_training:
            try:
                output_dir = Path.cwd() / "networks"
                Path(output_dir / f"{PINNsDict['file_name']}.pth").unlink()
            except FileNotFoundError:
                pass

        network = poisson.PINNs(PINNsDict=PINNsDict)
        elapsed = network.train(epochs=epochs, n_collocation=n_collocation)

        print(f"Computational time: {elapsed:3.2f} sec.")

    else:
        network = poisson.PINNs(PINNsDict=PINNsDict)

    if (
        PINNsDict["boundary_condition"] == "homogeneous_dirichlet"
        and PINNsDict["symplecto_name"] == "ellipse"
        and PINNsDict["source_term"] == "one"
    ):
        network.plot_result(save_plots, error=True)
    else:
        network.plot_result(save_plots)
