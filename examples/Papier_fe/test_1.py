from pathlib import Path

import scimba.nets.training_tools as training_tools
import scimba.pinns.pinn_losses as pinn_losses
import scimba.pinns.pinn_x as pinn_x
import scimba.pinns.training_x as training_x
import scimba.sampling.sampling_parameters as sampling_parameters
import scimba.sampling.sampling_pde as sampling_pde
import scimba.sampling.uniform_sampling as uniform_sampling
import torch
from scimba.equations import domain, pdes
import time

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"torch loaded; device is {device}")

torch.set_default_dtype(torch.double)
torch.set_default_device(device)

PI = 3.14159265358979323846


class Poisson_2D(pdes.AbstractPDEx):
    def __init__(self, space_domain):
        super().__init__(
            nb_unknowns=1,
            space_domain=space_domain,
            nb_parameters=2,
            parameter_domain=[[-0.5, 0.5], [-0.5, 0.5]],
        )

        self.first_derivative = True
        self.second_derivative = True
        self.third_derivative = True

        self.coeff_third_derivative = 0.1

    def make_data(self, n_data):
        pass

    def bc_residual(self, w, x, mu, **kwargs):
        return self.get_variables(w)

    def residual(self, w, x, mu, **kwargs):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)

        # compute auxiliary variables
        exp = torch.exp((-(x2 - mu2) ** 2 - (x1 - mu1) ** 2) / 2)
        sin1 = torch.sin(2 * x1)
        sin2 = torch.sin(2 * x2)
        cos1 = torch.cos(2 * x1)
        cos2 = torch.cos(2 * x2)

        # compute residual
        u_xx = self.get_variables(w, "w_xx")
        u_yy = self.get_variables(w, "w_yy")
        f = -exp * (
            ((mu1 - x1) ** 2 + (mu2 - x2) ** 2 - 10) * sin1 * sin2
            + 4 * (mu1 - x1) * cos1 * sin2
            + 4 * (mu2 - x2) * sin1 * cos2
        )

        res = u_xx + u_yy + f

        # compute d/dx residual
        u_xxx = self.get_variables(w, "w_xxx")
        u_xyy = self.get_variables(w, "w_xyy")

        df_dx = -(mu1 - x1) * exp * (
            ((mu2 - x2) ** 2 + (mu1 - x1) ** 2 - 10) * sin1 * sin2
            + 4 * (mu1 - x1) * cos1 * sin2
            + 4 * (mu2 - x2) * sin1 * cos2
        ) + exp * (
            (4 - 2 * ((mu2 - x2) ** 2 + (mu1 - x1) ** 2 - 10)) * cos1 * sin2
            + 10 * (mu1 - x1) * sin1 * sin2
            - 8 * (mu2 - x2) * cos1 * cos2
        )

        dres_dx = u_xxx + u_xyy + df_dx

        # compute d/dy residual
        u_xxy = self.get_variables(w, "w_xxy")
        u_yyy = self.get_variables(w, "w_yyy")

        df_dy = -(mu2 - x2) * exp * (
            ((mu2 - x2) ** 2 + (mu1 - x1) ** 2 - 10) * sin1 * sin2
            + 4 * (mu1 - x1) * cos1 * sin2
            + 4 * (mu2 - x2) * sin1 * cos2
        ) + exp * (
            (4 - 2 * ((mu2 - x2) ** 2 + (mu1 - x1) ** 2 - 10)) * sin1 * cos2
            + 10 * (mu2 - x2) * sin1 * sin2
            - 8 * (mu1 - x1) * cos1 * cos2
        )

        dres_dy = u_xxy + u_yyy + df_dy

        return torch.sqrt(
            res**2 + self.coeff_third_derivative**2 * (dres_dx**2 + dres_dy**2)
        )

    def post_processing(self, x, mu, w):
        x1, x2 = x.get_coordinates()
        return (0.5 * PI - x1) * (x1 + 0.5 * PI) * (x2 + 0.5 * PI) * (0.5 * PI - x2) * w

    def reference_solution(self, x, mu):
        x1, x2 = x.get_coordinates()
        mu1, mu2 = self.get_parameters(mu)
        g = torch.exp(-((x1 - mu1) ** 2 + (x2 - mu2) ** 2) / 2)
        return g * torch.sin(2 * x1) * torch.sin(2 * x2)


def Run_laplacian2D(pde, bc_loss_bool=False, w_bc=0, w_res=1.0):
    tps1 = time.time()
    x_sampler = sampling_pde.XSampler(pde=pde)
    mu_sampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=pde
    )
    sampler = sampling_pde.PdeXCartesianSampler(x_sampler, mu_sampler)

    file_name = "test_fe1.pth"
    new_training = False
    # new_training = True
    training = True

    if new_training:
        (
            Path.cwd()
            / Path(training_x.TrainerPINNSpace.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)

    tlayers = [40, 60, 60, 60, 40]
    network = pinn_x.MLP_x(pde=pde, layer_sizes=tlayers, activation_type="sine")
    pinn = pinn_x.PINNx(network, pde)
    losses = pinn_losses.PinnLossesData(
        bc_loss_bool=bc_loss_bool, w_res=w_res, w_bc=w_bc
    )
    optimizers = training_tools.OptimizerData(
        learning_rate=1.6e-2, decay=0.99, switch_to_LBFGS=False, switch_to_LBFGS_at=1000
    )
    trainer = training_x.TrainerPINNSpace(
        pde=pde,
        network=pinn,
        sampler=sampler,
        losses=losses,
        optimizers=optimizers,
        file_name=file_name,
        batch_size=6000,
    )

    if not bc_loss_bool:
        if training:
            trainer.train(epochs=1500, n_collocation=6000, n_data=0)
    else:
        if training:
            trainer.train(
                epochs=12, n_collocation=5000, n_bc_collocation=2000, n_data=0
            )

    tps2 = time.time()
    print(" >>>",tps2 - tps1)
    trainer.plot(20000, random=True, reference_solution=True)

    return network, trainer


if __name__ == "__main__":
    # Laplacien strong Bc on Square with nn
    xdomain = domain.SpaceDomain(
        2, domain.SquareDomain(2, [[-0.5 * PI, 0.5 * PI], [-0.5 * PI, 0.5 * PI]])
    )
    pde = Poisson_2D(xdomain)

    network, trainer = Run_laplacian2D(pde)

    # test contour plots on square
    trainer.plot_2d_contourf(draw_contours=True)
    trainer.plot_2d_contourf(draw_contours=True, error=True)
