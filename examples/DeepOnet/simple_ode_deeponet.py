from pathlib import Path

import matplotlib.pyplot as plt
import torch
from scimba.equations.pdes import AbstractODE
from scimba.neural_operators import deeponet_t
from scimba.pinns import pinn_t
from scimba.pinos import pino_t, training_t
from scimba.sampling import (
    data_sampling_ode,
    sampling_functions,
    sampling_ode,
    sampling_parameters,
    uniform_sampling,
)
from scimba.pinns.pinn_losses import PinnLossesData
from scimba.nets.training_tools import OptimizerData

"""
    Using DeepONet to solve the equation below:

        \frac{du}{dt} + \mu_1 u = f, u(0)=1

    where f = exp(-\mu_2 t)
"""


class SimpleOdeWithSource(AbstractODE):
    r"""
    .. math::

        \frac{du}{dt} + \alpha u = f

    """

    def __init__(self):
        super().__init__(
            nb_unknowns=1,
            time_domain=[0, 10.0],
            nb_parameters=1,
            parameter_domain=[[0.9999, 1]],
        )

        self.first_derivative = True
        self.second_derivative = False
        self.t_min, self.t_max = self.time_domain[0], self.time_domain[1]
        self.data_construction = "sampled"

    def initial_condition(self, mu, **kwargs):
        alpha = self.get_parameters(mu)
        return torch.ones_like(alpha)

    def residual(self, w, t, mu, **kwargs):
        alpha = self.get_parameters(mu)
        u = self.get_variables(w)
        u_t = self.get_variables(w, "w_t")
        f = kwargs.get("f", None)
        return u_t + alpha * u - f

    def post_processing(self, t, mu, w):
        return self.initial_condition(mu) + t * w

    def reference_solution(self, t, mu):
        return None


def plot(network, trainer, n_visu=500, n_plot=5):
    for i_plot in range(n_plot):
        sample, f_loss = trainer.sampler.sampling(n_visu, 1)
        out = network.get_w(sample.t_loss, sample.params, sample)

        α = sample.params
        b = trainer.sampler.source_sampler.params

        plt.scatter(
            sample.t_loss.detach().cpu(),
            out.detach().cpu(),
            label=f"DeepONet α, mu = {α[0].item():3.2f}, {b[0].item():3.2f}",
        )

        t = torch.linspace(*trainer.ode.time_domain, n_visu)[:, None]

        exact = torch.exp(-α * t) * (torch.exp(t * (α - b)) + α - b - 1) / (α - b)

        plt.plot(
            t.detach().cpu(),
            exact.detach().cpu(),
            label=f"ref α, mu = {α[0].item():3.2f}, {b[0].item():3.2f}",
            color="black",
        )
        plt.legend()
        plt.show()


def main():
    ode = SimpleOdeWithSource()
    t_usampler = sampling_ode.TSampler(
        sampler=uniform_sampling.UniformSampling, ode=ode
    )
    mu_usampler = sampling_parameters.MuSampler(
        sampler=uniform_sampling.UniformSampling, model=ode
    )
    w_initial_usampler = uniform_sampling.UniformSampling(1, [[1.0, 1.000001]])

    class myFunc(sampling_functions.ParametricFunction_t):
        def __init__(self):
            super().__init__(dim_f=1, dim_p=1, p_domain=[[0.0, 1.0]])

        def __call__(self, t, params):
            return torch.exp(-self.get_parameters(params) * t)

    ode_sampler = data_sampling_ode.ode_data(
        sampler_t=t_usampler,
        sampler_params=mu_usampler,
        sampler_initial_condition=w_initial_usampler,
        source=myFunc(),
        n_sensor=80,
    )

    no_network = deeponet_t.DeepONetTime(
        net=pinn_t.MLP_t,
        ode=ode,
        ode_sampler=ode_sampler,
        lat_size=5,
        layers_b=[80, 60, 60, 40],
        layers_t=[20, 40, 40, 20],
        decoder_type="nonlinear",
        encoder_type="PointNet",
    )
    network = pino_t.PINOt(no_network, ode)

    file_name = "test.pth"
    new_training = True
    train = True

    if new_training:
        (
            Path.cwd()
            / Path(training_t.TrainerPINOTime.FOLDER_FOR_SAVED_NETWORKS)
            / file_name
        ).unlink(missing_ok=True)


    losses = PinnLossesData(w_res=1.0)
    optimizers = OptimizerData(learning_rate=2e-2,decay=0.992)
    trainer = training_t.TrainerPINOTime(
        network=network,
        ode=ode,
        sampler=ode_sampler,
        losses=losses,
        optimizers=optimizers,
        batch_size=1000,
        file_name=file_name,
    )

    if train:
        trainer.train(epochs=1000, n_simu=5, n_collocation_t=1000)

    return network, trainer


if __name__ == "__main__":
    network, trainer = main()
    plot(network, trainer)
