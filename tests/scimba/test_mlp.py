import os

import pytest
import torch

from scimba.nets import mlp, training
from scimba.sampling import uniform_sampling


def g(x):
    x1 = x[:, 0]
    x2 = x[:, 1]
    return torch.sin(x1) * torch.sin(x2)


@pytest.mark.parametrize(
    "ac_type", ["tanh", "adaptative_tanh", "sine", "cosin", "silu", "sigmoid"]
)
@pytest.mark.parametrize(
    "architecture",
    [
        mlp.GenericMLP,
        mlp.GenericMMLP,
        mlp.DiscontinuousMLP,
        mlp.FourierMLP,
    ],
)
def test_train_ac(tmp_path, ac_type, architecture):
    sampler = uniform_sampling.UniformSampling(2, [[-2.0, 2.0], [-2.0, 2.0]])
    x = sampler.sampling(2)
    y = g(x)
    y = y[:, None]

    file_name = "test_mlp.pth"
    os.chdir(tmp_path)

    tlayers = [2, 4, 2]
    network = architecture(2, 1, layer_sizes=tlayers, activation_type=ac_type)

    trainer = training.TrainerSupervised(
        in_size=2,
        input_data=x,
        out_size=1,
        output_data=y,
        network=network,
        file_name=file_name,
        learning_rate=1.2e-2,
        decay=0.99,
        batch_size=200,
    )

    trainer.train(epochs=3)
