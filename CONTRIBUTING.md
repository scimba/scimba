# Contribution Guidelines

Thank you for considering contributing to Scimba project! We appreciate your interest and support. This guide will help you get started with the contribution process.

## Always start by opening an issue

Before you start working on a pull request, you should always [open an issue](-/issues) first (consider [this blog article](https://about.gitlab.com/blog/2016/03/03/start-with-an-issue/)).
This allows us to discuss your proposed changes before you invest a lot of time and effort.
It also allows us to coordinate if multiple people are interested in working on the same thing.

Please follow these steps:

- Using the search function of this issue page, you can check if someone else has already opened an issue for the same topic.
- When you open an issue, provide a clear description of the problem you want to solve or the feature you want to add.
- Select the appropriates issue tags.
- Mention the names of people you want to be involved in the discussion by using the `@` symbol.

## Create a feature branch

GitLab workflow: [Feature Branch Workflow](https://docs.gitlab.com/ee/topics/gitlab_flow.html#feature-branch-workflow)
We recommend that you create a new branch for each merge request.