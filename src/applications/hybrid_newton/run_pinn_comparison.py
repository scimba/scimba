import numpy as np
from comparison import MAXITER, compare_to_pinns


def manipulate_results(results, n_simu):
    means = np.mean(results, axis=0)
    failure_rates = 100 * np.sum(results >= MAXITER, axis=0) / n_simu

    n_nets = 3
    n_nx = 4
    n_alpha0 = 3

    res = np.empty((n_nets, n_nx, 2 * n_alpha0))

    res[..., ::2] = means
    res[..., 1::2] = failure_rates

    res = res.transpose(1, 0, 2)

    return res


if __name__ == "__main__":
    n_simu = 40

    results = []
    for seed in 42 + np.arange(n_simu):
        results.append(compare_to_pinns(seed))

    results = np.array(results)

    np.save("pinn_comparison.npy", results)

    res = manipulate_results(results, n_simu)
    print(res)


# results below

""" tables for each N

$1$      & 280 & 0    & 1293 & 30  & 1825 & 75 \\
Int-Deep & 276 & 10   & 304  & 10  & 364  & 7.5 \\
PINN     & 81  & 2.5  & 40   & 0   & 98   & 2.5 \\
ours & & & & & & \\

$1$      & 351 & 0    & 781  & 5   & 1288 & 32.5 \\
Int-Deep & 442 & 12.5 & 702  & 22.5 & 681 & 17.5 \\
PINN     & 198 & 2.5  & 219  & 0   & 306  & 2.5 \\
ours & & & & & & \\

$1$      & 1157 & 5    & 1888 & 70  & 2000 & 100 \\
Int-Deep & 803  & 15   & 1117 & 20  & 1491 & 37.5 \\
PINN     & 495  & 2.5  & 696  & 0   & 950  & 2.5 \\
ours & & & & & & \\

$1$      & 1736 & 45   & 2000 & 100 & 2000 & 100 \\
Int-Deep & 1159 & 22.5 & 1614 & 32.5 & 1866 & 60 \\
PINN     & 769  & 2.5  & 1125 & 0   & 1520 & 17.5 \\
ours & & & & & & \\

"""
